//
//  MyAudioPlayer.swift
//  Audio Player
//
//  Created by ksymac on 2019/09/29.
//  Copyright © 2019 Or Fleisher. All rights reserved.
//

import Foundation
import AVFoundation


protocol MyAudioPlayerDelegate {
    func SetDuration(duration: CGFloat)
    func SetProgress(duration: CGFloat, currentTime: Double)
}

class MyAudioPlayer {
    var audioPlayer: AVAudioPlayer? = nil
    var path = Bundle.main.url(forResource: "song", withExtension: "mp3")
    var duration:CGFloat = 0.0
    var currentTime:Double = 0.0
    var timer: Timer? = nil
    
    var delegate: MyAudioPlayerDelegate?
    
    func setFileUrl(url:URL?){
        guard  let url = url else {
            return
        }
        path = url
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url )
            duration = CGFloat(audioPlayer?.duration ?? 0.0)
            self.delegate?.SetDuration(duration: duration)
        } catch {
            //assertionFailure("Failed crating audio player: \(error).")
            //return nil
        }
    }
    func playStart(){
        do {
            if let unpackedPath = path {
                try audioPlayer = AVAudioPlayer(contentsOf: unpackedPath)
                audioPlayer!.play()
            }
        } catch {
            print(error)
        }
        let displayLink = CADisplayLink(target: self, selector: #selector(update(_:)))
        displayLink.preferredFramesPerSecond = 2
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    func playStop(){
        audioPlayer!.stop()
        audioPlayer!.currentTime = 0
    }
    func playPause(){
        audioPlayer!.pause()
    }
    @objc func update(_ displayLink: CADisplayLink) {
        self.currentTime = self.audioPlayer?.currentTime ?? 0.0
        self.delegate?.SetProgress(duration: self.duration, currentTime: self.currentTime)
    }
}
