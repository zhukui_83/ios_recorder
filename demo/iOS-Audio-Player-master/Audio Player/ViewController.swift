//
//  ViewController.swift
//  Audio Player
//
//  Created by ITP on 10/11/17.
//  Copyright © 2017 Or Fleisher. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    //References to the UI elements
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var ProgressLabel: UILabel!
    @IBOutlet weak var ProgressBar: UIProgressView!
    
    let volume = AVAudioSession.sharedInstance().outputVolume
    let player = MyAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        volumeSlider.value = volume
        player.delegate = self 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func PressPlay(_ sender: Any) {
            player.playStart()
    }
    
    @IBAction func PressStop(_ sender: Any) {
        player.playStop()
    }
    
    @IBAction func PressPause(_ sender: Any) {
        player.playPause()
    }
    
    @IBAction func m1(_ sender: Any) {
        let path = Bundle.main.url(forResource: "song", withExtension: "mp3")
        player.setFileUrl(url: path)
    }
    @IBAction func m2(_ sender: Any) {
        let path = Bundle.main.url(forResource: "1", withExtension: "mp3")
        player.setFileUrl(url: path)
    }
    @IBAction func m3(_ sender: Any) {
        let path = Bundle.main.url(forResource: "2", withExtension: "m4a")
        player.setFileUrl(url: path)
    }
    @IBAction func m4(_ sender: Any) {
        let path = Bundle.main.url(forResource: "song", withExtension: "mp3")
        player.setFileUrl(url: path)
    }
}

extension ViewController:MyAudioPlayerDelegate{
    func SetDuration(duration: CGFloat){
        ProgressLabel.text = duration.description
    }
    
    func SetProgress(duration: CGFloat, currentTime: Double){
        let progress:Float = Float(CGFloat(currentTime) / duration)
        self.ProgressBar.progress = progress
    }
}
