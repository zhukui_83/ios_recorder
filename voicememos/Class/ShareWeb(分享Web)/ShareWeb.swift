//
//  ShareWeb.swift
//  voicememos
//
//  Created by ksymac on 2019/10/06.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation
import SSZipArchive

protocol ShareWebDelegate {
    func createdZip()
}

class ShareWeb{
    
    var dirName = ""
    var dirPath = ""
    var zipdirPath = ""
    var imgDirPath = ""
    var htmlPath = ""
    var zipPath = ""
    var delegate:ShareWebDelegate?
    
    var bean : TrackMain?
    var messages: [ZukMessageModel]?
    let fileManager = FileManager.default
    
    func creatHtml(bean : TrackMain? )  {
        self.bean = bean
        let items = DBDetailTable.shared.getAllByfileName((self.bean?.filename)!)
        self.messages = MyDataAdapter.getMessageFromDBData(items: items)
        
        dirName = "/recordshare/" + (self.bean?.filename ?? "temp")
        creatDir()
        
        var str = creatRowHtml(item: nil, header: true)
        for item in messages ?? [] {
            str += creatRowHtml(item: item)
        }
        
        let ret = htmlstr1_header + str + htmlstr1_footer
        do {
            let htmlURL = URL.init(fileURLWithPath: htmlPath)
            try ret.write(to: htmlURL, atomically: false, encoding: .utf8)
            //let text = try String( contentsOf: htmlURL, encoding: String.Encoding.utf8 )
            //print( text )
        }catch {/* error handling here */}
        
        copyRecordFile()
        
        SSZipArchive.createZipFile(atPath: zipPath, withContentsOfDirectory: dirPath)
        
        self.delegate?.createdZip()
    }
    
    func copyRecordFile(){
        if let frompath = bean?.fileurl?.path {
            let topath = dirPath + "/" + (bean?.filenamewithextension ?? "record.mp3")
            do {
                try FileManager.default.copyItem( atPath: frompath, toPath: topath )
            } catch {
                print("error")
            }
        }
    }
    
    func creatDir(){
        zipdirPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/recordshare/zip"
        
        dirPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + dirName

        
        imgDirPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + dirName + "/image"
        htmlPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + dirName + "/memo.html"
        
        zipPath = zipdirPath + "/" + (self.bean?.filename ?? "temp") + ".zip"
        
        var isDir : ObjCBool = false
        
        fileManager.fileExists(atPath: dirPath, isDirectory: &isDir)
        
        try? fileManager.removeItem(atPath: dirPath)
        try! fileManager.createDirectory(atPath: dirPath ,withIntermediateDirectories: true, attributes: nil)
        try! fileManager.createDirectory(atPath: zipdirPath ,withIntermediateDirectories: true, attributes: nil)
        try! fileManager.createDirectory(atPath: imgDirPath ,withIntermediateDirectories: true, attributes: nil)
        //if !isDir {
        //}
    }
    
    func creatRowHtml(item: ZukMessageModel?, header:Bool = false) -> String{
        var headstr = """
        <div>
            <h1>###</h1>
        </div>
        """
        var imgstr = """
        <div>
            <small>#time#:</small>
            <img src="###" style="max-height: 400px; max-width: 400px;">
        </div>
        """
        var txtstr = """
        <div>
        <small>#time#:</small>
        <big>###</big>
        </div>
        """
        if (header ){
            let temp = headstr.replacingOccurrences(of: "###", with: (self.bean?.filenamewithextension ?? ""))
            return temp
        }
        guard let item = item else{
            return ""
        }
        if item.MsgType == ZukMessageModel.MessageType.image{
            let fileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: item.ImageName ?? "")
            let temp = imgstr.replacingOccurrences(of: "###", with: ("./image/" + (item.ImageName ?? ""))).replacingOccurrences(of: "#time#", with: MyDateUtils.stringFromTimeInterval(time: item.TagTime))

            saveImageFileToCache(img: fileURL, item: item)
            
            return temp
        } else {
            let temp = txtstr.replacingOccurrences(of: "###", with: (item.body ?? "")).replacingOccurrences(of: "#time#", with: MyDateUtils.stringFromTimeInterval(time: item.TagTime))
            
            return temp
        }
    }
    func saveImageFileToCache(img:URL?, item:ZukMessageModel){
        guard let frompath = img?.path else{
            return
        }
        let topath = imgDirPath + "/" + (item.ImageName ?? "")
        do {
            try FileManager.default.copyItem( atPath: frompath, toPath: topath )
        } catch {
            print("error")
        }
    }
    
    let htmlstr1 = """
    <html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title></title>
    </head>
    <body>

    <div class="container"><div class="row">

        <div>
            <h5>Naimish Sakhpara</h5>
            <small>Hello</small>
        </div>
        <div>
            <h5 > Naimish Sakhpara</h5>
            <img border="0" src="http://www.htmq.com/images/img002.gif" width="128" height="128" alt="イラスト1">
        </div>

    </div>
    </body>
    </html>
    """
    let htmlstr1_header = """
    <!DOCTYPE html>
    <html><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title></title>
    </head><body>
    <div class="container">
    <div class="row">
    """
    let htmlstr1_footer = """
    </div>
    </div></body></html>
    """
}
