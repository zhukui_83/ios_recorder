//
//  ShareWebViewController.swift
//  voicememos
//
//  Created by ksymac on 2019/10/06.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
let screenSize = UIScreen.main.bounds
class ShareWebViewController: UIViewController , WKUIDelegate, ShareWebDelegate {
    
    var webView: WKWebView!
    var bean : TrackMain?
    let sharedWeb = ShareWeb()
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        self.title = self.bean?.filename
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBarItem()
        SVProgressHUD.show()
        sharedWeb.delegate = self
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            self.sharedWeb.creatHtml(bean: self.bean)
        }
    }
    func createdZip() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            SVProgressHUD.dismiss()
            let url = URL.init(fileURLWithPath: self.sharedWeb.htmlPath)
            self.webView.loadFileURL(url, allowingReadAccessTo: url.deletingLastPathComponent())
            self.showActionSheet(self.navigationItem.rightBarButtonItems?[0])
        }
    }

    func setupBarItem(){
        let cancelButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                                           target: self,
                                                           action: #selector(self.closeMe(_:)))
        self.navigationItem.leftBarButtonItems = [cancelButton,]
        
        let shareButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action,
                                                           target: self,
                                                           action: #selector(self.shareMenu(_:)))
        self.navigationItem.rightBarButtonItems = [shareButton,]
        
    }
    @objc func closeMe(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @objc func shareMenu(_ sender: Any) {
        self.showActionSheet(sender as! UIBarButtonItem)
    }
    
    func showActionSheet(_ sender: UIBarButtonItem?) {
        let actionMenu = UIAlertController(title: nil, message: "Share", preferredStyle: .actionSheet)
        let shareAction1 = UIAlertAction(title: NSLocalizedString("share_view_zipfile", comment: ""),
                                       style: .default,
                                       handler: { [weak self] (action:UIAlertAction) -> Void in
                                       self?.share1()
        })
        let shareAction2 = UIAlertAction(title: NSLocalizedString("share_view_recordfile", comment: ""),
                                        style: .default,
                                        handler: { [weak self]  (action:UIAlertAction) -> Void in
                                        self?.share2()
        })
        let cancelAction = UIAlertAction(title:
            NSLocalizedString("rec_view_tag_editdialog_cancel", comment: ""),
                                         style: .cancel,
                                         handler: { [weak self]  (action:UIAlertAction) -> Void in
                                            
            })
        actionMenu.addAction(shareAction1)
        actionMenu.addAction(shareAction2)
        actionMenu.addAction(cancelAction)
        actionMenu.modalPresentationStyle = .popover
        if UIDevice.current.userInterfaceIdiom == .pad{
            actionMenu.popoverPresentationController?.sourceView = self.view
            actionMenu.popoverPresentationController?.sourceRect = CGRect.init(x: screenSize.width/2, y: screenSize.height-100, width: 0, height: 0)
            self.present(actionMenu, animated: true, completion: nil)
        }else{
            self.present(actionMenu, animated: true, completion: nil)
        }
    }
    
    func share1() {
        let zipFile = URL.init(fileURLWithPath: sharedWeb.zipPath)
        let title = bean?.filename
        let activityItems = [title, zipFile] as [Any]
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        let excludedActivityTypes = [
            UIActivityType.print,
        ]
        activityVC.excludedActivityTypes = excludedActivityTypes

        if UIDevice.current.userInterfaceIdiom == .pad{
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.popoverPresentationController?.sourceRect = CGRect.init(x: screenSize.width/2, y: screenSize.height-100, width: 0, height: 0)
            self.present(activityVC, animated: true, completion: nil)
        }else{
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func share2() {
        let audioFileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record, filename: bean?.filenamewithextension ?? "")
        let title = bean?.filename
        let activityItems = [title, audioFileURL] as [Any]
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        let excludedActivityTypes = [
            UIActivityType.print,
        ]
        activityVC.excludedActivityTypes = excludedActivityTypes
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.popoverPresentationController?.sourceRect = CGRect.init(x: screenSize.width/2, y: screenSize.height-100, width: 0, height: 0)
            self.present(activityVC, animated: true, completion: nil)
        }else{
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}


