//
//  TrackDetail.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/12.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import Foundation

class RecTagDetail:Codable {
    var rowID : Int = -1
    var id : Int = 0
    var fileName: String = ""
    var detailNo: Int = 0
    var date: String = ""
    
    var type: Int = 0
    var imgname: String = ""
    var detail: String = ""
    var insertDate: String = ""
    
    var TagTime: Double = 0//time
    var url: String = ""//url
    var senderName: String = ""//senderName
    var isIncomingType: Int = 0//isIncoming
    
    
    // イニシャライザ
    init(fileName: String, detailno: Int,date: String,
         type: Int,imgname: String,detail:String,tagtime:Double,
         url:String,senderName:String,isIncoming:Int) {
        
        self.fileName = fileName
        self.detailNo = detailno
        self.date = date
        self.type = type
        self.imgname = imgname
        self.detail = detail
        self.TagTime = tagtime
        self.url = url
        self.senderName = senderName
        self.isIncomingType = isIncoming
    }
    init() {
        
    }
}
