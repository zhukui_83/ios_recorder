//
//  TrackMain.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/12.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import Foundation

class TrackMain:Codable{
    var rowID : Int = -1
    
    var rectype: Int = 0  // 录音格式 0，1，2，3
    var filename: String = ""
    var recexten: String = ""  // 文件扩展名
    
    var filenamewithextension: String {
        return  String.init(format: "%@%@", filename, recexten)
    }
    
    var title: String = ""
    var comment: String = ""

    var datasize: Int = 0
    var duration: Int = 0
    var fileurl:URL? {
        get{
            return  MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record, filename: filenamewithextension)
        }
    }
    
    var coverimgname: String = ""
    var coverimgurl: String = ""
    var author: String = ""
    
    
    // 下记都未使用
    var dataType: String = ""//1,2,3
    var typeName: String = ""//国際、見つからない等
    
    var networking: Bool = false
    var status: Int = 0     //doing
    var strStatus: String = ""
    
    var haveUpdate: Bool = false
    var errorCode: Int = 0
    var errorMsg: String = ""
    var latestDate: String = ""
    var latestStatus: String = ""
    var latestStore: String = ""
    var latestDetail: String = ""
    var latestDetailNo: Int = -1
    var insertDate: String = ""
    var updateDate: String = ""

//    var detailList : Array<TrackDetail> = Array<TrackDetail>()
//    init(filename: String, type: String) {
//        self.filename = filename
//        self.dataType = type
//    }
    init(filename: String, rectype: AudioRecordType) {
        self.filename = filename
        self.rectype = rectype.RecType.rawValue
        self.recexten = rectype.FileExtension
    }
    
    init() {
    }
}

