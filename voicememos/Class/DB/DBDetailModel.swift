//
//  TrackDetailModel.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/12.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import Foundation


let sendname_left = "myrec"
let sendname_sys = "sys"
class DBDetailTable {
    static let shared = DBDetailTable()
    
    let create_sql =
        "create table if not exists DBDetailTable_001("
            + "ID integer primary key autoIncrement,"
            + "fileName text not null default '',"//"fileName":.StringVal,
            + "detailNo INTEGER not null default 0,"//"detailNo":.IntVal,
            + "date text not null default '',"//"date":.StringVal,
            + "type INTEGER not null default 0,"//图片还是文字
            + "imgname text not null default '',"//保存图片信息
            + "detail text not null default '',"//保存文字信息
            + "url text not null default '',"//
            + "tagTime INTEGER not null default 0,"//时间
            + "senderName text not null default '',"//发送者的信息
            + "isIncomingType INTEGER not null default 0,"//
            + "insertDate text not null default '');"
    // テーブル作成
    init() {
        let (tb, err) = SD.existingTables()
        if !tb.contains("DBDetailTable_001") {
            if let err = SD.createTable(sql: create_sql){
                debug_print("--DB: create DBDetailTable_001 error---")
            } else {
                debug_print("--DB: create DBDetailTable_001---")
            }
        }
    }
    
    /**
    INSERT文（追加）
    var add = 自作databaseクラス.add("3urprise") これでdatabaseに"3urprise"と追加される
    */
    func add(_ trackdetail:RecTagDetail) -> Int {
        var result = -1
        trackdetail.insertDate = ComFunc().getNowDateStr()
        if let err = SD.executeChange(sqlStr: "INSERT INTO DBDetailTable_001 (fileName,detailNo,date,type,imgname, detail,insertDate,tagTime,url,senderName,isIncomingType) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
            withArgs: [trackdetail.fileName as AnyObject,trackdetail.detailNo as AnyObject,trackdetail.date as AnyObject,trackdetail.type as AnyObject,trackdetail.imgname as AnyObject,trackdetail.detail as AnyObject,trackdetail.insertDate as AnyObject,
                       trackdetail.TagTime as AnyObject,
                       trackdetail.url as AnyObject,
                       trackdetail.senderName as AnyObject,
                       trackdetail.isIncomingType as AnyObject
                       ]) {
            // there was an error during the insert, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result = -1
            }else{
                // ok
                result = id
            }
        }
        return result
    }
    
    
    
    func updateFileName(_ newfilename:String,filename:String) -> Bool {
        var result:Bool? = false
        if let err = SD.executeChange(sqlStr: "UPDATE DBDetailTable_001 set fileName=? where fileName=?", withArgs: [newfilename as AnyObject,
                            filename as AnyObject,]) {
            
        } else {
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                result=false
            }else{
                result=true
            }
        }
        return result!
    }
    
    /**
    DELETE文
    var del = 自作databaseクラス.delete(Int) これでテーブルのID削除
    */
    func deleteByRowID(_ id:Int) -> Bool {
        if let err = SD.executeChange(sqlStr: "DELETE FROM DBDetailTable_001 WHERE ID = ?", withArgs: [id as AnyObject,]) {
            // there was an error during the insert, handle it here
            return false
        } else {
            // no error, the row was inserted successfully
            return true
        }
    }
    func deleteByfileName(_ fileName:String) -> Bool {
        if let err = SD.executeChange(sqlStr: "DELETE FROM DBDetailTable_001 WHERE fileName = ?", withArgs: [fileName as AnyObject,]) {
            // there was an error during the insert, handle it here
            return false
        } else {
            // no error, the row was inserted successfully
            return true
        }
    }
    func deleteByfileNameAndNo(fileName:String, detailNo:Int) -> Bool {
        if let err = SD.executeChange(sqlStr: "DELETE FROM DBDetailTable_001 WHERE fileName = ? and detailNo = ?", withArgs: [fileName as AnyObject,detailNo as AnyObject]) {
            // there was an error during the insert, handle it here
            return false
        } else {
            // no error, the row was inserted successfully
            return true
        }
    }
    /**
    SELECT文
    var selects = 自作databaseクラス.getAll() これでNSMutableArray型の内容が取得出来る
    */
    func getAll() -> Array<RecTagDetail> {
        var result = Array<RecTagDetail>()
        // 新しい番号から取得する場合は "SELECT * FROM fileNameListTable_001 ORDER BY ID DESC" を使う
        let (resultSet, err) = SD.executeQuery(sqlStr: "SELECT * FROM DBDetailTable_001 ORDER BY detailNo DESC")
        if err != nil {
            // nil
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let trackdetail : RecTagDetail = RecTagDetail()
                    trackdetail.rowID = id
                    
                    let fileName = row["fileName"]?.asString()!
                    let detailNo = row["detailNo"]?.asInt()!
                    let date = row["date"]?.asString()!
                    let type = row["type"]?.asInt()!
                    let imgname = row["imgname"]?.asString()!
                    let detail = row["detail"]?.asString()!
                    let insertDate = row["insertDate"]?.asString()!
                    
                    trackdetail.fileName = fileName!
                    trackdetail.detailNo = detailNo!
                    trackdetail.date = date!
                    trackdetail.type = type!
                    trackdetail.imgname = imgname!
                    trackdetail.detail = detail!
                    trackdetail.insertDate = insertDate!
                    
                    result.append(trackdetail)
                }
            }
        }
        return result
    }
    func getAllByfileName(_ fileName:String) -> Array<RecTagDetail> {
        var result = Array<RecTagDetail>()
        // 新しい番号から取得する場合は "SELECT * FROM fileNameListTable_001 ORDER BY ID DESC" を使う
        let (resultSet, err) = SD.executeQuery(sqlStr: "SELECT * FROM DBDetailTable_001 WHERE fileName = ? ORDER BY tagTime, detailNo ", withArgs: [fileName as AnyObject])
        if err != nil {
            // nil
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let trackdetail : RecTagDetail = RecTagDetail()
                    trackdetail.rowID = id
                    
                    let strfileName = row["fileName"]?.asString()!
                    let detailNo = row["detailNo"]?.asInt()!
                    let date = row["date"]?.asString()!
                    let type = row["type"]?.asInt()!
                    let imgname = row["imgname"]?.asString()!
                    let detail = row["detail"]?.asString()!
                    let insertDate = row["insertDate"]?.asString()!
                    let tagTime = row["tagTime"]?.asDouble()!
                    let url = row["url"]?.asString()!
                    let senderName = row["senderName"]?.asString()!
                    let isIncomingType = row["isIncomingType"]?.asInt()!
                    
                    trackdetail.detailNo = detailNo!
                    trackdetail.date = date!
                    trackdetail.type = type!
                    trackdetail.imgname = imgname!
                    trackdetail.detail = detail!
                    trackdetail.insertDate = insertDate!
                    trackdetail.fileName = strfileName!
                    
                    trackdetail.TagTime = tagTime!
                    trackdetail.url = url!
                    trackdetail.senderName = senderName!
                    trackdetail.isIncomingType = isIncomingType!

                    result.append(trackdetail)
                }
            }
        }
        return result
    }
    
    func getMaxDetailNoWithfileName(_ fileName:String) -> Int {
        var ret_detailno = 0
        // 新しい番号から取得する場合は "SELECT * FROM fileNameListTable_001 ORDER BY ID DESC" を使う
        let (resultSet, err) = SD.executeQuery(sqlStr: "SELECT * FROM DBDetailTable_001 WHERE fileName = ? ORDER BY detailNo ", withArgs: [fileName as AnyObject])
        if err != nil {
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let strfileName = row["fileName"]?.asString()!
                    let detailNo = row["detailNo"]?.asInt()!
                    if detailNo! > ret_detailno{
                        ret_detailno = detailNo!
                    }
                }
            }
        }
        return ret_detailno
    }
    
    
    //MARK: - Retore
    func retoreData( datas:Array<RecTagDetail>) -> Bool {
        for data in datas{
            add(data)
        }
        return true
    }
    
    //MARK: - delete all
    func clearDB() -> Bool{
        if let err = SD.executeChange(sqlStr: "DELETE FROM DBDetailTable_001 ", withArgs: []) {
            return false
        } else {
            return true
        }
    }
}
