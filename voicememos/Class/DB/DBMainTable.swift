//
//  MYQRMainModel.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/12.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import Foundation


class DBMainTable {
    
    static let shared = DBMainTable()
    let create_sql =
        "create table if not exists MainTable_001("
            + "ID integer primary key autoIncrement,"
            + "title text not null default '',"//"title":.StringVal, ..not null default ''
            + "comment text not null default '',"//"comment":.StringVal,
            
            + "filename text not null default '',"//作为主key
            + "rectype INTEGER not null default 0,"//保存文件的类型
            + "recexten text not null default '',"//为文件的扩展名，.mp3
            
            + "duration INTEGER not null default 0,"//保存文件的信息
            + "datasize INTEGER not null default 0,"//保存文件的信息
            
            + "coverimgname text not null default '',"//for bak
            + "coverimgurl text not null default '',"
            + "author text not null default '',"
            
            + "dataType text not null default '',"//下面都没有使用，今后再说
            + "typeName text not null default '',"//

            + "codeType text not null default '',"//
            + "codeName text not null default '',"//
            
            + "openType text not null default '',"
            + "openName text not null default '',"
            
            + "bk1 text not null default '',"//
            + "bk2 text not null default '',"//
            + "bk3 text not null default '',"//
            + "bk4 text not null default '',"//
            + "bk5 text not null default '',"//
            
            + "networking BOOL not null default false,"//"networking":.BoolVal,
            + "status INTEGER not null default 0,"//"status":.IntVal,
            + "haveUpdate BOOL not null default false,"//"haveUpdate":.BoolVal,
            + "errorCode INTEGER not null default 0,"//"errorCode":.IntVal,
            + "errorMsg text not null default '',"//"errorMsg":.StringVal,
            + "latestDate text not null default '',"//"latestDate":.StringVal,
            + "latestStatus text not null default '',"//"latestStatus":.StringVal,
            + "latestStore text not null default '',"//"latestStore":.StringVal,
            + "latestDetail text not null default '',"//"latestDetail":.StringVal,
            + "latestDetailNo INTEGER not null default 0,"//"latestDetailNo":.IntVal,
            + "insertDate text not null default '',"
            + "updateDate text not null default '');"

    // テーブル作成
    init() {
        let (tb, err) = SD.existingTables()
        if !tb.contains( "MainTable_001") {
            if let err = SD.createTable(sql: create_sql){
                debug_print("--DB: create MainTable_001 error---")
            } else {
                debug_print("--DB: create MainTable_001---")
            }
        }
        if let i: Int = UserDefaults.standard.integer(forKey: "MYQRMainModel_Version"){
            if(i < 15){
                let addcol_sql = "alter table MainTable_001 add column title text not null default '';"
                if let err = SD.executeChange( sqlStr:addcol_sql, withArgs: []){
                    print(err)
                }else{
                    debug_print("--DB: update ok---")
                    UserDefaults.standard.set(15, forKey: "MYQRMainModel_Version")
                }
            }
            if(i < 16){
                let updatecol_sql = "update MainTable_001 set title = '' where title is null;"
                if let err = SD.executeChange( sqlStr:updatecol_sql, withArgs: []){
                    print(err)
                }else{
                    debug_print("--DB: update ok---")
                    UserDefaults.standard.set(16, forKey: "MYQRMainModel_Version")
                }
            }
        }
    }
    
    /**
    INSERT文（追加）
    var add = 自作databaseクラス.add("3urprise") これでdatabaseに"3urprise"と追加される
    */
    func add(_ trackmain:TrackMain , restoreflg:Bool = false) -> String {
        var result:String? = nil
        if restoreflg == false {
            trackmain.insertDate = comfunc.getNowDateStr()
            trackmain.updateDate = comfunc.getNowDateStr()
        }
        if let err = SD.executeChange(sqlStr: "INSERT INTO MainTable_001 (filename,dataType,typeName,rectype,recexten,duration,datasize,networking,status,comment,title,haveUpdate,errorCode,errorMsg,latestDate,latestStatus,latestStore,latestDetail,latestDetailNo,insertDate,updateDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            withArgs: [trackmain.filename as AnyObject,
                       trackmain.dataType as AnyObject,
                       trackmain.typeName as AnyObject,
                       trackmain.rectype as AnyObject,
                       trackmain.recexten as AnyObject,
                       trackmain.duration as AnyObject,
                       trackmain.datasize as AnyObject,
                       trackmain.networking as AnyObject,trackmain.status as AnyObject,trackmain.comment as AnyObject,trackmain.title as AnyObject,trackmain.haveUpdate as AnyObject,trackmain.errorCode as AnyObject,trackmain.errorMsg as AnyObject,trackmain.latestDate as AnyObject,trackmain.latestStatus as AnyObject,trackmain.latestStore as AnyObject,trackmain.latestDetail as AnyObject,trackmain.latestDetailNo as AnyObject,trackmain.insertDate as AnyObject,trackmain.updateDate as AnyObject,]) {
            // there was an error during the insert as AnyObject as AnyObject, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result="-1"
            }else{
                // ok
                result = String(id)
            }
        }
        return result!
    }
    
    func update(_ trackmain:TrackMain,restoreflg:Bool = false) -> Bool {
        var result:Bool? = false
        if restoreflg == false {
            trackmain.updateDate = ComFunc().getNowDateStr()
        }
        if let err = SD.executeChange(sqlStr: "UPDATE MainTable_001 set dataType=?,typeName=?,rectype=?,recexten=?,duration=?,datasize=?,status=?,networking=?,comment=?,title=?,haveUpdate=?,errorCode=?,errorMsg=?,latestDate=?,latestStatus=?,latestStore=?,latestDetail=?,latestDetailNo=?,updateDate=? where filename=?",
            withArgs: [trackmain.dataType ,
                       trackmain.typeName ,
                       trackmain.rectype as AnyObject,
                       trackmain.recexten as AnyObject,
                       trackmain.duration as AnyObject,
                       trackmain.datasize as AnyObject,
                       trackmain.status as AnyObject,
                       trackmain.networking as AnyObject,
                       trackmain.comment as AnyObject,trackmain.title as AnyObject,trackmain.haveUpdate as AnyObject,trackmain.errorCode as AnyObject,trackmain.errorMsg as AnyObject,trackmain.latestDate as AnyObject,trackmain.latestStatus as AnyObject,trackmain.latestStore as AnyObject,trackmain.latestDetail as AnyObject,trackmain.latestDetailNo as AnyObject,trackmain.updateDate as AnyObject,trackmain.filename as AnyObject,]) {
                // there was an error during the insert, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result=false
            }else{
                // ok
                result=true
            }
        }
        return result!
    }
    func updateByID(_ trackmain:TrackMain) -> Bool {
        var result:Bool? = false
        trackmain.updateDate = ComFunc().getNowDateStr()
        if let err = SD.executeChange(sqlStr: "UPDATE MainTable_001 set filename=?,rectype=?,recexten=?,duration=?,datasize=?,dataType=?,typeName=?,networking=?,status=?,comment=?,title=?,haveUpdate=?,errorCode=?,errorMsg=?,latestDate=?,latestStatus=?,latestStore=?,latestDetail=?,latestDetailNo=?,updateDate=? where ID=?",
            withArgs: [trackmain.filename as AnyObject,
                       trackmain.rectype as AnyObject,
                       trackmain.recexten as AnyObject,
                       trackmain.duration as AnyObject,
                       trackmain.datasize as AnyObject,
                       trackmain.dataType as AnyObject,trackmain.typeName as AnyObject,trackmain.networking as AnyObject,trackmain.status as AnyObject,trackmain.comment as AnyObject,trackmain.title as AnyObject,trackmain.haveUpdate as AnyObject,trackmain.errorCode as AnyObject,trackmain.errorMsg as AnyObject,trackmain.latestDate as AnyObject as AnyObject,trackmain.latestStatus as AnyObject as AnyObject,trackmain.latestStore as AnyObject as AnyObject,trackmain.latestDetail as AnyObject as AnyObject,trackmain.latestDetailNo as AnyObject as AnyObject,trackmain.updateDate as AnyObject as AnyObject,trackmain.rowID as AnyObject as AnyObject,]) {
                // there was an error during the insert, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result=false
            }else{
                // ok
                result=true
            }
        }
        return result!
    }
    
    func updateStatus(_ trackmain:TrackMain) -> Bool {
        var result:Bool? = false
        trackmain.updateDate = ComFunc().getNowDateStr()
        if let err = SD.executeChange(sqlStr: "UPDATE MainTable_001 set dataType=?,typeName=?,networking=?,status=?,comment=?,title=?,haveUpdate=?,errorCode=?,errorMsg=?,latestDate=?,latestStatus=?,latestStore=?,latestDetail=?,latestDetailNo=?,updateDate=? where filename=?",
            withArgs: [trackmain.dataType as AnyObject,trackmain.typeName as AnyObject,trackmain.networking as AnyObject,trackmain.status as AnyObject,trackmain.comment as AnyObject,trackmain.title as AnyObject,trackmain.haveUpdate as AnyObject,trackmain.errorCode as AnyObject,trackmain.errorMsg as AnyObject,trackmain.latestDate as AnyObject,trackmain.latestStatus as AnyObject,trackmain.latestStore as AnyObject,trackmain.latestDetail as AnyObject,trackmain.latestDetailNo as AnyObject,trackmain.updateDate as AnyObject,trackmain.filename as AnyObject,]) {
                // there was an error during the insert, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result=false
            }else{
                // ok
                result=true
            }
        }
        return result!
    }
    func setReaded(_ strfilename: String) -> Bool {
        var result:Bool? = false
        
        if let err = SD.executeChange(sqlStr: "UPDATE MainTable_001 set haveUpdate=? where filename=?",
            withArgs: [false ,strfilename]) {
                // there was an error during the insert, handle it here
        } else {
            // no error, the row was inserted successfully
            // lastInsertedRowID = 直近の INSERT 文でデータベースに追加された行の ID を返す
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                // err
                result=false
            }else{
                // ok
                result=true
            }
        }
        return result!
    }
    /**
    DELETE文
    var del = 自作databaseクラス.delete(Int) これでテーブルのID削除
    */
    func delete(_ id:Int) -> Bool {
        if let err = SD.executeChange(sqlStr: "DELETE FROM MainTable_001 WHERE ID = ?", withArgs: [id as AnyObject]) {
            // there was an error during the insert, handle it here
            return false
        } else {
            // no error, the row was inserted successfully
            return true
        }
    }
    
    /**
    SELECT文
    var selects = 自作databaseクラス.getAll() これでNSMutableArray型の内容が取得出来る
    */
    func getAll() -> Array<TrackMain> {
        var result = Array<TrackMain>()
        // 新しい番号から取得する場合は "SELECT * FROM filenameListTable_001 ORDER BY ID DESC" を使う
        let (resultSet, err) = SD.executeQuery(sqlStr: "SELECT * FROM MainTable_001 WHERE status >= 0 ORDER BY ID DESC")
        if err != nil {
            // nil
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let trackmain : TrackMain = TrackMain()
                    
                    trackmain.rowID = id
                    let filename = row["filename"]?.asString()!
                    let rectype = row["rectype"]?.asInt()!
                    let recexten = row["recexten"]?.asString()!

                    
                    let datasize = row["datasize"]?.asInt()!
                    let duration = row["duration"]?.asInt()!
                    let dataType = (row["dataType"]?.asString()!)!
                    let typeName = row["typeName"]?.asString()!
                
                    let status = row["status"]?.asInt()!
                    let comment = row["comment"]?.asString()!
                
                    var title : String = ""
                    if let row_title = row["title"]{
                        if let x = row_title.asString() {
                            title = x
                        } else {
//                            print("value is nil")
                        }
                    }
                    
                    let haveUpdate = row["haveUpdate"]?.asBool()!
                    let errorCode = row["errorCode"]?.asInt()!
                    let errorMsg = row["errorMsg"]?.asString()!
                    let latestDate = row["latestDate"]?.asString()!
                    let latestStatus = row["latestStatus"]?.asString()!
                    let latestStore = row["latestStore"]?.asString()!
                    let latestDetail = row["latestDetail"]?.asString()!
                    let latestDetailNo = row["latestDetailNo"]?.asInt()!
                    let insertDate = row["insertDate"]?.asString()!
                    let updateDate = row["updateDate"]?.asString()!
                    
                    trackmain.filename = filename!
                    trackmain.rectype = rectype!
                    trackmain.recexten = recexten!
                    trackmain.title = title
                    trackmain.comment = comment!
                    
                    trackmain.datasize = datasize!
                    trackmain.duration = duration!
                    
                    trackmain.dataType = dataType
                    trackmain.typeName = typeName!
                    if let networking = row["networking"]?.asBool(){
                        trackmain.networking = networking
                    }
                    trackmain.status = status!

                    trackmain.haveUpdate = haveUpdate!
                    trackmain.errorCode = errorCode!
                    trackmain.errorMsg = errorMsg!
                    trackmain.latestDate = latestDate!
                    trackmain.latestStatus = latestStatus!
                    trackmain.latestStore = latestStore!
                    trackmain.latestDetail = latestDetail!
                    trackmain.latestDetailNo = latestDetailNo!
                    trackmain.insertDate = insertDate!
                    trackmain.updateDate = updateDate!
                    
                    result.append(trackmain)
                }
            }
        }
        return result
    }
    
    func getCountByStatus(_ intStatus : Int) -> Int {
        //var strSQL : String
        if(intStatus == ComFunc.TrackList_all){
            let strSQL = "SELECT count(*) FROM MainTable_001 WHERE status >= 0  ORDER BY ID DESC"
            let (resultSet, err) = SD.executeQuery(sqlStr: strSQL )
            if err != nil {
                return 0;
            } else {
                var nRows : Int = 0
                for row in resultSet {
//                    let x = row["count(*)"]
                    nRows = (row["count(*)"]?.asInt()!)!
                }
                return nRows
            }
        }else{
            let strSQL = "SELECT count(*) FROM MainTable_001 WHERE status = ?  ORDER BY ID DESC"
            let (resultSet, err) = SD.executeQuery(sqlStr: strSQL , withArgs: [intStatus as AnyObject])
            if err != nil {
                return 0;
            } else {
                var nRows : Int = 0
                for row in resultSet {
                    nRows = (row["count(*)"]?.asInt()!)!
                }
                return nRows
            }
        }
    }
    
    func getAllByStatus(_ intStatus : Int) -> Array<TrackMain> {
        var result = Array<TrackMain>()
        // 
        let strSQL : String = "SELECT * FROM MainTable_001 WHERE status = ?  ORDER BY ID DESC"
        let (resultSet, err) = SD.executeQuery(sqlStr: strSQL , withArgs: [intStatus as AnyObject ])
        if err != nil {
            // nil
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let trackmain : TrackMain = TrackMain()
                    
                    trackmain.rowID = id
                    let filename = row["filename"]?.asString()!
                    let rectype = row["rectype"]?.asInt()!
                    let recexten = row["recexten"]?.asString()!

                    let datasize = row["datasize"]?.asInt()!
                    let duration = row["duration"]?.asInt()!
                    
                    let dataType = (row["dataType"]?.asString()!)!
                    let typeName = row["typeName"]?.asString()!
                    
                    let status = row["status"]?.asInt()!
                    let comment = row["comment"]?.asString()!
                    
                    var title : String = ""
                    if let row_title = row["title"]{
                        if let x = row_title.asString() {
                            title = x
                        } else {
                             print("value is nil")
                        }
                    }
                    let haveUpdate = row["haveUpdate"]?.asBool()!
                    let errorCode = row["errorCode"]?.asInt()!
                    let errorMsg = row["errorMsg"]?.asString()!
                    let latestDate = row["latestDate"]?.asString()!
                    let latestStatus = row["latestStatus"]?.asString()!
                    let latestStore = row["latestStore"]?.asString()!
                    let latestDetail = row["latestDetail"]?.asString()!
                    let latestDetailNo = row["latestDetailNo"]?.asInt()!
                    let insertDate = row["insertDate"]?.asString()!
                    let updateDate = row["updateDate"]?.asString()!
                    
                    trackmain.filename = filename!
                    trackmain.rectype = rectype!
                    trackmain.recexten = recexten!
                    
                    trackmain.datasize = datasize!
                    trackmain.duration = duration!
                    
                    trackmain.dataType = dataType
                    trackmain.typeName = typeName!
                    if let networking = row["networking"]?.asBool(){
                        trackmain.networking = networking
                    }
                    trackmain.status = status!
                    trackmain.comment = comment!
                    trackmain.title = title
                    trackmain.haveUpdate = haveUpdate!
                    trackmain.errorCode = errorCode!
                    trackmain.errorMsg = errorMsg!
                    trackmain.latestDate = latestDate!
                    trackmain.latestStatus = latestStatus!
                    trackmain.latestStore = latestStore!
                    trackmain.latestDetail = latestDetail!
                    trackmain.latestDetailNo = latestDetailNo!
                    trackmain.insertDate = insertDate!
                    trackmain.updateDate = updateDate!
                    
                    result.append(trackmain)
                }
            }
        }
        return result
    }
    
    func getAllByfilename(_ filename:String) -> TrackMain? {
        var result:TrackMain? = nil
        let sqlstr = String.init(format: "SELECT * FROM MainTable_001 WHERE filename = '%@' ORDER BY ID DESC", filename)
        print(sqlstr)
        // 新しい番号から取得する場合は "SELECT * FROM filenameListTable_001 ORDER BY ID DESC" を使う
        let (resultSet, err) = SD.executeQuery(sqlStr: sqlstr, withArgs: [])
        if err != nil {
            return nil
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let trackmain : TrackMain = TrackMain()
                    
                    trackmain.rowID = id
                    let filename = row["filename"]?.asString()!
                    let rectype = row["rectype"]?.asInt()!
                    let recexten = row["recexten"]?.asString()!

                    let datasize = row["datasize"]?.asInt()!
                    let duration = row["duration"]?.asInt()!
                    let dataType = (row["dataType"]?.asString()!)!
                    let typeName = row["typeName"]?.asString()!
                    let status = row["status"]?.asInt()!
                    let comment = row["comment"]?.asString()!
                    var title : String = ""
                    if let row_title = row["title"]{
                        if let x = row_title.asString() {
                            title = x
                        } else {
                            print("value is nil")
                        }
                    }
                    let haveUpdate = row["haveUpdate"]?.asBool()!
                    let errorCode = row["errorCode"]?.asInt()!
                    let errorMsg = row["errorMsg"]?.asString()!
                    let latestDate = row["latestDate"]?.asString()!
                    let latestStatus = row["latestStatus"]?.asString()!
                    let latestStore = row["latestStore"]?.asString()!
                    let latestDetail = row["latestDetail"]?.asString()!
                    let latestDetailNo = row["latestDetailNo"]?.asInt()!
                    let insertDate = row["insertDate"]?.asString()!
                    let updateDate = row["updateDate"]?.asString()!
                    
                    trackmain.filename = filename!
                    trackmain.rectype = rectype!
                    trackmain.recexten = recexten!
                    
                    trackmain.datasize = datasize!
                    trackmain.duration = duration!

                    trackmain.dataType = dataType
                    trackmain.typeName = typeName!
                    if let networking = row["networking"]?.asBool(){
                        trackmain.networking = networking
                    }
                    trackmain.status = status!
                    trackmain.comment = comment!
                    trackmain.title = title
                    trackmain.haveUpdate = haveUpdate!
                    trackmain.errorCode = errorCode!
                    trackmain.errorMsg = errorMsg!
                    trackmain.latestDate = latestDate!
                    trackmain.latestStatus = latestStatus!
                    trackmain.latestStore = latestStore!
                    trackmain.latestDetail = latestDetail!
                    trackmain.latestDetailNo = latestDetailNo!
                    trackmain.insertDate = insertDate!
                    trackmain.updateDate = updateDate!
                    
                    result = trackmain
                }
            }
        }
        return result
    }
//
//    //MARK:
//    func retoreData( datas : Array<TrackMain> , trackdetail: TrackDetailModel) -> Bool {
//        for data in datas{
//            if let trackmain = getAllByfilename(data.filename){
//                self.update(data,restoreflg: true)
//                trackdetail.deleteByfilename(data.filename)
//            }else{
//                self.add(data,restoreflg: true)
//            }
//        }
//        return true
//    }
    
    //MARK: - delete all
    func clearDB() -> Bool{
        if let err = SD.executeChange(sqlStr: "DELETE FROM MainTable_001 ", withArgs: []) {
            return false
        } else {
            return true
        }
    }
}
