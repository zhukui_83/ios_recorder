//
//  ViewController.m
//  GCDWebServerDemo
//
//  Created by shapp on 2017/8/23.
//  Copyright © 2017年 shapp. All rights reserved.
//

#define Screen_W [UIScreen mainScreen].bounds.size.width
#define Screen_H [UIScreen mainScreen].bounds.size.height

#import "GCDViewController.h"
#import "GCDWebUploader.h"
#import "SJXCSMIPHelper.h"

@interface GCDViewController () <GCDWebUploaderDelegate, UITableViewDelegate, UITableViewDataSource>
{
    GCDWebUploader * _webServer;
}

@property (weak, nonatomic) IBOutlet UISwitch *OnOffSwitch;
/* 显示ip地址 */
@property (nonatomic, weak) IBOutlet UILabel *showIpLabel;
/* fileTableView */
@property (nonatomic, weak) IBOutlet UITableView *fileTableView;
/* fileArray */
@property (nonatomic, strong) NSMutableArray *fileArray;
@end

@implementation GCDViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
    [self initFileData];
    self.showIpLabel.text = NSLocalizedString(@"GCDWebServer not running!", nil);
    

//    self.navigationItem.isTranslucent = false
//    self.navigationController.navigationBar.barTintColor = GlobalHeadColor
////    self.UINavigationController.UINavigationBar.
//    UINavigationBar.appearance().barStyle = UIBarStyle.default
    // UIBarButtonItemに表示文字列を渡して、インスタンス化します。
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]
                            initWithTitle:@"戻る"
                            style:UIBarButtonItemStylePlain
                            target:self
                            action:@selector(back:)];
    // ナビゲーションバーの左側に追加する。
    self.navigationItem.leftBarButtonItem = btn;
}

    
    
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor =  [UIColor whiteColor];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [self serverStop];
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.textLabel.text = self.fileArray[indexPath.row];
    return cell;
}

#pragma mark - <GCDWebUploaderDelegate>
- (void)webUploader:(GCDWebUploader*)uploader didUploadFileAtPath:(NSString*)path {
    NSLog(@"[UPLOAD] %@", path);
    
//    self.showIpLabel.hidden = YES;
    
    NSString *string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    self.fileArray = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:string error:nil]];
    
    [self.fileTableView reloadData];
}

- (void)webUploader:(GCDWebUploader*)uploader didMoveItemFromPath:(NSString*)fromPath toPath:(NSString*)toPath {
    NSLog(@"[MOVE] %@ -> %@", fromPath, toPath);
}

- (void)webUploader:(GCDWebUploader*)uploader didDeleteItemAtPath:(NSString*)path {
    NSLog(@"[DELETE] %@", path);
    NSString *string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    self.fileArray = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:string error:nil]];
    [self.fileTableView reloadData];
}

- (void)webUploader:(GCDWebUploader*)uploader didCreateDirectoryAtPath:(NSString*)path {
    NSLog(@"[CREATE] %@", path);
}
- (IBAction)serverOnOffChanged:(id)sender {
    bool onoff = [sender isOn];
    if(onoff == true){
        [self serverStart];
    }else{
        [self serverStop];
    }
}


- (void) serverStart{
    // 文件存储位置
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"serverStart 文件存储位置 : %@", documentsPath);
    // 创建webServer，设置根目录
    _webServer = [[GCDWebUploader alloc] initWithUploadDirectory:documentsPath];
    // 设置代理
    _webServer.delegate = self;
    _webServer.allowHiddenItems = YES;
    // 限制文件上传类型
    // 暂时不限制
    //    _webServer.allowedFileExtensions = @[@"doc", @"docx", @"xls", @"xlsx", @"txt", @"pdf"];
    // 设置网页标题
    _webServer.title = @"MyBox的demo";
    // 设置展示在网页上的文字(开场白)
    _webServer.prologue = @"MyBox的WIFI管理平台";
    // 设置展示在网页上的文字(收场白)
    _webServer.epilogue = @"MyBox制作";
    if ([_webServer start]) {
        self.showIpLabel.hidden = NO;
        self.showIpLabel.text = [NSString stringWithFormat:@"请在网页输入这个地址  http://%@:%zd/", [SJXCSMIPHelper deviceIPAdress], _webServer.port];
    } else {
        self.showIpLabel.text = NSLocalizedString(@"GCDWebServer not running!", nil);
    }
}
- (void) serverStop{
    [_webServer stop];
    _webServer = nil;
    [_OnOffSwitch setOn:false];
    self.showIpLabel.text = NSLocalizedString(@"GCDWebServer not running!", nil);
    NSLog(@"serverStop -----");
}
#pragma mark - <懒加载>
//- (UILabel *)showIpLabel {
//    if (!_showIpLabel) {
//        UILabel *lb = [[UILabel alloc] init];
//
//        lb.bounds = CGRectMake(0, 0, Screen_W, 20);
////        lb.center = CGPointMake(Screen_W * 0.5, Screen_H * 0.5);
//        lb.center = CGPointMake(Screen_W * 0.5, 100);
//        lb.textColor = [UIColor darkGrayColor];
//        lb.textAlignment = NSTextAlignmentCenter;
//        lb.font = [UIFont systemFontOfSize:13.0];
//        lb.numberOfLines = 0;
//
//        [self.view addSubview:lb];
//        _showIpLabel = lb;
//    }
//    return _showIpLabel;
//}
- (void) initUI{
    _showIpLabel.textColor = [UIColor darkGrayColor];
    _showIpLabel.textAlignment = NSTextAlignmentCenter;
    _showIpLabel.font = [UIFont systemFontOfSize:13.0];
    _showIpLabel.numberOfLines = 0;
    
    // 设置代理
    _fileTableView.delegate = self;
    // 设置数据源
    _fileTableView.dataSource = self;
    // 清除表格底部多余的cell
    _fileTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [_fileTableView reloadData];
}
- (void) initFileData{
    NSString *string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    self.fileArray = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:string error:nil]];
    [_fileTableView reloadData];
}

//- (UITableView *)fileTableView {
//    if (!_fileTableView) {
//        UITableView *tv = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, Screen_W, Screen_H) style:UITableViewStylePlain];
//
//        // 设置代理
//        tv.delegate = self;
//        // 设置数据源
//        tv.dataSource = self;
//        // 清除表格底部多余的cell
//        tv.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//
//        [self.view addSubview:tv];
//        _fileTableView = tv;
//    }
//    return _fileTableView;
//}
- (NSMutableArray<NSString *> *)fileArray {
    if (!_fileArray) {
        _fileArray = [NSMutableArray array];
    }
    return _fileArray;
}
- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
