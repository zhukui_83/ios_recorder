//
//  MyNaviViewController.swift
//  MediaBox
//
//  Created by Zhu Kui on 2017/12/15.
//  Copyright © 2017年 Zhu Kui. All rights reserved.
//

import UIKit

class GCDWebNaviViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let storyboard = UIStoryboard(name: "GCDWebServer", bundle: nil)
//        let vc: GCDViewController = storyboard.instantiateViewController(withIdentifier: "GCDViewController") as! GCDViewController
//        
//        self.addChildViewController(vc)
//        
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
//        UINavigationBar.appearance().barStyle = UIBarStyle.default
//        self.setBarItem()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    func setBarItem(){
        let closeButton = ComUI.getCloseButton()
        closeButton.addTarget(self, action: #selector(self.coloseView(_:)),
                              for: UIControlEvents.touchUpInside)
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: closeButton),]
    }
    @objc func coloseView(_ sender: AnyObject){
        self.dismiss(animated: true) {
        }
    }

}
