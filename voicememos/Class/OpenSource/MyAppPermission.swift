//
//  MyAppPermission.swift
//  voicememos
//
//  Created by ksymac on 2018/8/4.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import Foundation
import AVFoundation

class MyAppPermission: NSObject {
    static let shared = MyAppPermission()
    
    //check  microphone in the app, and add the appropriate key to the info.plist "Privacy-MicrophoneUsageDescription
    func askMicroPhonePermission(completion: @escaping (_ success: Bool)-> Void) {
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            completion(true)
        case AVAudioSessionRecordPermission.denied:
            completion(false) //show alert if required
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    completion(true)
                } else {
                    completion(false) // show alert if required
                }
            })
        default:
            completion(false)
        }
    }
    
    func requetSettingForAuth(){
        if let url = URL.init(string: UIApplicationOpenSettingsURLString){
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: [:]) { (b) in
                }
            }
        }
    }
}
    

