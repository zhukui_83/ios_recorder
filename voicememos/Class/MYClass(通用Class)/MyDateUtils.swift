//
//  DataUtils.swift
//  voicememos
//
//  Created by ksymac on 2018/3/10.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

class MyDateUtils: NSObject {
    
    //获取现在的时间
    class func getStrTimeInterval(timeInterval: TimeInterval) -> String {
        let ti = NSInteger(timeInterval)
        let ms = Int((CGFloat(timeInterval).truncatingRemainder(dividingBy: 1)) * 10)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        if hours > 0 {
            return NSString(format: "%0.2d:%0.2d:%0.2d.%0.1d",hours,minutes,seconds,ms) as String
        }else{
            return NSString(format: "%0.2d:%0.2d.%0.1d",minutes,seconds,ms) as String
        }
    }
    class func stringFromTimeInterval(time: Double) -> String {
        let h = Int(time) / 360
        let xtime:Int = Int(time) % 360
        let s = xtime % 60
        let m = xtime / 60
        let ms = Int(time.truncatingRemainder(dividingBy: 1) * 10)
        if h > 0{
            return String(format: "%02d:%02d:%02d.%0.1d",h,m,s,ms)
        }else{
            return String(format: "%02d:%02d.%0.1d", m,s,ms)
        }
    }

    class func stringFromDate(date: Date, format: String) -> String {
        var formatter = DateFormatter()
        let jaLocale = Locale(identifier: "ja_JP")
        formatter.locale = jaLocale
        formatter.dateFormat = format
//        var now = Date()
//        print(formatter.string(from:now))
        return formatter.string(from:date)
    }
}


