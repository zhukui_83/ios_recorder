//
//  MyGuild.swift
//  voicememos
//
//  Created by ksymac on 2018/7/15.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

class MySampleFileManage {
    static let shared = MySampleFileManage()
    let dbMainTb = DBMainTable.shared//model
    let dbStatusTb = DBStatusTable.shared//model
    let dbDetailTb = DBDetailTable.shared//model
    
    
    func createSampleFile(){
        let b = UserDefaults.standard.object(forKey: "Have_CreateSample_File") as? Bool
        if b == nil || b == false{
            copyMusicFile()
            createMainBean()
            createMsg()
        }
        UserDefaults.standard.set(true, forKey:  "Have_CreateSample_File")
    }
    let sample_filename =  "sample"
    let sample_recordType: AudioRecordType = AudioRecorderManager.shared.getAudioRecordType(value: .mp3)
    let sample_title = "8 secrets of success"
    let sample_desc = "Richard St. John"
    
    

    
    func copyMusicFile(){
        let file_url = Bundle.main.url(forResource: "sample", withExtension: ".mp3")
        var dir_url = MyClassFile.sharedInstance.getDirectory(type: .Record)
        dir_url.appendPathComponent("sample.mp3")
        let manager = FileManager()
        do {
            try manager.copyItem(atPath: (file_url?.path)!, toPath: dir_url.path)
        }catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    func createMainBean(){
        WriteToDB_StartInfo()
    }
    func createMsg(){
        if let path = Bundle.main.path(forResource: "sample", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>,
                    let array = jsonResult["detail"] as? [Dictionary<String, AnyObject>] {
                    print(jsonResult)
                    print(array)
                        for item in array{
                            print(item)
                            if let type = item["type"] as? Int{
                                if type == 0{
                                    let model = ZukMessageModel.init(.text, msgincoming: .left)
                                    model.No = item["no"] as! Int
                                    model.body = item["text"] as! String
                                    model.senderName = sendname_sys
                                    model.TagTime = item["time"] as! Double
                                    WriteToDB_AddTag(fileName: sample_filename,model: model)
                                }else{
                                    if let imgname = item["image"] as? String,
                                        let img = UIImage.init(named: imgname){
                                        
                                        let model = ZukMessageModel.createImageModel(img: img, detailno:  item["no"] as! Int, time: item["time"] as! Double)
                                        model.senderName = sendname_sys
                                        WriteToDB_AddTag(fileName: sample_filename,model: model)
                                    }
                                }
                            }
                        }
                }
            } catch {
            }
        }
    }
    

    func WriteToDB_StartInfo(){
        let bean = TrackMain.init(filename:sample_filename,
                                  rectype:sample_recordType)
        bean.title = sample_title
        bean.comment = sample_desc
        bean.datasize = Int(Int(1.7 * 1014 * 1024))
        bean.duration = 102
        dbMainTb.add(bean)
    }
    
    // MARK: Detail DB -----
    func WriteToDB_AddTag(fileName: String, model:ZukMessageModel){
        let detailbean = RecTagDetail.init()
        detailbean.type = model.MsgType.rawValue
        detailbean.fileName = fileName
        detailbean.detailNo = model.No
        if let body = model.body{
            detailbean.detail = body
        }
        if let imgname = model.ImageName{
            detailbean.imgname = imgname
        }
        
        detailbean.TagTime = model.TagTime //time
        if let url = model.url{
            detailbean.url = url
        }
        detailbean.senderName = model.senderName//senderName
        detailbean.isIncomingType = model.isIncomingType.rawValue//isIncoming
        dbDetailTb.add(detailbean)
    }
}
