//
//  MyBoxFile.swift
//  MediaBox
//
//  Created by Zhu Kui on 2018/01/25.
//  Copyright © 2018年 Zhu Kui. All rights reserved.
//

import UIKit

enum DirectoryType:String {
    case Document = "Document"
    case Video = "Video"
    case Music = "Music"
    case Epub = "Epub"
    case VoiceMemos = "VoiceMemos"
    case Record = "VoiceMemos/Record"
    case Image = "VoiceMemos/Image"
    case RecordPdf = "VoiceMemos/Pdf"
    case RecordJson = "VoiceMemos/Json"
    case RecordXml = "VoiceMemos/Xml"
    case Nothing = ""
}

class MyClassFile: NSObject {

    class var sharedInstance : MyClassFile {
        struct Static {
            static let instance : MyClassFile = MyClassFile()
        }
        return Static.instance
    }
    func getDirectory(type: DirectoryType) -> URL{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        if type != DirectoryType.Nothing{
            let dataPath = documentsDirectory.appendingPathComponent(type.rawValue)
            return dataPath
        }else{
            return documentsDirectory as! URL
        }
    }
    func createDirectory(docPath: URL)  {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: docPath.path) {
            do {
                try fileManager.createDirectory(atPath: docPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch  let error as NSError{
                NSLog("Couldn't create document directory")
                print(error.localizedDescription);
            }
        }
    }
    
    func getFileUrlWithFileName(type: DirectoryType,filename: String) -> URL?{
        let url = self.getDirectory(type: type)
        return url.appendingPathComponent(filename)
    }
    
    
    //Mark:MyBox的初始化文件夹
    func createInitFolder(){
//        self.createDirectory(docPath: self.getDirectory(type: .Document))
//        self.createDirectory(docPath: self.getDirectory(type: .Video))
//        self.createDirectory(docPath: self.getDirectory(type: .Music))
//        self.createDirectory(docPath: self.getDirectory(type: .Epub))
        self.createDirectory(docPath: self.getDirectory(type: .VoiceMemos))
        self.createDirectory(docPath: self.getDirectory(type: .Record))
        self.createDirectory(docPath: self.getDirectory(type: .Image))
        self.createDirectory(docPath: self.getDirectory(type: .RecordPdf))
    }
    
    
    func getStrWithNow()->String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmssSSS"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
//        let interval = date.timeIntervalSince1970
        return dateString
    }
    
}
