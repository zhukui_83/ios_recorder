//
//  TitleEditViewController.swift
//  AlertViewSample
//
//  Created by Zhu Kui on 2018/03/15.
//  Copyright © 2018年 Interrupt LLC. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import SVProgressHUD

protocol TitleEditViewControllerDelegate {
    // 音を鳴らす
    func cancelClicked()
    // nameを変える
    func renameRecFileName(filename:String, title:String,desc:String)
}

class TitleEditViewController: MyAlertViewController,UITextFieldDelegate {
//        @IBOutlet weak var alertView: UIView!
//        @IBOutlet weak var okButton: UIButton!
//        @IBOutlet weak var cancelButton: UIButton!
//        @IBOutlet weak var messageLabel: UILabel!
//        @IBOutlet weak var titleLabel: UILabel!
//
//        var titleText:String = ""
//        var messageText:String = ""
//    @IBOutlet weak var TitleTextView: UITextField!
//    @IBOutlet weak var DetailTextView: UITextView!
    @IBOutlet weak var CointerView: UIView!
    @IBOutlet weak var CointerViewHeight: NSLayoutConstraint!
    var deleage: TitleEditViewControllerDelegate?
    @IBOutlet weak var backgroudView: UIView!
    
    
    @IBOutlet weak var FileNameView: MKTextField!
    @IBOutlet weak var TitleView: MKTextField!
    @IBOutlet weak var DetailView: KMPlaceholderTextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CointerView.backgroundColor = UIColor.clear
            // Do any additional setup after loading the view.
        view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
//            titleLabel.text = titleText
//            messageLabel.text = messageText
        self.TitleView.delegate = self
        self.FileNameView.delegate = self
        self.CointerViewHeight.constant = 600

        self.FileNameView.floatingPlaceholderEnabled = true
        self.FileNameView.placeholder = "File Name："
        self.FileNameView.rippleLocation = .right
        self.FileNameView.bottomBorderEnabled = false
        
        self.TitleView.floatingPlaceholderEnabled = true
        self.TitleView.placeholder = "Title："
        self.TitleView.rippleLocation = .right
        self.TitleView.bottomBorderEnabled = false
        
        self.DetailView.placeholder = "Desc："
        
        self.FileNameView.tintColor = UIColor.black
        self.TitleView.tintColor = UIColor.black
        self.DetailView.tintColor = UIColor.black
        
        self.FileNameView.backgroundColor = UIColor.clear
        self.TitleView.backgroundColor = UIColor.clear
        self.DetailView.backgroundColor = UIColor.clear
        
        self.FileNameView.textColor = UIColor.black
        self.TitleView.textColor = UIColor.black
        self.DetailView.textColor = UIColor.black
        
        self.backgroudView.layer.borderWidth = 1
//        self.backgroudView.layer.borderColor = UIColor.blue.cgColor
        self.backgroudView.layer.borderColor = GlobalHeadColor.cgColor
        self.backgroudView.layer.cornerRadius = 10
        self.backgroudView.layer.masksToBounds = true
        self.backgroudView.backgroundColor = UIColor.clear
        
        self.alertView.layer.cornerRadius = 10
//        self.alertView.backgroundColor = GlobalHeadColor
        self.FileNameView.text = filenameText
        self.TitleView.text = titleText
        self.DetailView.text = descText
        if self.viewmode > 0{
            self.FileNameView.isEnabled = false
            self.FileNameView.backgroundColor = UIColor.gray
            self.FileNameView.tintColor = UIColor.gray
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.viewmode > 0{
            self.TitleView.becomeFirstResponder()
        }else{
            self.FileNameView.becomeFirstResponder()
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.DetailView.resignFirstResponder()
        return true
    }
    
    override init(nibName nibNameOrNil:String?, bundle ninBundleOrNil:Bundle? ){
            super.init(nibName:nibNameOrNil,bundle:ninBundleOrNil)
            self.transitioningDelegate = self
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalPresentationStyle = .custom
        }
    convenience init(filename: String, title:String, desc:String, mode:Int = 0){
            self.init(nibName:"TitleEditViewController",bundle:nil)
        
            filenameText = filename
            titleText = title
            descText = desc
            viewmode = mode
    }
    required init?(coder aDecider: NSCoder){
            fatalError("init error")
        }
    // MARK: - UIViewControllerTransitioningDelegate
    override func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            return AlertAnimation(show:true)
    }
    override func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            return AlertAnimation(show:false)
    }
    @IBAction override func cancelClicked(){
        dismiss(animated: true, completion: nil)
        self.deleage?.cancelClicked()
    }
    @IBAction override func okClicked(){
        if let newFilename = self.FileNameView.text{
            if filenameText != newFilename.removeWhitespace(){
                if let bean = DBMainTable.shared.getAllByfilename(newFilename.removeWhitespace()){
                    SVProgressHUD.showError(withStatus: NSLocalizedString("rec_file_rename_err_havefile", comment: ""))
                    SVProgressHUD.dismiss(withDelay: 0.5)
                    return 
                }
            }
        }
        
        dismiss(animated: true, completion: nil)
        self.deleage?.renameRecFileName(
            filename: self.FileNameView.text!,
            title: self.TitleView.text!,
            desc: self.DetailView.text)
    }
}
    

