//
//  MyBoxFileModel.swift
//  MediaBox
//
//  Created by Zhu Kui on 2018/01/26.
//  Copyright © 2018年 Zhu Kui. All rights reserved.
//

import UIKit

class MyBoxFileModel: NSObject {
    var fileURL: URL?
    var fullName: String?
    var name: String?
    var filExtension: String?
    
}
