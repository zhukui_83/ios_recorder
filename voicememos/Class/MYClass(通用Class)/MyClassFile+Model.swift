//
//  MyClassFile+Model.swift
//  MediaBox
//
//  Created by Zhu Kui on 2018/01/26.
//  Copyright © 2018年 Zhu Kui. All rights reserved.
//

import UIKit

extension MyClassFile {
    //Mark: 获取当前文件夹下的所有文件
    
    func getAllFileWithPath(documentsURL:URL)->([MyBoxFileModel]?,[MyBoxDirectoryModel]? ){
        let fileManager = FileManager.default
        do {
//let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            var fileArray = [MyBoxFileModel]()
            var directoryArray = [MyBoxDirectoryModel]()
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants)
            for file in fileURLs{
//print(file.description)
                var isDir : ObjCBool = false
                if fileManager.fileExists(atPath: file.path, isDirectory:&isDir) {
                    if isDir.boolValue {
                        // file exists and is a directory
                        let bean = MyBoxDirectoryModel()
                        bean.fileURL = file
                        bean.name = file.lastPathComponent
                        directoryArray.append(bean)
                    } else {
                        // file exists and is not a directory
                        let bean = MyBoxFileModel()
                        bean.fileURL = file
                        bean.fullName = file.lastPathComponent
                        bean.filExtension = file.pathExtension
                        bean.name = file.lastPathComponent.deletingPathExtension
                        fileArray.append(bean)
                    }
                } else {
                    // file does not exist
                }
                let attributes = try? fileManager.attributesOfItem(atPath: file.path) //结果为AnyObject类型
//                print("attributes: \\(attributes!)")
                print("创建时间：\(attributes![FileAttributeKey.creationDate]!)")
                print("修改时间：\(attributes![FileAttributeKey.modificationDate]!)")
                print("文件大小：\(attributes![FileAttributeKey.size]!)")
            }
            return (fileArray,directoryArray)
        } catch {
//            print("Error while enumerating files \(destinationFolder.path): \(error.localizedDescription)")
        }
        return (nil,nil)
    }
    //Mark: 获取当前文件夹的级数
    
    //Mark:
    //Mark:
    //Mark:
    //Mark:
    
}
