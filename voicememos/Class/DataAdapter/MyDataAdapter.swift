//
//  MyDataAdapter.swift
//  voicememos
//
//  Created by ksymac on 2019/10/06.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation

class MyDataAdapter{
    
    // MARK:get detail model
    static func getMessageFromDBData(items: Array<RecTagDetail>) -> [ZukMessageModel]{
        var msgs = [ZukMessageModel]()
        for item in items{
            let msg = ZukMessageModel(ZukMessageModel.MessageType(rawValue: item.type)!, msgincoming: .left)
            msg.No = item.detailNo
            msg.body = item.detail
            msg.ImageName = item.imgname
            msg.url = item.imgname
            msg.TagTime = item.TagTime
            do {
                let fileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: item.imgname)
            } catch {
                print("Error loading image : \(error)")
            }
            msg.senderName = sendname_left
            msgs.append(msg)
        }
        return msgs
    }
}
