//
//  ILOutgoingMessageCollectionViewCell.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-14.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class ILOutgoingMessageCollectionViewCell: ILDirectMessagesCollectionViewCell {
    static let nib = UINib(nibName: "ILOutgoingMessageCollectionViewCell", bundle: nil)
    static let identifier =  "ILOutgoingMessageCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
