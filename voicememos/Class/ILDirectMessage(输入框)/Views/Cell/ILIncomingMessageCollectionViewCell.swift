//
//  ILIncomingMessageCollectionViewCell.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-14.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class ILIncomingMessageCollectionViewCell: ILDirectMessagesCollectionViewCell {
    static let nib = UINib(nibName: "ILIncomingMessageCollectionViewCell", bundle: nil)
    static let identifier =  "ILIncomingMessageCollectionViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
