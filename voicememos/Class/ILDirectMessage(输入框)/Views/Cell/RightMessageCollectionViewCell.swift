//
//  ILOutgoingMessageCollectionViewCell.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-14.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class RightMessageCollectionViewCell: ILDirectMessagesCollectionViewCell {

    
    public static let nib = UINib(nibName: "RightMessageCollectionViewCell", bundle: nil)
    public static let identifier =  "RightMessageCollectionViewCell"
//    public static let nib = UINib(nibName: "SysMessageCollectionViewCell", bundle: nil)
//    public static let identifier =  "SysMessageCollectionViewCell"
    
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var MenuButton: UIButton!
    @IBAction func MenuButtonClicked(_ sender: Any) {
        delegate?.MessageMenuBtnClick(message)
    }
    
    public var message: ZukMessageModel?
    public var delegate: MessageCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(message: ZukMessageModel) {
//        super.configure(message: message)
        if let body = message.body {
            self.textView.text = body
            self.textView.isHidden = false
        }

        if let message = message as? ZukMessageModel {
            if message.MsgType == .image{
                // set the dummy image for now to figure out sizing related to the aspect ratio
                self.backgroundImageView.image = message.image
                self.mediaMessageContainerView.isHidden = false
            }
        }
//        let timeInterval = Double(message.TagTime)
//        let myNSDate = Date(timeIntervalSince1970: timeInterval)
//        //        let date = DateUtils.dateFromString(dateString, format: "HH:mm:ss")
//        self.TimeLabel.text = MyDateUtils.stringFromDate(date: myNSDate, format: "HH:mm:ss")    }
        self.TimeLabel.text =
            MyDateUtils.stringFromTimeInterval(time: message.TagTime)
    }
}
