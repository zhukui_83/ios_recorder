//
//  ILIncomingMessageCollectionViewCell.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-14.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class CenterMessageCollectionViewCell: ILDirectMessagesCollectionViewCell {

    static let nib = UINib(nibName: "CenterMessageCollectionViewCell", bundle: nil)
    static let identifier =  "CenterMessageCollectionViewCell"

    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var MenuButton: UIButton!
    @IBAction func MenuButtonClicked(_ sender: Any) {
        delegate?.MessageMenuBtnClick(message)
    }
    
    public var message: ZukMessageModel?
    public var delegate: MessageCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(message: ZukMessageModel) {
        if let body = message.body {
            self.textView.text = body
            self.textView.isHidden = false
            self.profileImageView.isHidden = true
        }
        if let message = message as? ZukMessageModel {
            self.message = message
            if message.MsgType == .image{
                // set the dummy image for now to figure out sizing related to the aspect ratio
                self.backgroundImageView.image = message.image
                self.mediaMessageContainerView.isHidden = false
            }
        }
        if message.TagTime > 0 {
//            self.TimeLabel.text = MyDateUtils.stringFromTimeInterval(time: message.TagTime)
        }
//self.messageContainerView.backgroundColor = CollectionView_CenterCell_BackgroundColor
    }
}
