//
//  ILIncomingMessageCollectionViewCell.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-14.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class LeftMessageCollectionViewCell: ILDirectMessagesCollectionViewCell {
    static let nib = UINib(nibName: "LeftMessageCollectionViewCell", bundle: nil)
    static let identifier =  "LeftMessageCollectionViewCell"
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var MenuButton: UIButton!
    @IBAction func MenuButtonClicked(_ sender: Any) {
        delegate?.MessageMenuBtnClick(message)
    }
    
    public var message: ZukMessageModel?
    public var delegate: MessageCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configure(message: ZukMessageModel, editmode:Bool) {
        if let body = message.body {
            self.textView.text = body
            self.textView.isHidden = false
        }
        if let message = message as? ZukMessageModel {
            self.message = message
            if message.MsgType == .image{
                self.backgroundImageView.image = message.image
                self.mediaMessageContainerView.isHidden = false
            }
        }

        if !(editmode){
            self.MenuButton.isEnabled = false
            self.profileImageView.backgroundColor = UIColor.gray
        }else{
            self.MenuButton.isEnabled = true
            self.profileImageView.backgroundColor = UIColor.flatWhite()
        }
        //self.profileImageView.isHidden = !(editmode)
        self.TimeLabel.text = MyDateUtils.stringFromTimeInterval(time: message.TagTime)

        if message.ShowMode_OverPlayerTime == false{
            self.TimeLabel.textColor = UIColor.white
        }else{
            self.TimeLabel.textColor = GlobalHeadColor
        }
        
        //关闭Container的touch，可以使Cell接受到点击
        self.messageContainerView.isUserInteractionEnabled = false
        //self.messageContainerView.backgroundColor = CollectionView_LeftCell_BackgroundColor
    }
    
    func startAlphaAnimation(){
        view.alpha = 0.0
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveEaseIn], animations: {
            self.view.alpha = 1.0
        }, completion: nil)
    }
    //        UIView.animate(withDuration: 2.0, animations: {
    //            self.view.layer.backgroundColor = UIColor.red.cgColor
    //        })
    //        view.alpha = 0.0
    //        UIView.animate(withDuration: 1.0, delay: 0.1, options: [.curveEaseIn], animations: {
    //            self.view.alpha = 1.0
    //        }, completion: nil)
}
