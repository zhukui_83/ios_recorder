//
//  ILDirectMessagesInputContainerView.swift
//  ILDirectMessagesTextView
//
//  Created by Igar Liubavetskiy on 2017-07-03.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class MyUIButton:UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override var isHighlighted: Bool{
        didSet {
            self.backgroundColor = self.isHighlighted ? UIColor.gray.withAlphaComponent(0.5) : UIColor.clear
        }
    }
}

protocol MyTagKeyBoardViewDelegate: class {
    func didChangeSize(inputContainerView: MyTagKeyBoardView, size: CGSize)
    func didTapSendButton(inputContainerView: MyTagKeyBoardView)
    
    func didTapEditButton(inputContainerView: MyTagKeyBoardView)
    func didTapTagButton(inputContainerView: MyTagKeyBoardView)
    func didTapPhotoButton(inputContainerView: MyTagKeyBoardView)
    func didTapCameraButton(inputContainerView: MyTagKeyBoardView)
}

class MyTagKeyBoardView: UIView {
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editTextView: MyInputTextView!
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var renameButton: UIButton!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var pictureButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    var minimumContainerHeight: CGFloat?
    var maximumContainerHeight: CGFloat?
    
    weak var delegate: MyTagKeyBoardViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonPrepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonPrepare()
    }
    
    func setColorMode(color: UIColor){
        self.editTextView.textColor = color
        self.editTextView.tintColor = color
        self.editTextView.placeholderColor = UIColor.white.withAlphaComponent(0.5)
        let image1 = UIImage(named: "icons8-save-as-50")
        self.renameButton.setImage(image1?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.renameButton.tintColor = color
        let image2 = UIImage(named: "icons8-price-tag-50")
        self.tagButton.setImage(image2?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.tagButton.tintColor = color
        let image3 = UIImage(named: "gallery-input-icon")
        self.pictureButton.setImage(image3?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.pictureButton.tintColor = color
        let image4 = UIImage(named: "icons8-camera-50")
        self.cameraButton.setImage(image4?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.cameraButton.tintColor = color
        self.sendButton.setTitle("Send", for: .normal)
        self.sendButton.setTitleColor(color, for: .normal)
    }
    
    func commonPrepare() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        self.contentView = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        // Toolbar
        let font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        self.menuView.clipsToBounds = true
        self.addSubview(self.contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.frame = self.bounds
        self.layoutTextView(with: self.editTextView.text)
    }
    @IBAction func sendBarButtonItemTapped(_ sender: NSObject) {
        self.delegate?.didTapSendButton(inputContainerView: self)
    }
    @IBAction func editBarButtonItemTapped(_ sender: NSObject) {
        self.delegate?.didTapEditButton(inputContainerView: self)
    }
    @IBAction func tagBarButtonItemTapped(_ sender: NSObject) {
        self.delegate?.didTapTagButton(inputContainerView: self)
    }
    @IBAction func photoBarButtonItemTapped(_ sender: NSObject) {
        self.delegate?.didTapPhotoButton(inputContainerView: self)
    }
    @IBAction func cameraBarButtonItemTapped(_ sender: NSObject) {
        self.delegate?.didTapCameraButton(inputContainerView: self)
    }
}

extension MyTagKeyBoardView {
    func resetViewHeight(){
        self.layoutTextView(with: editTextView.text)
    }
    func layoutTextView(with text: String) {
        let originalText = self.editTextView.text
        let inputContainerViewBounds = self.bounds
        let constraintWidth = inputContainerViewBounds.size.width - 2 * 10.0
        let constraintSize = CGSize(width: constraintWidth, height: .greatestFiniteMagnitude)
        
        self.editTextView.isScrollEnabled = false
        let newTextViewSize = self.editTextView.sizeThatFits(constraintSize)
        let adjustedTextViewContainerViewHeight = 10.0 + 15.0 + self.menuView.bounds.size.height + 1.0 + newTextViewSize.height
        let newSize = CGSize(width: 2 * 10.0 + newTextViewSize.width, height: adjustedTextViewContainerViewHeight)
        
        // Update constraints
        self.textViewHeightConstraint.constant = newTextViewSize.height
        self.delegate?.didChangeSize(inputContainerView: self, size: newSize)
    
        // Reset the textvie
        self.editTextView.text = originalText
        self.editTextView.setContentOffset(CGPoint.zero, animated: true)
        self.editTextView.isScrollEnabled = true
    }
}

extension MyTagKeyBoardView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.layoutTextView(with: textView.text)
    }
}
