//
//  ILDirectCollectionView.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-06-18.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class ILDirectMessagesCollectionView: UICollectionView {

    var messages: [ZukMessageModel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func message(at indexPath: IndexPath) -> ZukMessageModel {
        return messages[indexPath.item]
    }
    
    
    func scrollToLastItem(animated: Bool) {
        if self.numberOfSections == 0 {
            return
        }
        
        let lastIndexPath = IndexPath(item: self.numberOfItems(inSection: 0) - 1, section: 0)
        
        if self.numberOfSections < lastIndexPath.section {
            return
        }
        
        let contentHeight = self.collectionViewLayout.collectionViewContentSize.height
        if contentHeight < self.bounds.height {
            let visibleRect = CGRect(x: 0.0, y: contentHeight, width: 0.0, height: 0.0)
            self.scrollRectToVisible(visibleRect, animated: animated)
            return
        }
        
        guard let collectionViewLayout = self.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout else { return }
        let cellSize = collectionViewLayout.sizeForItem(at: lastIndexPath)
        let boundsHeight = self.bounds.height
        //- self.inputContainerView.frame.height
        let position =  (cellSize.height > boundsHeight) ? UICollectionViewScrollPosition.bottom : UICollectionViewScrollPosition.top
        
        self.scrollToItem(at:lastIndexPath, at: position, animated: animated)
    }
}
