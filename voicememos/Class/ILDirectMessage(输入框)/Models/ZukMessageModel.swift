//
//  MessageModel.swift
//  voicememos
//
//  Created by ksymac on 2018/3/10.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

class ZukMessageModel: ILMediaMessage {
    public enum MessageType:Int {
        case text = 0
        case image = 1
    }
    public enum MessageIncomingType:Int {
        case left = 0
        case center = 1
        case right = 2
    }
    var MsgType: MessageType!
//    var MsgIncomingType: MessageIncomingType?
    var isIncomingType: MessageIncomingType!
    var TagTime: Double = 0
    var No: Int = 0
    var ImageName: String?
    
    var ShowMode_OverPlayerTime = false //只用来显示用的
    
    init(_ msgtype: MessageType, msgincoming: MessageIncomingType) {
        self.MsgType = msgtype
        self.isIncomingType = msgincoming
        super.init()
    }
    
    override var hash: Int  {
        get {
//            return super.hash ^ self.No.hashValue ^ TagTime.hashValue
            if self.MsgType == .text{
                print("No : \(No) MsgType : \(MsgType) body : \(body)   ")
                print("hash : \(self.No.hashValue ^ self.MsgType.hashValue ^ self.body!.hashValue) ")
                return self.No.hashValue ^ self.MsgType.hashValue ^ self.body!.hashValue
            }else{
                print("No : \(No) MsgType : \(MsgType) ImageName : \(ImageName)   ")
                print("hash : \(self.No.hashValue ^ self.MsgType.hashValue ^ self.ImageName!.hashValue)")
                return self.No.hashValue ^ self.MsgType.hashValue ^ self.ImageName!.hashValue
            }
        }
    }
}



extension ZukMessageModel{
    static func createImageModel(img:UIImage,detailno:Int,time:Double )->ZukMessageModel {
        let message = ZukMessageModel(ZukMessageModel.MessageType.image, msgincoming: .left)
        message.MsgType = ZukMessageModel.MessageType.image
        message.date = Date(timeIntervalSinceNow: 0)
        message.isIncomingType = .left
        message.senderName = sendname_left
        message.image = img
        let imgname = saveImage(image: img)
        message.ImageName = imgname
        message.url = imgname
        message.No = detailno
        message.TagTime = time
        return message
    }
    static func saveImage (image: UIImage) -> String{
        let imgname = "img"+MyClassFile.sharedInstance.getStrWithNow()+".jpg"
        let path = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: imgname)
        let jpgImageData = UIImageJPEGRepresentation(image, 0.8)
        do {
            try jpgImageData?.write(to: path!, options: .atomic)
            return imgname
        } catch {
            print(error)
            return ""
        }
    }
}




