//
//  ILMediaMessage.swift
//  ILDirectMessages
//
//  Created by Igar Liubavetskiy on 2017-07-16.
//  Copyright © 2017 Igar Liubavetskiy. All rights reserved.
//

import UIKit

class ILMediaMessage: ILMessage {
    var url: String?
    var image: UIImage?
    var isAudioMessage: Bool = false
//    var isIncoming: MessageIncomingType!
    
    override var hash: Int  {
        get {
            if let url = url , let image = image {
                return super.hash ^ url.hashValue ^ image.hashValue
            }else{
                return super.hash
            }
        }
    }
}
