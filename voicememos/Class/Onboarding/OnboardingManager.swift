//
//  OnboardingManager.swift
//  voicememos
//
//  Created by ksymac on 2019/10/20.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation


let Key_Onboarding_Showed = "Key_Onboarding_Showed"


class OnboardingManager {
    static let shared = OnboardingManager()
    func saveValue(){
        UserDefaults.standard.set(true, forKey: Key_Onboarding_Showed)
    }
    func showOnboarding(basevc: UIViewController){
        if let value: Bool = UserDefaults.standard.object(forKey: Key_Onboarding_Showed) as? Bool {
            if value == true{
                return
            }
        }
        presentOnboarding(basevc: basevc)
    }
    func presentOnboarding(basevc: UIViewController){
        let vc = OnboardingMainViewController.getInstance()
        vc.modalPresentationStyle = .fullScreen
        basevc.navigationController?.present(vc, animated: true, completion: {
            
        })
    }
}

