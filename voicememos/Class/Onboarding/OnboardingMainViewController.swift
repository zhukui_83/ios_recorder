//
//  ViewController.swift
//  testPageView
//
//  Created by Abhinav Prakash on 24/12/17.
//  Copyright © 2017 Abhinav Prakash. All rights reserved.
//

import UIKit

class OnboardingMainViewController: UIViewController {

    @IBOutlet weak var onboardingContainer: UIView!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBAction func skipOnboarding(_ sender: UIButton) {
        sender.removeFromSuperview()
        //onboardingContainer.removeFromSuperview()
        self.dismiss(animated: true) {
            
        }
    }
    
    static func getInstance() -> OnboardingMainViewController {
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OnboardingMainViewController") as! OnboardingMainViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OnboardingManager.shared.saveValue()
    }
}

