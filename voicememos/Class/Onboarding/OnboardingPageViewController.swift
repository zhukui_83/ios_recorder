//
//  OboardingPageViewControllerr.swift
//  voicememos
//
//  Created by ksymac on 2019/10/20.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var pageNo:Int = 0
    
    static func getInstance() -> OnboardingPageViewController {
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OnboardingPageViewController") as! OnboardingPageViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView(){
        let title = String(format: "onboarding_title_%d", pageNo)
        let detail = String(format: "onboarding_msg_%d", pageNo)
        let imgname = String(format: "onboarding-%d", pageNo)
        self.titleLabel.text = NSLocalizedString(title, comment: "")
        self.detailLabel.text = NSLocalizedString(detail, comment: "")
        self.imgView.image = UIImage.init(named: imgname)
    }
}
