//
//  ViewController.swift
//  qrbox
//
//  Created by ksymac on 2018/1/14.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

extension CodeAddViewController{
  
    func setInitUI(){
        self.textViewTitle.delegate = self;
        self.textViewComment.delegate = self;
        self.textViewContent.delegate = self;
        
        self.ContentView.layer.borderWidth = 1
        self.ContentView.layer.borderColor = UIColor.black.cgColor
        self.ContentView.layer.cornerRadius = 10
        self.ContentView.layer.masksToBounds = true
        self.ContentView.backgroundColor = GlobalHeadColor
        

        //        textViewTitle.layer.borderColor = UIColor.clear.cgColor
        self.textViewTitle.floatingPlaceholderEnabled = true
        self.textViewTitle.placeholder = "備考："
        self.textViewTitle.rippleLocation = .right
        self.textViewTitle.bottomBorderEnabled = false
        
        self.textViewContent.tintColor = UIColor.white
        self.textViewTitle.tintColor = UIColor.white
        self.textViewComment.tintColor =  UIColor.white
        
        self.textViewTitle.backgroundColor = UIColor.clear
        self.textViewComment.backgroundColor = UIColor.clear
        self.textViewContent.backgroundColor = UIColor.clear
        
        self.textViewTitle.textColor = UIColor.white
        self.textViewComment.textColor = UIColor.white
        self.textViewContent.textColor = UIColor.white
        
//        self.textViewTitle.placeholderColor = UIColor.lightText
//        self.textViewComment.placeholderColor = UIColor.lightGray
        self.textViewComment.placeholderColor = UIColor.colorWithCustomAlpha(0, g: 0, b: 26, alpha: 0.22)
        self.textViewContent.placeholderColor = UIColor.colorWithCustomAlpha(0, g: 0, b: 26, alpha: 0.22)
//        R:0 G:0 B:0.1 A:0.22
        self.textViewTitle.floatingLabelTextColor = UIColor.white
//        self.textViewTitle.floatingLabel.textColor = UIColor.lightText
        self.textViewTitle.reloadInputViews()
        
        self.BikoView.layer.borderWidth = 1
        self.BikoView.layer.borderColor = UIColor.black.cgColor
        self.BikoView.layer.cornerRadius = 10
        self.BikoView.layer.masksToBounds = true
        self.BikoView.backgroundColor = UIColor.clear
        
        barcodeImg.backgroundColor = UIColor.white
        barcodeImg.layer.cornerRadius = 5
        barcodeImg.layer.shadowColor = UIColor.black.cgColor
        barcodeImg.layer.shadowOffset = CGSize(width: 0, height: 5)
        barcodeImg.layer.shadowOpacity = 0.6
        
        
        btnTrackStart.layer.shadowOpacity = 0.55
        btnTrackStart.layer.shadowRadius = 5.0
        btnTrackStart.layer.shadowColor = UIColor.gray.cgColor
        btnTrackStart.layer.shadowOffset = CGSize(width: 0, height: 2.5)
        btnTrackStart.titleLabel!.font = UIFont(name: "Helvetica-Bold",size: CGFloat(20))
        
        btnTrackAdd.layer.shadowOpacity = 0.55
        btnTrackAdd.layer.shadowRadius = 5.0
        btnTrackAdd.layer.shadowColor = UIColor.gray.cgColor
        btnTrackAdd.layer.shadowOffset = CGSize(width: 0, height: 2.5)
        btnTrackAdd.titleLabel!.font = UIFont(name: "Helvetica-Bold",size: CGFloat(20))
        
        btnOther.layer.shadowOpacity = 0.55
        btnOther.layer.shadowRadius = 5.0
        btnOther.layer.shadowColor = UIColor.gray.cgColor
        btnOther.layer.shadowOffset = CGSize(width: 0, height: 2.5)
        btnOther.titleLabel!.font = UIFont(name: "Helvetica-Bold",size: CGFloat(20))
        
        self.view.backgroundColor = GlobalHeadColor
        self.setupBarItem()
    }
    func setupBarItem(){
        let cancelButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                                           target: self,
                                                           action: #selector(self.closeMe(_:)))
        self.navigationItem.leftBarButtonItems = [cancelButton,]
    }
    
    @objc func closeMe(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.endblock()
        }
    }
    func setInitView(){
        switch viewMode {
        case .addView:
//            self.btnTrackAdd.titleLabel!.text = "追加"
            self.title = "追加"
            self.strContent = ""
            self.strTrackType = "0"
            self.strTitle = ""
            self.strComment = ""
            self.setTrackType(self.strTrackType)
        case .editView:
//            self.btnTrackAdd.titleLabel!.text = "修正"
            self.title = "修正"
            btnTrackAdd.reloadInputViews()
        case .scanView:
//            self.textViewContent.text = scanRetStr
            if let content = scanRetStr {
                self.searchBeanAndSetView(content)
            }
        }
//        self.btnTrackAdd.reloadInputViews()

        self.textViewTitle.text = strTitle
        self.textViewContent.text = strContent
        self.textViewComment.text = strComment

    }
    func searchBeanAndSetView(_ content:String){
        let temp = dbMainTb.getAllByfilename(content)
        if(temp == nil){
            self.strContent = content
            self.strTrackType = "0"
            self.strTitle = ""
            self.strComment = ""
//            self.btnTrackAdd.titleLabel!.text = "追加"
        }else{
            self.strContent = content
            self.strTrackType = "0"
            self.strTitle = (temp?.title)!
            self.strComment = (temp?.comment)!
//            self.btnTrackAdd.titleLabel!.text = "修正"
        }
    }
    @IBAction func closeKeyboard(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    


}
//MARK: - 保存修改数据
extension CodeAddViewController{
    // MARK: - 保存修改数据
    @IBAction func btnAddAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        if(self.checkInput()){
            self.addToDB()
            self.dismiss(animated: true, completion: {
            })
        }
    }
    //　MARK: 拷贝番号
    func copyContent(_ content: String){
        comfunc.copytoPasteBoard(content)
        self.view.makeToast(message: "内容をコピーました")
    }
}

//MARK: - 打开
extension CodeAddViewController{
    // MARK: - 打开方式
    @IBAction func btnTrackStart(_ sender: AnyObject) {
        self.view.endEditing(true)
        if(self.checkInput()){
            guard let trackmain = self.addToDB() else{
                return
            }
            self.openContent(bean: trackmain)
        }
    }
    func openContent(bean : TrackMain?){
        let vc = self
//        let appDelegate:AppDelegate = UIApplication.shared.delegate! as! AppDelegate
//        MyQRBaseAction.sharedInstance.displayOpenSheet(vc: self, bean: bean)
        let shareMenu = UIAlertController(title: nil, message: "メニュー", preferredStyle: .actionSheet)
        let safariAction = UIAlertAction(title: "Safariで開く", style: .default, handler: {
            (action:UIAlertAction) -> Void in
            let str = "\((bean?.filename)!)"
            let urlString = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            vc.dismiss(animated: false, completion: nil)
            if let okurl = URL(string: urlString!){
                if UIApplication.shared.canOpenURL(okurl) {
//                    UIApplication.shared.open(okurl, options: [:], completionHandler: nil)
                    //If you want handle the completion block than
                    UIApplication.shared.open(okurl, options: [:], completionHandler: { (success) in
                        print("Open url : \(success)")
                    })
                }
            }else{
            }
        })

        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
//        shareMenu.addAction(safariAction)
//        shareMenu.addAction(copyContentAction)
//        shareMenu.addAction(shareContentAction)
//        shareMenu.addAction(shareImgAction)
        shareMenu.addAction(cancelAction)
        shareMenu.modalPresentationStyle = .popover
        if let presentation = shareMenu.popoverPresentationController {
            presentation.barButtonItem = navigationItem.rightBarButtonItems?[0]
        }
        vc.present(shareMenu, animated: true, completion: nil)
    }
}

//MARK: - その他
extension CodeAddViewController{
    @IBAction func btnOtherClick(_ sender: Any) {
        self.view.endEditing(true)
        if(self.checkInput()){
            guard let bean = self.addToDB() else{
                return
            }
            self.displayOtherSheet(vc: self, bean: bean)
        }
    }
    func displayOtherSheet(vc:UIViewController,bean : TrackMain?) {
        let shareMenu = UIAlertController(title: nil, message: "メニュー", preferredStyle: .actionSheet)
    
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)

        shareMenu.addAction(cancelAction)
        shareMenu.modalPresentationStyle = .popover
        if let presentation = shareMenu.popoverPresentationController {
            presentation.barButtonItem = navigationItem.rightBarButtonItems?[0]
        }
        vc.present(shareMenu, animated: true, completion: nil)
    }
}


