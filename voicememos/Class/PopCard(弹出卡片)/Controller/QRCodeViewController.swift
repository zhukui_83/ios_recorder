//
//  QRCodeViewController.swift
//  qrbox
//
//  Created by ksymac on 2018/1/14.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

protocol QRCodeViewControllerDelegate {
    func OpenEditViewBlock(bean:TrackMain?);
    
}


class QRCodeViewController: BaseViewController {


    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelComment: UILabel!
    
    @IBOutlet weak var labelContent: UILabel!
   
    @IBOutlet weak var barcodeImg: UIImageView!
    
    var bean:TrackMain?
    var delegate: QRCodeViewControllerDelegate?
    
    
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var CenterView: UIView!
    
    
    @IBOutlet weak var Btn1: UIButton!
    @IBOutlet weak var Btn2: UIButton!
    @IBOutlet weak var Btn3: UIButton!
    
    class func share(data:TrackMain) -> QRCodeViewController{
        let storyboard: UIStoryboard = UIStoryboard(name: "PopCard", bundle: nil)
        let vc: QRCodeViewController = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        vc.bean = data
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBarItem()
        self.initUI()
    }
    @objc func closeMe(_ sender: Any) {
        self.dismiss(animated: true) {
//            self.delegate?.endblock()
        }
    }
    func setupBarItem(){
        let cancelButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                                           target: self,
                                                           action: #selector(self.closeMe(_:)))
        self.navigationItem.leftBarButtonItems = [cancelButton,]
    }
}

extension QRCodeViewController{
    func initUI(){
        self.labelTitle.text = bean?.title
        self.labelComment.text = bean?.comment
        self.labelContent.text = bean?.filename
        self.setImageWithStr(str: bean?.filename)
        self.view.backgroundColor = GlobalHeadColor

//        self.TopView.layer.borderColor = UIColor.red as! CGColor
//        self.TopView.layer.borderWidth = 10
        self.TopView.layer.cornerRadius = 10
        self.TopView.layer.masksToBounds = true
//        self.CenterView.layer.borderWidth = 10
        self.CenterView.layer.cornerRadius = 10
        self.CenterView.layer.masksToBounds = true
        self.CenterView.backgroundColor = UIColor.groupTableViewBackground
//        self.BottomView.layer.borderWidth = 10
        self.BottomView.layer.cornerRadius = 10
        self.BottomView.layer.masksToBounds = true
        self.BottomView.backgroundColor = UIColor.groupTableViewBackground
        
        
        self.Btn1.addTarget(self, action: #selector(QRCodeViewController.BtnClick1(_:)), for: .touchUpInside)
        self.Btn2.addTarget(self, action: #selector(QRCodeViewController.BtnClick2(_:)), for: .touchUpInside)
        self.Btn3.addTarget(self, action: #selector(QRCodeViewController.BtnClick3(_:)), for: .touchUpInside)
    }
    func setImageWithStr(str: String?){
        barcodeImg.backgroundColor = UIColor.cyan
        barcodeImg.layer.cornerRadius = 5
        barcodeImg.layer.shadowColor = UIColor.black.cgColor
        barcodeImg.layer.shadowOffset = CGSize(width: 0, height: 5)
        barcodeImg.layer.shadowOpacity = 0.6
        

        barcodeImg.image = nil
    }
    
}
extension QRCodeViewController{
    @objc func BtnClick1(_ sender: AnyObject){
    }
    @objc func BtnClick2(_ sender: AnyObject){
//        MyQRBaseAction.sharedInstance.displayOpenSheet(vc:self, bean: bean)
    }
    @objc func BtnClick3(_ sender: AnyObject){
        displayOtherSheet(vc:self, bean: bean)
    }
    
    
    // MARK:  other
    func displayOtherSheet(vc:UIViewController,bean : TrackMain?) {


    }
}






