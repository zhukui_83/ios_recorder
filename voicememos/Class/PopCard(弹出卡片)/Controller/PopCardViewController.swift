//
//  TrackWebViewController.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/14.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//
import UIKit

protocol PopCardViewControllerDelegate {
    func endblock();
}
class PopCardViewController: UIViewController {
    
    var delegate: PopCardViewControllerDelegate?
    @IBOutlet weak var scanRetLabel: UILabel!

    var scanRetStr:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.scanRetLabel.text = scanRetStr
    }
    class func share(ret:String?) -> PopCardViewController{
        let storyboard: UIStoryboard = UIStoryboard(name: "PopCard", bundle: nil)
        let vc: PopCardViewController = storyboard.instantiateViewController(withIdentifier: "PopCardViewController") as! PopCardViewController
        vc.scanRetStr = ret
        return vc
    }
    
    @IBAction func closeMe(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.endblock()
        }
    }
}
