//
//  BarButtonItemViewController.swift
//  MaterialKit
//
//  Created by Le Van Nghia on 11/29/14.
//  Copyright (c) 2014 Le Van Nghia. All rights reserved.
//

import UIKit

import KMPlaceholderTextView

protocol CodeAddViewControllerDelegate {
    func endblock();
}


public enum AddViewMode {
    case scanView
    case editView
    case addView
}


class CodeAddViewController: BaseViewController , UITextFieldDelegate ,UITextViewDelegate,
     UIImagePickerControllerDelegate{
    var delegate: CodeAddViewControllerDelegate?

    
    @IBOutlet weak var btnOther: MKButton!
    @IBOutlet weak var btnTrackStart: MKButton!
    @IBOutlet weak var btnTrackAdd: MKButton!
//    @IBOutlet weak var btnTrackType: MKButton!
//    @IBOutlet weak var textViewContent: MKTextField!
//    @IBOutlet weak var textViewsComment: MKTextField!
    
    //
    @IBOutlet weak var textViewTitle: MKTextField!
    //
    @IBOutlet weak var textViewContent: KMPlaceholderTextView!
    //
    @IBOutlet weak var textViewComment: KMPlaceholderTextView!
    
    @IBOutlet weak var barcodeImg: UIImageView!
    
    let dbMainTb = DBMainTable()//model
    
    var strContent : String = ""
    var strTrackType : String = ""
    var strTitle : String = ""
    var strComment : String = ""
    
//    var oldstrContent : String = ""
    var oldbean : TrackMain?
    
    //UI Mode: 0:Scan; 1:Edit;
    var viewMode : AddViewMode = AddViewMode.addView

    var scanRetStr:String?
    
    //備考View
    @IBOutlet weak var BikoView: UIView!
    //内容View
    @IBOutlet weak var ContentView: UIView!
    
    
    class func share(mode:AddViewMode, content:String?) -> CodeAddViewController{
        let storyboard: UIStoryboard = UIStoryboard(name: "PopCard", bundle: nil)
        let vc: CodeAddViewController = storyboard.instantiateViewController(withIdentifier: "CodeAddViewController") as! CodeAddViewController
        
        vc.viewMode = mode
        vc.scanRetStr = content
        return vc
    }
    //var trackMain : TrackMain = TrackMain()
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置视图
        self.setInitUI()
        self.setInitView()
        //添加切换
        self.makeKeybord()
    }
    //MARK:切换输入文字和数字
    let chagerKeyboardItem = UIBarButtonItem.init(title: "Abc", style: .plain,
                                                  target: self, action: #selector(changeButtonTapped))
    func makeKeybord(){
        let kbToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
        kbToolBar.barStyle = .default  // スタイルを設定
        kbToolBar.sizeToFit()  // 画面幅に合わせてサイズを変更
        let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let commitButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.commitButtonTapped))
        kbToolBar.items = [ spacer, commitButton,chagerKeyboardItem]
        textViewContent.inputAccessoryView = kbToolBar
        textViewComment.inputAccessoryView = kbToolBar
    }
    func initchagerKeyboardItem(){
        textViewContent.keyboardType = .default
        textViewComment.keyboardType = .default
        chagerKeyboardItem.title = "Abc"
    }
    @objc func changeButtonTapped (){
        if textViewContent.keyboardType != .twitter {
            textViewContent.keyboardType = .twitter
            chagerKeyboardItem.title = "123"
        }else{
            textViewContent.keyboardType = .decimalPad
            chagerKeyboardItem.title = "Abc"
        }
        textViewContent.reloadInputViews()
    }
    @objc func commitButtonTapped (){
        self.view.endEditing(true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if(viewMode == .addView ){
//            self.btnTrackAdd.titleLabel!.text = "追加"
//        }else{
//            self.btnTrackAdd.titleLabel!.text = "修正"
//            btnTrackAdd.reloadInputViews()
//            self.setTrackType(self.strTrackType)
//        }
        initchagerKeyboardItem()
    }
    //改行ボタンが押された際に呼ばれる.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    func setTrackType( _ stype : String){
        self.strTrackType = stype
    }
   
    func animateLabelRipple() {
    }
    
    func animateImageRipple() {
    }
    
    func checkInput() -> Bool{
//        .uppercased()
        strContent = self.textViewContent.text!
        strComment = self.textViewComment.text!
        strTitle = self.textViewTitle.text!
        if(strContent.isEmpty){
            //let alert = SCLAlertView()
            SCLAlertView().showError("内容", subTitle:"内容はからです。", closeButtonTitle:"OK")
            return false
        }
        return true
    }
    
    // MARK: -  添加到DB insert or update
    func addToDB() -> TrackMain? {
//        var temp : TrackMain? =  nil
        self.strTrackType = "0"
        //        if(self.viewMode == .addView ){ //add
        //            temp = dbMainTb.getAllByContent(strContent)
        //        }else{ //edit
        //            temp = dbMainTb.getAllByContent(oldstrContent)
        //        }
        if let temp = self.oldbean {
            temp.filename = strContent
            temp.dataType = strTrackType
            temp.comment = strComment
            temp.title = strTitle
            if(temp.status == ComFunc.TrackList_del){
                temp.status = ComFunc.TrackList_doing
            }
            _ = dbMainTb.updateByID(temp)
        }else{
            let trackmain = TrackMain()
            trackmain.filename = strContent
            trackmain.dataType = strTrackType
            trackmain.comment = strComment
            trackmain.title = strTitle
            trackmain.status = ComFunc.TrackList_doing
            _ = dbMainTb.add(trackmain)
        }
        self.view.makeToast(message: "データ保存済み")
        self.oldbean = dbMainTb.getAllByfilename(strContent)
        return self.oldbean
    }
    
    
//
//    func moveToDetailView(_ trackmain : TrackMain){
//
//    }
    
    // MARK: -  生成QRCode图片
    @IBAction func createQRCodeImage(_ sender: Any) {

    }
    // MARK: -  textView编辑完时，重新刷新图片
    func textViewDidEndEditing(_ textView: UITextView) {

    }
}




