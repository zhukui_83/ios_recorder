//
//  MyPandoraPlayer+Player.swift
//  voicememos
//
//  Created by ksymac on 2018/6/30.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import Foundation
import AudioKit
import AVKit
import MediaPlayer

extension MyPandoraPlayer{
    // MARK: 关闭视图
    @IBAction func closeButtonDidClick(_ sender: Any) {
        //let isPlaying = self.player?.isPlaying
        player?.playStop()
        self.player = nil
        //onClose?(())
        self.dismiss(animated: true) {
        }
    }
}

// MARK: PlayerSongListDelegate
extension MyPandoraPlayer: PlayerSongListDelegate {
    func scrollBetween(left: CellActivityItem, right: CellActivityItem) {
        //        fadeImageView.isHidden = false
        //
        //        let origin = left.index == currentSongIndex ? left: right
        //        let destination = left.index != currentSongIndex ? left: right
        //
        //        blurredAlbumImageView.image = library[origin.index].metadata?.artwork
        //        fadeImageView.image = library[destination.index].metadata?.artwork
        //
        //        blurredAlbumImageView.alpha = origin.activity
        //        fadeImageView.alpha = destination.activity
    }
    
    func currentSongDidChanged(index: Int) {
        //self.currentSongIndex = index
    }
    
    func next() {
        updatePlaybackStatus()
    }
    
    func didTap() {
        //togglesPlay()
        MyAudioPlayerStartOrPause()
    }
    
    func previous() {
        updatePlaybackStatus()
    }
    
    func prefetchItems(at indices: [Int]) {
        //        for index in indices {
        //            guard library[index].colors == nil else { continue }
        //            guard tasks[index] == nil else { continue }
        //            var item: DispatchWorkItem!
        //            item = DispatchWorkItem(block: {
        //                guard !item.isCancelled else { return }
        //                guard let image = self.library[index].metadata?.artwork else { return }
        //                guard !item.isCancelled else { return }
        //                image.getColors(scaleDownSize: CGSize(width: scaleDownSizeWidth, height: scaleDownSizeHeight), completionHandler: { (colors) in
        //                    guard !item.isCancelled else { return }
        //                    self.library[index].colors = colors
        //                    DispatchQueue.main.async(execute: {
        //                        if self.currentSongIndex == index {
        //                            self.updateForColors(colors)
        //                        }
        //                    })
        //                })
        //            })
        //            tasks[index] = item
        //            DispatchQueue.global(qos: .default).async(execute: item)
        //        }
    }
}

// MARK: PlayerControlsDelegate 控制menu的方法
extension MyPandoraPlayer: PlayerControlsDelegate {
    func onRepeat() {
        self.isRepeatModeOn = !self.isRepeatModeOn
        self.player?.onRepeat(b: self.isRepeatModeOn)
    }
    
    func onRewindBack() {
        //rewindBackward()
    }
    
    func onPlay() {
        //togglePlay()
        MyAudioPlayerStartOrPause()
    }
    
    func onRewindForward() {
        //rewindForward()
        share(vc: self,bean:  self.mainbean!)
    }
    
    func share(vc:UIViewController,bean : TrackMain) {
        let vc =  ShareWebViewController.init()
        let nav = UINavigationController.init(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        vc.bean = bean
        self.present(nav, animated: true, completion: nil)
//        let audioFileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record, filename: bean.filenamewithextension)
//        let title = bean.filenamewithextension
//        let activityItems = [title, audioFileURL] as [Any]
//        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
//        // 使用しないアクティビティタイプ
//        let excludedActivityTypes = [
//            //UIActivityType.postToFacebook,
//            //UIActivityType.postToTwitter,
//            //UIActivityType.message,
//            //UIActivityType.saveToCameraRoll,
//            UIActivityType.print,
//        ]
//        activityVC.excludedActivityTypes = excludedActivityTypes
//        vc.present(activityVC, animated: true, completion: nil)
    }
    //MARK: 修改变为是否可以显示输入框的功能
    func onShuffle() {
        //self.isShuffleModeOn = !self.isShuffleModeOn
        self.switchInputViewShowOrHidden()
    }
}

// MARK: PlayerSliderProtocol
extension MyPandoraPlayer: PlayerSliderProtocol {
    func onValueChanged(progress: Float, timePast: TimeInterval) {
        player?.seekTime(progress: progress)
        //beeingSeek = true
        //let frame = Int64(Float(player.audioFile.totalFrames) * progress)
        //self.player.seek(toFrame: frame)
    }
}

extension MyPandoraPlayer {
        //MARK: 根据当前播放器的进度修改cell的颜色
        public func resetCellColorWithPlayerTime(nowTime:Double){
            if abs(nowTime - SeekTime) > 0.5{
                SeekTime = nowTime
            }else{
                return
            }
    //        print(nowTime)
            if self.isEditMode{
                return
            }
            var index = 0
            var indexPaths: [IndexPath] = []
            var changeitem : Int?
            for msg in messages{
                if msg.ShowMode_OverPlayerTime == false && msg.TagTime < nowTime
                {
                    msg.ShowMode_OverPlayerTime = true
                    indexPaths.append(IndexPath(item: index, section: 0))
                    changeitem = index
                }
                if msg.ShowMode_OverPlayerTime == true && msg.TagTime > nowTime
                {
                    msg.ShowMode_OverPlayerTime = false
                    indexPaths.append(IndexPath(item: index, section: 0))
                }
                index += 1
            }
            if indexPaths.count > 0{
                DispatchQueue.main.async {[weak sliderView, weak controlsView] in
                    self.collectionView.reloadItems(at: indexPaths)
                    if changeitem != nil{
                        self.scrollToItem(index: changeitem!, animated: true)
                    }
                }
            }
        }
}




//// MARK: EZAudioPlayerDelegate
//extension MyPandoraPlayer: EZAudioPlayerDelegate {
//    public func audioPlayer(_ audioPlayer: EZAudioPlayer!, playedAudio buffer: UnsafeMutablePointer<UnsafeMutablePointer<Float>?>!, withBufferSize bufferSize: UInt32, withNumberOfChannels numberOfChannels: UInt32, in audioFile: EZAudioFile!) {
//        
//        DispatchQueue.main.async {[weak self] in
//            self?.updatePlaybackStatus()
//        }
//        if bufferSize == 0{
//            return
//        }
//        self.waveVisualizer?.updateWaveWithBuffer(buffer, withBufferSize: bufferSize, withNumberOfChannels: numberOfChannels)
//    }
//    
//    public func audioPlayer(_ audioPlayer: EZAudioPlayer!, updatedPosition framePosition: Int64, in audioFile: EZAudioFile!) {
//        guard !beeingSeek else {
//            beeingSeek = false
//            return
//        }
//        
//        let duration = audioFile.duration
//        let progress = audioFile.totalFrames > 0 ? Float(framePosition) / Float(audioFile.totalFrames): 0
//        let nowTime = progress * duration
//
//        resetCellColorWithPlayerTime(nowTime: nowTime)
//        let isPlaying = audioPlayer.isPlaying
//        DispatchQueue.main.async {[weak sliderView, weak controlsView] in
//            controlsView?.isPlaying = isPlaying
//            sliderView?.duration = duration
//            sliderView?.progress = progress
//        }
//    }
////    public func audioPlayer(_ audioPlayer: EZAudioPlayer!, reachedEndOf audioFile: EZAudioFile!) {
////        guard !isRepeatModeOn else {
////            DispatchQueue.main.async { [weak self] in
////                self?.reloadPlayer()
////                if self?.player != nil && !(self?.player.isPlaying)! {
////                    self?.player.play()
////                }
////            }
////            return
////        }
////        guard self.currentSongIndex < library.count else {
////            return
////        }
////
////        let newIndex = currentSongIndex < library.count - 1 ? currentSongIndex + 1: 0
////        DispatchQueue.main.async { [weak self] in
////            self?.currentSongIndex = newIndex
////        }
////    }
//}
