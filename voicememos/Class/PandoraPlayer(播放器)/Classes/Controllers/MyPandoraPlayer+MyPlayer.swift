//
//  MyPandoraPlayer+MyPlayer.swift
//  voicememos
//
//  Created by ksymac on 2019/09/29.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation

extension MyPandoraPlayer:MyAudioPlayerDelegate {
    func initMyAudioPlayer(){
        if player == nil{
            if let unpackedPath = path {
                 player = MyAudioPlayer.init(delegate: self)
                 player?.setFileUrl(url: unpackedPath)
             }
        }
    }
    func MyAudioPlayerStartOrPause(){
        if player?.isPlaying == true {
            player?.playPause()
        } else {
            player?.playStart()
        }
    }
    
    func SetDuration(duration: CGFloat){
        self.sliderView.duration = TimeInterval(duration)
    }
    func SetProgress(duration: CGFloat, currentTime: Double){
        let progress:Float = Float(CGFloat(currentTime) / duration)
        self.sliderView.progress = progress
        self.resetCellColorWithPlayerTime(nowTime:currentTime)
    }
    func playerStart(){
        updatePlaybackStatus()
    }
    func playerPause(){
        updatePlaybackStatus()
    }
    func playerStop(){
        updatePlaybackStatus()
    }
}
