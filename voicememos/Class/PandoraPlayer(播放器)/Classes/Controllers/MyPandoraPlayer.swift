//
//  MYPandoraPlayer.swift
//  MyPandoraPlayer
//
//  Created by Boris Bondarenko on 6/1/17.
//  Copyright © 2017 Applikey Solutions. All rights reserved.
//

import UIKit
import AudioKit
import AVKit
import MediaPlayer
import AVFoundation


// MARK: Constants
let storyboardIdentifier = "MyPandoraPlayer"
let unknown = "Unknown"

var rightChannelColor: UIColor = UIColor.green
var leftChannelColor: UIColor = UIColor.blue
var songNameColor: UIColor = UIColor.white
var songAlbumColor: UIColor = UIColor.white
var deadlineTimeOffset = 1000
let scaleDownSizeWidth = 100
let scaleDownSizeHeight = 100

let animatableViewHeight = 100
let animatableViewWidth = 100
let animatableViewAlpha: CGFloat = 0.3
let animatableViewScale: CGFloat = 1.7
let animationInterval: TimeInterval = 0.2
let defaultStartProgress: Float = 0.0

let asyncOffset: Float = 0.2

//播放器
open class MyPandoraPlayer: UIViewController {
    let dbMainTb = DBMainTable.shared//model
    let dbStatusTb = DBStatusTable.shared//model
    let dbDetailTb = DBDetailTable.shared//model
    
    var nowPlaying = "Now Playing"
    //MARK: Public
    //public var playImmediately: Bool = false
    //public var onClose: GenericClosure<Void>?
    
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: ILDirectMessagesCollectionView!
    
    // MARK: Message用
    //var library: [Song] = []
    //var ChatMessages: [ChatEntity] = []
    var messages: [ZukMessageModel] = []
    var sys_messages: ZukMessageModel?
    var mainbean: TrackMain?
    var messageEdit_CurrentIndex = -99
    
    //播放器
    var path = Bundle.main.url(forResource: "song", withExtension: "mp3")
    var player:MyAudioPlayer? = nil
    
    //var originalPlayList: [Song] = []
    //var playerTimer = Timer()
    //var tasks: [Int: DispatchWorkItem] = [:]
    var count: Int = 0
    var beeingSeek = false
    var designated: Bool = false
    var SeekTime:Double = 0
    var isReady: Bool = false

    @IBOutlet weak var ControlsViewHeightConstraint: NSLayoutConstraint!
    let ControlsViewHeightConstraint_Fix:CGFloat = 50.0
    //MARK: Title view
    @IBOutlet weak var TitleView: UIView!
    @IBOutlet weak var TitleViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: input view
    @IBOutlet weak var inputContainerView: UIView!
    var inputToolBarContainerView: MyTagKeyBoardView!
    @IBOutlet weak var inputContainerViewHeightConstraint: NSLayoutConstraint!
    var isEditMode: Bool = false
    
    var isRepeatModeOn: Bool = false {
        didSet {
            controlsView.isRepeatModeOn = isRepeatModeOn
        }
    }

    // MARK: Outlets
    @IBOutlet weak var blurredAlbumImageView: UIImageView!
    @IBOutlet weak var sliderView: PlayerSlider!
    @IBOutlet weak var waveVisualizer: WaveVisualizer!
    @IBOutlet weak var controlsView: PlayerControls!
    @IBOutlet var fadeImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

//    public static func configure(withPath path: String) -> MyPandoraPlayer {
//        return MyPandoraPlayer.configure(withPaths: [path])
//    }
    
    public static func configure(withAVItems items: [AVPlayerItem]) -> MyPandoraPlayer {
        let playerVC = MyPandoraPlayerInstance()
        //let songItems = items.flatMap({ return Song(withAVPlayerItem: $0) })
        //playerVC.library = songItems
        playerVC.readyForPlay()
        return playerVC
    }

    func setMsgs( items: [ZukMessageModel])  {
        self.messages = items
    }

    // MARK: Life Cycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        reloadDataFromDB()        //设置显示的Data
        configure()
        setupCollectionViewUI() //设置显示的View
        initMyAudioPlayer()     // init Audio Player
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupInputView()        //设置输入View的逻辑，必须再可以有的frame的时候进行设置
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initInputViewHidden()   //让输入View隐藏
        self.view.backgroundColor = UIColor.clear

        if let image =  UIImage.init(named: "default_background-1"){
            self.fadeImageView.image = image
        }
    }
    
    override open var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: Custom Initialization
    private func configure() {
        configureNavigationBar()
        configurePlayer()
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    //    private func configureBackgroundImage() {
    //        fadeImageView.isHidden = false
    //        blurredAlbumImageView.image = currentSong?.metadata?.artwork
    //    }
    
    // MARK: Player
    func readyForPlay() {
        isReady = true
        //originalPlayList = library
        if !designated { return }
        configurePlayer()
        //self.sliderView.duration = currentSong?.metadata?.duration ?? 0
        //if !player.isPlaying && playImmediately {
        //reloadPlayer()
        //}
        MyAudioPlayerStartOrPause()
    }

    
    private func configurePlayer() {
        configurePlayerControls()
        configurePlayerTimeSlider()
        //player = EZAudioPlayer()
        //player.delegate = self
        //updatePlaybackStatus()
        //currentSongIndex = 0
    }
    
    func updatePlaybackStatus() {
        guard self.controlsView != nil else {
            print("retire early, don't even bother going to the cloud!")
            print("thanks, SO!")
            return
        }
        //guard self.player != nil else {
        //            print("retire early, don't even bother going to the cloud!")
        //            print("thanks, SO!")
        //            return
        //}
        //self.controlsView.isPlaying = self.player.isPlaying
        if let player = self.player {
            self.controlsView.isPlaying = player.isPlaying
        }
    }
    
    private func configurePlayerControls() {
        controlsView.status = isReady ? .Ready: .Loading
        controlsView.delegate = self
    }
    
    private func configurePlayerTimeSlider() {
        sliderView.delegate = self
        sliderView.progress = defaultStartProgress
    }
    
    deinit {
        print("----deint---")
        NotificationCenter.default.removeObserver(self)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        MyADManager.shared.showInterstitial()
    }
    
    // MARK: Helpers
    static func MyPandoraPlayerInstance() -> MyPandoraPlayer {
        let storyboard = UIStoryboard(name: storyboardIdentifier, bundle: Bundle(for: MyPandoraPlayer.classForCoder()))
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyPandoraPlayer
    }
}



//    func rewindBackward() {
//        if currentSongIndex > 0 {
//            currentSongIndex -= 1
//        }
//        updatePlaybackStatus()
//    }
//    var currentSongIndex: Int = -1 {
//        didSet {
//            guard currentSongIndex != oldValue else {
//                return
//            }
//            //isReady ? reloadPlayer(): ()
//        }
//    }

//    var currentSong: Song? {
//        guard self.currentSongIndex >= 0,
//            currentSongIndex < self.library.count else {
//                return nil
//        }
//        let song = library[self.currentSongIndex]
//        return song
//    }

//    @IBOutlet private weak var songNameLabel: UILabel!
//    @IBOutlet private weak var songAlbumLabel: UILabel!
//    var isShuffleModeOn: Bool = false {
//        didSet {
//            controlsView.isShuffleModeOn = isShuffleModeOn
//            if isShuffleModeOn {
//                syncColorsWithOriginalList()
//                shufflePlayList()
//            } else {
//                resetPlaylist()
//            }
//            configureBackgroundImage()
//        }
//    }
//    @IBOutlet weak var playerSongListView: PlayerSongList!
//configurePlayerSongListView()
//private func configurePlayerSongListView() {
//        playerSongListView.delegate = self
//        playerSongListView.configure(with: library)
//}
    //    func shufflePlayList() {
    //        //resetPendingTasks()
    ////        library.shuffleInPlace()
    ////        configurePlayer(for: library)
    //    }
        
    //    func configurePlayer(for songs: [Song]) {
    ////        playerSongListView.configure(with: songs)
    //        let deadlineTime = DispatchTime.now() + .microseconds(deadlineTimeOffset)
    //        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
    //            self.currentSongIndex = -1
    //            self.currentSongIndex = 0
    //        }
    //    }

//    func rewindForward() {
//        if currentSongIndex < library.count {
//            currentSongIndex += 1
//        }
//        updatePlaybackStatus()
//    }

//    func updateSongLabels(metadata: MetaData, colors: UIImageColors?) {
//        songNameLabel.changeAnimated(metadata.title ?? unknown, color: colors?.primaryColor ?? .green)
//        songAlbumLabel.changeAnimated(metadata.albumName ?? unknown, color: .lightGray)
//    }

//    private func updateProgress(time: CMTime) {
//        guard !beeingSeek else {
//            beeingSeek = false
//            return
//        }
//
//        let currentSong = library[currentSongIndex]
//
//        guard let duration = currentSong.metadata?.duration, duration > 0 else {
//            self.sliderView.progress = 0
//            return
//        }
//
//        let seconds = Float(CMTimeGetSeconds(time))
//        self.sliderView.duration = duration
//        self.sliderView.progress = seconds / Float(duration)
//    }


    //    func reloadPlayer() {
    //        guard let songItem = currentSong,
    //            let url = songItem.url else {
    //                return
    //        }
    //
    //        let titleColor = songItem.colors
    //
    //        updateForColors(titleColor)
    ////        self.sliderView.duration = currentSong?.metadata?.duration ?? 0
    //
    //        playerSongListView.setCurrentIndex(index: currentSongIndex, animated: true)
    //
    //        //player.audioFile = EZAudioFile(url: url)
    //        path = url
    //
    //        //self.sliderView.duration = player.audioFile.duration
    //
    ////        if player != nil && !player.isPlaying {
    ////            player.play()
    ////        }
    //        /// バックグラウンドでも再生できるカテゴリに設定する
    //        let session = AVAudioSession.sharedInstance()
    //        do {
    //            try session.setCategory(AVAudioSessionCategoryPlayback)
    //        } catch  {
    //            // エラー処理
    //            fatalError("カテゴリ設定失敗")
    //        }
    //    }
    //
    //    func updateForColors(_ colors: UIImageColors?) {
    //        guard let songItem = currentSong,
    //            let metadata = songItem.metadata else {
    //                return
    //        }
    //
    //        if let color = colors?.primaryColor {
    //            rightChannelColor = color
    //        }
    //
    //        if let color = colors?.secondaryColor {
    //            leftChannelColor = color
    //        }
    //
    //        if let color = colors?.primaryColor {
    //            songNameColor = color
    //        }
    //
    //        if let color = colors?.primaryColor {
    //            songAlbumColor = color
    //        }
    //
    //        songNameLabel.changeAnimated(metadata.title ?? unknown, color: songNameColor)
    //        songAlbumLabel.changeAnimated(metadata.albumName ?? unknown, color: songAlbumColor)
    //        waveVisualizer.setColors(left: leftChannelColor, right: rightChannelColor)
    //    }
//    func resetPlaylist() {
//        resetPendingTasks()
//        //library = originalPlayList
//        //configurePlayer(for: originalPlayList)
//    }
//    func resetPendingTasks() {
//        tasks.values.forEach{ $0.cancel() }
//        tasks = [:]
//    }
    
//    func syncColorsWithOriginalList() {
//        for i in 0..<library.count {
//            originalPlayList[i].colors = library[i].colors
//        }
//    }

//    func togglePlay() {
//        guard let audioFile = player.audioFile, audioFile.url == currentSong?.url else {
//            reloadPlayer()
//            return
//        }
//        let isPlaying = self.player.isPlaying
//        if isPlaying {
//            player.pause()
//        } else {
//            player.play()
//        }
//        animatePlayToggling()
//        self.controlsView.isPlaying = self.player.isPlaying
//    }
//    func animatePlayToggling(duration: TimeInterval = animationInterval) {
//        weak var weak_self = self
//        let viewToAnimate = UIImageView(frame: CGRect(x: 0, y: 0, width: animatableViewWidth, height: animatableViewHeight))
//        let imageStr = self.player.isPlaying ? Images.pause: Images.play
//        let image = UIImage(named: imageStr, in: Bundle(for: self.classForCoder), compatibleWith: nil)
//        viewToAnimate.image = image
//        viewToAnimate.alpha = 0
//        viewToAnimate.center = view.center
//
//        view.addSubview(viewToAnimate)
////        view.isUserInteractionEnabled = false
//
//        UIView.animate(withDuration: duration/2, delay: 0, options: .autoreverse, animations: {
//            viewToAnimate.alpha = animatableViewAlpha
//        })
//
//        UIView.animate(withDuration: duration, animations: {
//            viewToAnimate.transform = viewToAnimate.transform.scaledBy(x: animatableViewScale, y: animatableViewScale)
//        })
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + duration - (duration * asyncOffset)) {
//            viewToAnimate.removeFromSuperview()
////            weak_self?.view.isUserInteractionEnabled = true
//        }
//    }
