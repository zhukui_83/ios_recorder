//
//  MYPandoraPlayer.swift
//  MyPandoraPlayer
//
//  Created by Boris Bondarenko on 6/1/17.
//  Copyright © 2017 Applikey Solutions. All rights reserved.
//

//import UIKit
//
//extension MyPandoraPlayer {
//    func setupTableUI() {
//        //        self.tableView.backgroundColor = UIColor(red: 113/255, green: 148/255, blue: 194/255, alpha: 1)
//        self.playerSongListView.backgroundColor = UIColor.clear
//        self.tableView.backgroundColor =  UIColor.clear
//
//        tableView.separatorColor = UIColor.clear // セルを区切る線を見えなくする
//        tableView.estimatedRowHeight = 10000 //
//        tableView.rowHeight = UITableViewAutomaticDimension // Contentに合わせたセルの高さに設定
//        tableView.allowsSelection = false // 選択を不可にする
//        tableView.keyboardDismissMode = .interactive //
//
//        tableView.register(YourChatViewCell.nib, forCellReuseIdentifier: "YourChat")
//        tableView.register(MyChatViewCell.nib, forCellReuseIdentifier: "MyChat")
//
//        self.tableView.dataSource = self;
//        self.tableView.delegate = self;
//        tableView.reloadData()
//    }
//}
//
//extension MyPandoraPlayer: UITableViewDataSource {
//    public func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.ChatMessages.count
//    }
//
//    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let chat = self.ChatMessages[indexPath.row]
//        if chat.isMyChat() {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "MyChat") as! MyChatViewCell
//            cell.clipsToBounds = true //bound外のものを表示しない
//            // Todo: isRead
//            cell.updateCell(text: chat.text, time: chat.time, isRead: true)
//            return cell
//        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "YourChat") as! YourChatViewCell
//            cell.clipsToBounds = true
//            cell.updateCell(text: chat.text, time: chat.time)
//            return cell
//        }
//    }
//}
//
//extension MyPandoraPlayer: UITableViewDelegate {
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath)
//    }
//
//    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 10
//    }
//}
