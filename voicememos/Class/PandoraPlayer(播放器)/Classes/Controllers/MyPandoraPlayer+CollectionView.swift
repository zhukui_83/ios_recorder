//
import UIKit
import Lightbox

extension MyPandoraPlayer:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func reloadDataFromDB(){
        let items = dbDetailTb.getAllByfileName((self.mainbean?.filename)!)
        self.messages = getMessages(items: items)
        self.nowPlaying = (self.mainbean?.filename)!
        self.titleLabel.text = self.nowPlaying
    }
    // MARK:get detail model
    func getMessages(items: Array<RecTagDetail>) -> [ZukMessageModel]{
        var msgs = [ZukMessageModel]()
        for item in items{
            let msg = ZukMessageModel(ZukMessageModel.MessageType(rawValue: item.type)!, msgincoming: .left)
            msg.No = item.detailNo
            msg.body = item.detail
            msg.ImageName = item.imgname
            msg.url = item.imgname
            //msg.isIncomingType = .left
            msg.TagTime = item.TagTime
            do {
                let fileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: item.imgname)
                let imageData = try Data(contentsOf: fileURL!)
                msg.image = UIImage(data: imageData)
            } catch {
                print("Error loading image : \(error)")
            }
            msg.senderName = sendname_left
            msgs.append(msg)
        }
        
        getSysMessage()
        if (sys_messages != nil){
            msgs.insert(sys_messages!, at: 0)
        }
        return msgs
    }
    
    func getSysMessage(){
        sys_messages = nil
        var str = ""
        if let title = mainbean?.title{
            str += "" + title
        }
        if let comment = mainbean?.comment{
            if str.count > 0{
                str += "\n " + comment
            }else{
                str = comment
            }
        }
        if str.count > 0{
            sys_messages = self.TxtMessage(with: str)
            sys_messages?.isIncomingType = .center
        }
    }
    
    func setupCollectionViewUI() {
        //self.playerSongListView.backgroundColor = UIColor.clear
        //self.playerSongListView.isHidden = true
        //self.tableView.backgroundColor =  UIColor.clear
        //self.tableView.isHidden = true
        self.collectionView.register(ILIncomingMessageCollectionViewCell.nib, forCellWithReuseIdentifier: ILIncomingMessageCollectionViewCell.identifier)
        self.collectionView.register(ILOutgoingMessageCollectionViewCell.nib, forCellWithReuseIdentifier: ILOutgoingMessageCollectionViewCell.identifier)
        self.collectionView.register(LeftMessageCollectionViewCell.nib, forCellWithReuseIdentifier: LeftMessageCollectionViewCell.identifier)
        self.collectionView.register(CenterMessageCollectionViewCell.nib, forCellWithReuseIdentifier: CenterMessageCollectionViewCell.identifier)
        self.collectionView.register(RightMessageCollectionViewCell.nib, forCellWithReuseIdentifier: RightMessageCollectionViewCell.identifier)
        
        self.collectionView.messages = self.messages
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.clear
        
        DispatchQueue.main.async {
            let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
            context.invalidateFlowLayoutCache = true
            (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
            //self.scrollToLastItem(animated: false)
        }
    }

    
    func prepareDemoMessages() -> [ILMessage] {
        return messages
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message = self.messages[indexPath.item]
        
        if message.isIncomingType == .center{
            var cell: CenterMessageCollectionViewCell!
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: CenterMessageCollectionViewCell.identifier, for: indexPath) as! CenterMessageCollectionViewCell
            cell.delegate = self
            cell.configure(message: message)
            return cell!
        }else{
            if message.isIncomingType == .right {
                var cell: RightMessageCollectionViewCell!
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: RightMessageCollectionViewCell.identifier, for: indexPath) as! RightMessageCollectionViewCell
                cell.delegate = self
                cell.configure(message: message)
                return cell!
            } else {
                var cell: LeftMessageCollectionViewCell!
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: LeftMessageCollectionViewCell.identifier, for: indexPath) as! LeftMessageCollectionViewCell
                cell.delegate = self
                cell.configure(message: message,editmode:self.isEditMode)
                //if indexPath == self.StartAnimation_IndexPath     {
                //                    cell.startAlphaAnimation()
                //                    self.StartAnimation_IndexPath = nil
                //}
                return cell!
            }
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.collectionView.collectionViewLayout as! ILDirectMessagesCollectionViewFlowLayout).sizeForItem(at: indexPath)
        return size
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let message = self.messages[indexPath.item]
        
        if message.isIncomingType == .center{

        }else{
            if message.isIncomingType == .right {
            } else {
                do {
                    if message.MsgType == .image, let imagename = message.ImageName{
                        let fileURL = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename:imagename )
                        let imageData = try Data(contentsOf: fileURL!)
                        let img = UIImage(data: imageData)
                        let images = [
                            LightboxImage(
                                image: img!,
                                text: ""
                            ),
                            ]
                        
                        let controller = LightboxController(images: images)
                        controller.dynamicBackground = true
                        controller.modalPresentationStyle = .fullScreen
                        present(controller, animated: true, completion: nil)
                    }
                    if message.MsgType == .text , let text = message.body{
                    }
                } catch {
                    print("Error loading image : \(error)")
                }

            }
        }
    }
}

//MARK:动画自动滚动到所定的时间
extension MyPandoraPlayer {
    func animateSending(msg_no: Int, inputContainerView: ILDirectMessagesInputContainerView?) {
        self.collectionView.reloadData()
        if let inputContainerView  = inputContainerView{
            guard let textView = inputContainerView.textView else { return }
            textView.text = nil
            inputContainerView.layoutTextView(with: textView.text)
        }
        if let index = self.messages.index(where: {$0.No == msg_no}){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.scrollToItem(index: index, animated: true)
            }
        }
    }
    func scrollToItem(index: Int ,animated: Bool) {
        if index < 0 {return }
        if self.collectionView.numberOfSections == 0 {
            return
        }
        let lastIndexPath = IndexPath(item: index, section: 0)
        if self.collectionView.numberOfSections < lastIndexPath.section {
            return
        }
        //StartAnimation_IndexPath = lastIndexPath
        for indexpath in self.collectionView.indexPathsForVisibleItems {
            print(indexpath)
            if indexpath.row == lastIndexPath.item && indexpath.section == lastIndexPath.section{
                //self.collectionView.reloadData()
                return
            }
        }
        let contentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
        if contentHeight < self.collectionView.bounds.height {
            let visibleRect = CGRect(x: 0.0, y: contentHeight, width: 0.0, height: 0.0)
            self.collectionView.scrollRectToVisible(visibleRect, animated: animated)
            return
        }
        
        guard let collectionViewLayout = self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout else { return }
        let cellSize = collectionViewLayout.sizeForItem(at: lastIndexPath)
        let boundsHeight = self.collectionView.bounds.height
        //- self.inputContainerView.frame.height
        let position =  (cellSize.height > boundsHeight) ? UICollectionViewScrollPosition.bottom : UICollectionViewScrollPosition.top

        self.collectionView.scrollToItem(at:lastIndexPath, at: position, animated: animated)
    }
}
extension MyPandoraPlayer:MessageCollectionViewCellDelegate{
    func MessageMenuBtnClick(_ bean: ZukMessageModel?) {
        if self.isEditMode == false{
            return 
        }
        
        let shareMenu = UIAlertController(title: nil, message: "Menu", preferredStyle: .actionSheet)
        let editAction = UIAlertAction(
            title: NSLocalizedString("rec_view_tag_editdialog_edit", comment: ""),
            style: .default, handler: {
                (action:UIAlertAction) -> Void in
                self.startEditTagBean(bean)
        })
        let delAction = UIAlertAction(
            title: NSLocalizedString("rec_view_tag_editdialog_delete", comment: ""),
            style: .default, handler: {
                (action:UIAlertAction) -> Void in
                self.deleteTag(bean)
        })
        let cancelAction = UIAlertAction(title:
            NSLocalizedString("rec_view_tag_editdialog_cancel", comment: ""), style: .cancel, handler: nil)
        shareMenu.addAction(editAction)
        shareMenu.addAction(delAction)
        shareMenu.addAction(cancelAction)
        shareMenu.modalPresentationStyle = .popover
        if let presentation = shareMenu.popoverPresentationController {
            presentation.barButtonItem = navigationItem.rightBarButtonItems?[0]
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            shareMenu.popoverPresentationController?.sourceView = self.view
            //let screenSize = UIScreen.main.bounds
            //let titleview = self.TitleView
            shareMenu.popoverPresentationController?.sourceRect = self.TitleView.frame
            self.present(shareMenu, animated: true, completion: nil)
        }else{
            
            self.present(shareMenu, animated: true, completion: nil)
        }
    }
    //MARK:更新数据
    func deleteTag(_ bean: ZukMessageModel?){
        for (i,item) in self.messages.enumerated(){
            if item.No == bean?.No{
                WriteToDB_DeleteTag(model: bean)
                self.messages.remove(at: i)
                self.collectionView.messages = self.messages
                self.collectionView.deleteItems(at: [IndexPath.init(row: i, section: 0)])
//                self.collectionView.invalidateIntrinsicContentSize()
                DispatchQueue.main.async {
                    let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
                    context.invalidateFlowLayoutCache = true
                    (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
//                    self.scrollToLastItem(animated: false)
//                    self.scrollToItem(index: i-1, animated: true)
                }
                return
            }
        }
    }
}

//    func initDataFromDB(){
//        let items = dbDetailTb.getAllByfileNo((self.mainbean?.filename)!)
//        self.messages = getMessages(items: items)
//        self.nowPlaying = (self.mainbean?.filename)!
//        self.titleLabel.text = self.nowPlaying
//    }

//    func scrollToLastItem(animated: Bool) {
//        if self.collectionView.numberOfSections == 0 {
//            return
//        }
//        let lastIndexPath = IndexPath(item: self.collectionView.numberOfItems(inSection: 0) - 1, section: 0)
//
//        if self.collectionView.numberOfSections < lastIndexPath.section {
//            return
//        }
//        let contentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
//        if contentHeight < self.collectionView.bounds.height {
//            let visibleRect = CGRect(x: 0.0, y: contentHeight, width: 0.0, height: 0.0)
//            self.collectionView.scrollRectToVisible(visibleRect, animated: animated)
//            return
//        }
//        guard let collectionViewLayout = self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout else { return }
//        let cellSize = collectionViewLayout.sizeForItem(at: lastIndexPath)
//        let boundsHeight = self.collectionView.bounds.height
//        //- self.inputContainerView.frame.height
//        let position =  (cellSize.height > boundsHeight) ? UICollectionViewScrollPosition.bottom : UICollectionViewScrollPosition.top
//        self.collectionView.scrollToItem(at:lastIndexPath, at: position, animated: animated)
//    }
//    func animateSending(animated: Bool, inputContainerView: ILDirectMessagesInputContainerView?) {
//        // Reset the textview
//        if let inputContainerView  = inputContainerView{
//            guard let textView = inputContainerView.textView else { return }
//            textView.text = nil
//            inputContainerView.layoutTextView(with: textView.text)
//        }
//        // Update the layout
//        self.collectionView.reloadData()
//        self.scrollToLastItem(animated: animated)
//    }
