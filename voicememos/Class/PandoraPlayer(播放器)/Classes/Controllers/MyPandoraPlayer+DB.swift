import UIKit

// MARK: - DB
extension MyPandoraPlayer {
    // MARK: Detail DB -----
    func WriteToDB_AddTag(fileNo: String, model:ZukMessageModel)->Int{
        let detailbean = RecTagDetail.init()
        detailbean.type = model.MsgType.rawValue
        detailbean.fileName = (self.mainbean?.filename)!
        detailbean.detailNo = model.No
        if let body = model.body{
            detailbean.detail = body
        }
        if let imgname = model.ImageName{
            detailbean.imgname = imgname
        }
        detailbean.TagTime = model.TagTime //time
        if let url = model.url{
            detailbean.url = url
        }
        detailbean.senderName = model.senderName//senderName
        detailbean.isIncomingType = model.isIncomingType.rawValue//isIncoming
        dbDetailTb.add(detailbean)
        return detailbean.detailNo
    }
    // MARK: delete tag
    func WriteToDB_DeleteTag(model: ZukMessageModel?){
            let fileName = self.mainbean?.filename
            let detailno = model?.No
        dbDetailTb.deleteByfileNameAndNo(fileName: fileName!,detailNo: detailno!)
    }
    // MARK: up tag
    func WriteToDB_UpdateTag(fileNo: String, model:ZukMessageModel){
        self.WriteToDB_DeleteTag(model: model)
        self.WriteToDB_AddTag(fileNo: fileNo, model: model)
    }
    
    // MARK: 重新设置title和comment
    func UpdatTilteAndComment(title:String,comment:String){
        do {
            // File Rename
            mainbean?.title = title
            mainbean?.comment = comment
            dbMainTb.updateByID(mainbean!)
            self.reloadDataFromDB()
        } catch {
            print("failed to set resource value")
        }
    }
}
