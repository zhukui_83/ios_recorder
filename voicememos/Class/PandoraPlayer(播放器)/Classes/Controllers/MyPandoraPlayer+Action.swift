//
//  MYPandoraPlayer.swift
//  MyPandoraPlayer
//

import UIKit
import SoundWave
import AVFoundation
import MobileCoreServices

extension MyPandoraPlayer{
    //send button
    func didTapSendButton(inputContainerView: MyTagKeyBoardView) {
        if inputContainerView.editTextView.text.isEmpty {
            self.EndEditTag()
            return
        }
        self.addTextTag(str: inputContainerView.editTextView.text)
    }
    func didTapTagButton(inputContainerView: MyTagKeyBoardView) {
        addTextTag(str: nil)
    }
    func addTextTag(str: String?){
        var msgBody = ""
        var msgno = -1
        if let str = str {
            msgBody = str
        }else{
            msgBody = String(format: "%@ %3d",
                             NSLocalizedString("rec_view_tag", comment: ""),
                             self.getNextMessageNo())
        }
        var upd_flg = 0
        if messageEdit_CurrentIndex >= 0{
            self.messages[messageEdit_CurrentIndex] = self.TxtMessage(with:msgBody,msg_index: messageEdit_CurrentIndex)
            self.WriteToDB_UpdateTag(fileNo: (self.mainbean?.filename)!, model:self.messages[messageEdit_CurrentIndex])
            msgno = self.messages[messageEdit_CurrentIndex].No
            upd_flg = 1//update
        }else{
            let message = self.TxtMessage(with: msgBody)
            message.isIncomingType = .left
            msgno = WriteToDB_AddTag(fileNo: (self.mainbean?.filename)!, model:message)
            upd_flg = 0//insert
        }
        self.reloadDataFromDB()
        self.collectionView.messages = self.messages
        self.EndEditTag()

//        self.animateSending(animated: true, inputContainerView: nil)
//        self.animateSending(msg_no: msgno, inputContainerView: nil)
        resetCollectionViewCellWith(msgno:msgno,flg:upd_flg)
    }
    func addImgTag(img:UIImage,url:URL){
        var msgno = -1
        var upd_flg = 0
        if messageEdit_CurrentIndex >= 0{
            self.messages[messageEdit_CurrentIndex] = self.ImgMessage(with:img,msg_index: messageEdit_CurrentIndex)
            WriteToDB_UpdateTag(fileNo: (self.mainbean?.filename)!, model:self.messages[messageEdit_CurrentIndex])
            msgno = self.messages[messageEdit_CurrentIndex].No
            upd_flg = 1//update
        }else{
            let mediaMessage1 = self.ImgMessage(with: img)
//            self.messages.append(mediaMessage1)
            msgno = WriteToDB_AddTag(fileNo: (self.mainbean?.filename)!,
                             model:mediaMessage1)
            upd_flg = 0//insert
        }
        self.reloadDataFromDB()
        self.collectionView.messages = self.messages
//        self.collectionView.reloadData()
//        self.collectionView.layoutIfNeeded()
        self.EndEditTag()
//        self.animateSending(msg_no: msgno, inputContainerView: nil)
        resetCollectionViewCellWith(msgno:msgno,flg:upd_flg)
    }
    func resetCollectionViewCellWith(msgno:Int,flg:Int){
        var indexrow = -1
        for (index, element) in self.messages.enumerated() {
            if element.No == msgno{
                indexrow = index
            }
        }
        if indexrow >= 0{
            let temp1 = IndexPath.init(row: indexrow, section: 0)
            if flg == 0{
                self.collectionView.insertItems(at: [temp1])
                self.collectionView.invalidateIntrinsicContentSize()
            }else{
                self.collectionView.reloadItems(at: [temp1])
                self.collectionView.invalidateIntrinsicContentSize()
            }
            DispatchQueue.main.async {
                let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
                context.invalidateFlowLayoutCache = true
                (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
                //                    self.scrollToLastItem(animated: false)
                //                    self.scrollToItem(index: i-1, animated: true)
                if flg == 0 {
                    self.scrollToItem(index: indexrow, animated: true)
                }
            }
        }
    }
}

extension MyPandoraPlayer: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func PickupImage() {
        photo_picker.sourceType = .photoLibrary
        photo_picker.allowsEditing = false
        photo_picker.delegate = self
        photo_picker.mediaTypes = [kUTTypeImage as String]
        photo_picker.modalPresentationStyle = .fullScreen
        present(photo_picker, animated: true, completion: nil)
    }
    func callCamera(_ animated: Bool) {
        photo_picker.sourceType = .camera
        photo_picker.delegate = self
        photo_picker.modalPresentationStyle = .fullScreen
        present(photo_picker, animated: true, completion: nil)
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if picker.sourceType == .camera{
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        addImgTag(img: image,url: URL.init(string: "https://cdn.vox-cdn.com/uploads/chorus_image/image/55102943/692626382.0.jpg")!)
        photo_picker.dismiss(animated: true) {
        }
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        photo_picker.dismiss(animated: true) {
        }
    }
}

//MARK:- 处理Messag的逻辑
extension MyPandoraPlayer {
    func getNextMessageNo()->Int{
        let max_detailno = dbDetailTb.getMaxDetailNoWithfileName((self.mainbean?.filename)!)
        return max_detailno + 1
//        return self.messages.count + 1
    }
    func TxtMessage(with body: String, msg_index: Int? = nil) -> ZukMessageModel {
        if let index = msg_index{
            let message = self.messages[index]
            message.MsgType = ZukMessageModel.MessageType.text
            message.body = body
            return message
        }else{
            let message = ZukMessageModel(ZukMessageModel.MessageType.text, msgincoming:.left)
            message.No = getNextMessageNo()
            message.isIncomingType = .left
            message.senderName = sendname_left
            message.TagTime = getIntervalTime()
            message.date = Date(timeIntervalSinceNow: 0)
            message.body = body
            return message
        }
    }
    func ImgMessage(with img: UIImage, msg_index: Int? = nil) -> ZukMessageModel {
        if let index = msg_index{
            let message = self.messages[index]
            message.MsgType = ZukMessageModel.MessageType.image
            message.image = img
            message.body = ""
            let imgname = saveImage(image: img)
            message.ImageName = imgname
            return message
        }else{
            let message = ZukMessageModel(ZukMessageModel.MessageType.image, msgincoming: .left)
            message.MsgType = ZukMessageModel.MessageType.image
            message.date = Date(timeIntervalSinceNow: 0)
            message.isIncomingType = .left
            message.senderName = sendname_left
            message.image = img
            let imgname = saveImage(image: img)
            message.ImageName = imgname
            message.url = imgname
            message.No = getNextMessageNo()
            message.TagTime = getIntervalTime()
            return message
        }
    }
    func getIntervalTime()->Double{
        return self.sliderView.nowTime
    }
    func saveImage (image: UIImage) -> String {
        let imgname = "img"+MyClassFile.sharedInstance.getStrWithNow()+".jpg"
        let path = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: imgname)
        let jpgImageData = UIImageJPEGRepresentation(image, 0.8)
        do {
            try jpgImageData?.write(to: path!, options: .atomic)
            return imgname
        } catch {
            print(error)
            return ""
        }
    }
}

//MARK:- 处理重新命名
extension MyPandoraPlayer:TitleEditViewControllerDelegate {
    func renameRecFileName(filename: String, title: String, desc: String) {
        if title.removeWhitespace().utf8.count > 0{
            UpdatTilteAndComment(title: title, comment: desc)
        }
        self.EndEditTag()
        self.reloadDataFromDB()
        self.collectionView.messages = self.messages
        self.collectionView.reloadData()
        DispatchQueue.main.async {
            let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
            context.invalidateFlowLayoutCache = true
            (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
                self.scrollToItem(index:0, animated: true)
        }

    }
    func didTapEditButton(inputContainerView: MyTagKeyBoardView) {
        self.popupRenameDialog()
    }
    func popupRenameDialog(){
        var filename = mainbean?.filename
        var title = mainbean?.title
        var desc = mainbean?.comment
        let alertView = TitleEditViewController(filename:filename!, title: title!, desc: desc!, mode:1)
        alertView.deleage = self
        present(alertView, animated: true, completion: nil)
    }
    func cancelClicked() {
        self.EndEditTag()
    }
}
