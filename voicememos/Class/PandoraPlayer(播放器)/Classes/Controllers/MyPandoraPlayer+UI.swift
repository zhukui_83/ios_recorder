//
//  MYPandoraPlayer.swift
//  MyPandoraPlayer
//
//  Created by Boris Bondarenko on 6/1/17.
//  Copyright © 2017 Applikey Solutions. All rights reserved.
//

import UIKit

//MARK:处理输入View的视图
extension MyPandoraPlayer:  UITextViewDelegate{
    
    override open func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        super.didRotate(from: fromInterfaceOrientation)
        self.collectionView.reloadData()
    }
    
    func addInputView(){
        self.inputContainerView.addSubview(self.inputToolBarContainerView)
        inputToolBarContainerView.translatesAutoresizingMaskIntoConstraints = false
        inputToolBarContainerView.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        inputToolBarContainerView.centerYAnchor.constraint(equalTo: inputContainerView.centerYAnchor).isActive = true
        
        inputToolBarContainerView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        inputToolBarContainerView.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor).isActive = true
        

    }
    
    func setupInputView(){
        self.inputToolBarContainerView = MyTagKeyBoardView(frame: self.inputContainerView.bounds)
        self.inputToolBarContainerView.delegate = self
        self.inputToolBarContainerView.editTextView.delegate = self
        self.inputToolBarContainerView.resignFirstResponder()
        
        self.inputToolBarContainerView.minimumContainerHeight = 50
        self.inputToolBarContainerView.maximumContainerHeight = 150

        
        addInputView()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillChangeFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChangeFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)

//        self.BottomViewHeight.constant = BottomViewHeight_Fix
        self.TitleViewHeightConstraint.constant = 0
        self.ControlsViewHeightConstraint.constant = ControlsViewHeightConstraint_Fix
        
        self.inputToolBarContainerView.layer.backgroundColor = UIColor.clear.cgColor
        self.inputToolBarContainerView.menuView.layer.backgroundColor = UIColor.clear.cgColor
        self.inputToolBarContainerView.editTextView.layer.backgroundColor = UIColor.clear.cgColor
        //self.inputContainerView.layer.backgroundColor = UIColor.clear.cgColor
        self.inputToolBarContainerView.setColorMode(color: UIColor.white)
        
        self.inputContainerView.addSubview(self.inputToolBarContainerView)

        self.inputContainerView.layer.shadowColor = UIColor.darkGray.cgColor
        self.inputContainerView.layer.shadowOffset = CGSize.init(width: -2, height: -2)
        self.inputContainerView.layer.shadowOpacity = 20
        //        self.sliderView.layer.shadowColor = UIColor.darkGray.cgColor
        //        self.sliderView.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        //        self.sliderView.layer.shadowOpacity = 20
        self.controlsView.layer.shadowColor = UIColor.darkGray.cgColor
        self.controlsView.layer.shadowOffset = CGSize.init(width: -2, height: -2)
        self.controlsView.layer.shadowOpacity = 20
    }
    func switchInputViewShowOrHidden(){
        if self.inputContainerView.isHidden==true{
            self.inputContainerView.isHidden = false
            self.inputContainerView.setNeedsLayout()
            self.isEditMode = true
        }else{
            self.inputContainerView.isHidden = true
            self.inputContainerViewHeightConstraint.constant = 0.0
            self.isEditMode = false
        }
        EndEditTag()
        self.collectionView.reloadData()
    }
    
    func initInputViewHidden(){
        if self.isEditMode == false{
            self.inputContainerView.isHidden = true
            self.inputContainerViewHeightConstraint.constant = 0.0
        }
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        self.setKeyboardOffset(notification: notification)
        print("keyboardWillShow-----------------")
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print("keyboardWillHide-----------------")
//        self.BottomViewHeight.constant = self.BottomViewHeight_Fix
        self.ControlsViewHeightConstraint.constant = ControlsViewHeightConstraint_Fix
    }
    
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        self.setKeyboardOffset(notification: notification)
        print("keyboardWillChangeFrame-----------------")
    }
    
    @objc func keyboardDidChangeFrame(notification: NSNotification) {
    }
    
    func setKeyboardOffset(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        }else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        guard let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let options = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt else { return }
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.init(rawValue: options), animations: {
            self.ControlsViewHeightConstraint.constant = keyboardHeight - 55
        }, completion: { (success) in
        })
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        self.resetInputToolBarContainerViewUI()
    }
    
    override open func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11.0, *) {
            super.viewSafeAreaInsetsDidChange()
            print(view.safeAreaInsets.bottom)
            print("viewSafeAreaInsetsDidChange")
        }
    }
    
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true);
        return true;
    }
    //Reset InputToolBarContainerView UI
    func resetInputToolBarContainerViewUI(){
        self.inputToolBarContainerView.resetViewHeight()
    }
}
//MARK:-输入框的各种控制
extension MyPandoraPlayer: MyTagKeyBoardViewDelegate {
    func didChangeSize(inputContainerView: MyTagKeyBoardView, size: CGSize) {
        if self.inputToolBarContainerView.isHidden || self.inputContainerView.isHidden{
            self.inputContainerViewHeightConstraint.constant = 0
        }else{
            
            self.inputContainerViewHeightConstraint.constant = size.height
            self.inputToolBarContainerView.translatesAutoresizingMaskIntoConstraints = true
            self.inputToolBarContainerView.frame.size = CGSize(width:self.inputToolBarContainerView.frame.size.width, height: size.height)
        }
//        self.scrollToLastItem(animated: false)
    }
    func didTapPhotoButton(inputContainerView: MyTagKeyBoardView) {
        self.PickupImage()
    }
    func didTapCameraButton(inputContainerView: MyTagKeyBoardView) {
        self.callCamera(true)
    }
    func startEditTagBean(_ bean: ZukMessageModel?){
        self.inputToolBarContainerView.editTextView.text = bean?.body
        self.inputToolBarContainerView.editTextView.becomeFirstResponder()
        for (i,item) in self.messages.enumerated(){
            if item.No == bean?.No{
                messageEdit_CurrentIndex = i
            }
        }
    }
    func EndEditTag(){
        self.messageEdit_CurrentIndex = -1
        self.view.endEditing(true)
        self.inputToolBarContainerView.editTextView.endEditing(true)
        self.inputToolBarContainerView.editTextView.text = ""
        self.resetInputToolBarContainerViewUI()
    }
}
