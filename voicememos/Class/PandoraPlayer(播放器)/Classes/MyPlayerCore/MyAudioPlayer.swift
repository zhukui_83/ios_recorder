//
//  MyAudioPlayer.swift
//  Audio Player
//
//  Created by ksymac on 2019/09/29.
//  Copyright © 2019 Or Fleisher. All rights reserved.
//

import Foundation
import AVFoundation

protocol MyAudioPlayerDelegate {
    func SetDuration(duration: CGFloat)
    func SetProgress(duration: CGFloat, currentTime: Double)
    func playerStart()
    func playerPause()
    func playerStop()
}
protocol MyAudioPlayerControlsDelegate: class {
    func onRepeat(b:Bool)
}

class MyAudioPlayer: NSObject{
    var audioPlayer: AVAudioPlayer? = nil
    var path = Bundle.main.url(forResource: "song", withExtension: "mp3")
    var duration:CGFloat = 0.0
    var currentTime:Double = 0.0
    var timer: Timer? = nil
    var delegate: MyAudioPlayerDelegate?
    var isPlaying: Bool = false
    var isRepeatMode: Bool = false
    
    private weak var displayLink: CADisplayLink?
    
    init(delegate : MyAudioPlayerDelegate){
        super.init()
        self.delegate = delegate
    }
    
    func setFileUrl(url:URL?){
        guard  let url = url else {
            return
        }
        path = url
        do {
            if let unpackedPath = path {
                try audioPlayer = AVAudioPlayer(contentsOf: unpackedPath)
                audioPlayer?.delegate = self
                duration = CGFloat(audioPlayer?.duration ?? 0.0)
                self.delegate?.SetDuration(duration: duration)
            }
        } catch {
            print(error)
        }
    }
    func playStart(){
        audioPlayer?.play()
        self.isPlaying = true
        self.delegate?.playerStart()
        
        if  self.displayLink == nil {
            let link  = CADisplayLink(target: self, selector: #selector(update(_:)))
            link.preferredFramesPerSecond = 2
            link.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
            displayLink = link
        }
    }
    func playStop(){
        audioPlayer?.stop()
        self.isPlaying = false
        audioPlayer?.currentTime = 0
        displayLink?.invalidate()
        self.delegate?.playerStop()
    }
    func playPause(){
        audioPlayer?.pause()
        self.isPlaying = false
        self.delegate?.playerPause()
    }
    @objc func update(_ displayLink: CADisplayLink) {
        self.currentTime = self.audioPlayer?.currentTime ?? 0.0
        print(self.audioPlayer?.currentTime)
        self.delegate?.SetProgress(duration: self.duration, currentTime: self.currentTime)
    }
    deinit {
        print("---deinit--- MyAudioPlayer")
    }
}

extension MyAudioPlayer:MyAudioPlayerControlsDelegate{
    func seekTime(progress: Float){
        audioPlayer?.currentTime = Double(duration) * Double(progress)
    }
    func onRepeat(b: Bool) {
        isRepeatMode = b
    }
}

extension MyAudioPlayer: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
         print("Music Finish")
        self.currentTime = self.audioPlayer?.currentTime ?? 0.0
        self.delegate?.SetProgress(duration: self.duration, currentTime: self.currentTime)
        if isRepeatMode == true {
            seekTime (progress: 0)
            playStart()
        } else {
            displayLink?.invalidate()
            displayLink = nil
            isPlaying = false
            self.delegate?.playerStop()
        }
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
         print("Error")
    }
}
