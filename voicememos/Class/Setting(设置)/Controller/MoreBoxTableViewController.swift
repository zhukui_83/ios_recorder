//
//  MoreBoxTableViewController.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/10/03.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import UIKit
import MessageUI


class MoreBoxTableViewController: UITableViewController , MFMailComposeViewControllerDelegate{
    
    //@IBOutlet weak var tableView: UITableView!
    var SectionTexts = [NSLocalizedString("setting_addbox_default_box", comment: ""),NSLocalizedString("setting_addbox_addition_box", comment: "")]
    let dbMainTb = DBMainTable()//model
    let dbStatusTb = DBStatusTable()//model
    var section0list : Array<TrackStatus> = []
    var section1list : Array<TrackStatus> = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //self.tableView.layer.borderWidth = 1;
        self.tableView.allowsSelection = false;
        self.title = NSLocalizedString("setting_addbox_vc_title", comment: "")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UINavigationBar.appearance().barStyle = UIBarStyle.default
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        let addButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add,
                                            target: self,
                                            action: #selector(MoreBoxTableViewController.addBox(_:)))
        
        self.navigationItem.rightBarButtonItem = addButton
        
        section0list = dbStatusTb.getSection1List()
        section1list = dbStatusTb.getSection2List()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return SectionTexts.count;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            return section0list.count;
        }
        else{
            return section1list.count;
        }
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return SectionTexts[section];
    }
    //row title
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "moreTableViewCell")
        
        if (indexPath.section == 0){
            cell.textLabel?.text = section0list[indexPath.row].statusname + " (" + String(section0list[indexPath.row].count) + ")";//件
        }else{
            cell.textLabel?.text = section1list[indexPath.row].statusname + " (" + String(section1list[indexPath.row].count) + ")";//件
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 0){
            onClickStartMailerBtn()}
        else{
            popCategorySetting()
        }
    }
    func popCategorySetting(){
        
    }
    
    func onClickStartMailerBtn() {
       //self.navigationController?.popViewControllerAnimated(true)
    }
    
    @objc func addBox(_ sender: UIBarButtonItem) {
//        #if FREE
//            if section1list.count>0 {
//                SCLAlertView().showError("ボックス追加制限", subTitle:"無料版の整理ボックスは最大一つを追加ですます。\n正式版またはオプションサービスをご購入ください", closeButtonTitle:"OK")
//                return;
//            }
//        #else
//        #endif
        showaddbox(flg: 0,indexPath: nil)
    }
    func editTable(_ indexPath: IndexPath) {
        showaddbox(flg: 1,indexPath: indexPath)
    }
    func showaddbox(flg:Int,indexPath: IndexPath?){
        var tilte = NSLocalizedString("setting_addbox_dialog_title_add", comment: "")//"ボックス追加"
        if(flg==1){
            tilte = NSLocalizedString("setting_addbox_dialog_title_rename", comment: "")//"ボックス名を変更"
        }
        let alert:UIAlertController = UIAlertController(title:tilte,
                                                            message: NSLocalizedString("setting_addbox_dialog_title_inputname", comment: ""),//"ボックス名をご入力ください",
                                                            preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel",
                                                           style: UIAlertActionStyle.cancel,
                                                           handler:{
                                                            (action:UIAlertAction!) -> Void in
            })
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                    style: UIAlertActionStyle.default,handler:{
                      (action:UIAlertAction!) -> Void in
                      let textFields:Array<UITextField>? =  alert.textFields as Array<UITextField>?
                      if textFields != nil {
                            guard let id = textFields?[0].text else{
                               return
                     }
                     if(flg==0){
                        self.insertDb(id)
                     }else{
                        self.updateTable(indexPath!,boxname : id)
                    }
                  }
            })
        alert.addTextField(configurationHandler: {(text:UITextField!) -> Void in
                text.placeholder = NSLocalizedString("setting_addbox_dialog_title_input", comment: "")//"ご入力してください"
                let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
                label.text = NSLocalizedString("setting_addbox_dialog_label", comment: "")//"名称"
                text.leftView = label
                text.leftViewMode = UITextFieldViewMode.always
            if(flg==1){
                text.text = self.section1list[indexPath!.row].statusname
            }
        })
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        present(alert, animated: true, completion: nil)
    }
    

    
    
    func insertDb(_ boxname :String){
        if(boxname.isEmpty){
            return
        }else{
            let trackmain : TrackStatus = TrackStatus()
            trackmain.status = dbStatusTb.getMaxNo()+1
            trackmain.statusname = boxname
            trackmain.indexno = trackmain.status
            
            dbStatusTb.add(trackmain)
            self.refreshView()
        }
    }
    func updateTable(_ indexPath : IndexPath,boxname :String){
        if(boxname.isEmpty){
            return
        }else{
            let trackstatus = self.section1list[indexPath.row]
            trackstatus.statusname = boxname
            dbStatusTb.updateByID(trackstatus)
            self.refreshView()
        }
    }
    //削除
    func delTable(_ indexPath: IndexPath ){
        let id = self.section1list[indexPath.row].rowID
        dbStatusTb.delete(id)
        self.refreshView()
        //listTrackMain.removeAtIndex(rowIndex)
        //tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    }

    func refreshView(){
        section0list = dbStatusTb.getSection1List()
        section1list = dbStatusTb.getSection2List()
        self.tableView.reloadData()
    }
    
    
    // 支持单元格编辑功能
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if(indexPath.section>0){
            return true
        }else{
           return false
        }
    
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.delTable(indexPath)
            //tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if(indexPath.section>0){
            let DelAction = UITableViewRowAction(style: .default, title: NSLocalizedString("common_delete", comment: "")) {
                (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                tableView.isEditing = false
                self.delTable(indexPath)
            }
            let EditAction = UITableViewRowAction(style: .default, title: NSLocalizedString("common_edit", comment: "")) {
                (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
                tableView.isEditing = false
                self.editTable(indexPath)
            }
            EditAction.backgroundColor = UIColor.blue
            
            return [DelAction,EditAction]
        }else{
            return nil
        }
    }
}
