//
//  DefaultViewController.swift
//  Example
//
//  Created by Ben on 01/09/2015.
//  Copyright (c) 2015 bcylin.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import UIKit
import QuickTableViewController
import Weakify
import MessageUI

internal final class RecorderSettingViewController: QuickTableViewController {

  // MARK: - Properties

  private let debugging = Section(title: nil, rows: [NavigationRow(title: "", subtitle: .none)])

  // MARK: - UIViewController

  override func viewDidLoad() {
    super.viewDidLoad()
    title = NSLocalizedString("setting_view_Settings", comment: "")
//    let gear = #imageLiteral(resourceName: "iconmonstr-gear")
//    let globe = #imageLiteral(resourceName: "iconmonstr-globe")
//    let time = #imageLiteral(resourceName: "iconmonstr-time")
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
    UINavigationBar.appearance().barStyle = UIBarStyle.default
    self.tableView.backgroundColor = GlobalBackgroundColor
//    self.setBarItem()
  }
//    func setBarItem(){
//        let closeButton = ComUI.getCloseButton()
//        closeButton.addTarget(self, action: #selector(self.coloseView(_:)),
//                              for: UIControlEvents.touchUpInside)
//        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: closeButton),]
//    }
    @IBAction func ColseVC(_ sender: Any) {
        self.dismiss(animated: true) {
        }
    }
//    @objc func coloseView(_ sender: AnyObject){
//        self.dismiss(animated: true) {
//        }
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initData()
    }
    func initData(){
        let RecType = AudioRecorderManager.shared.getCurrentAudioRecordType()
        let SampleRate = AudioRecorderManager.shared.getCurrentAudioSampleRate()
//        let SampleRateStr = String.init(format: "%.1f Hz", SampleRate.SampleRate)
        tableContents = [
            Section(title: NSLocalizedString("setting_view_recorder_format", comment: ""),
                    rows: [
                NavigationRow(title: NSLocalizedString("setting_view_recorder_type", comment: ""),
                              subtitle: .rightAligned(RecType.RecTypeName), icon: nil, action: weakify(self, type(of: self).showRecType)),
                NavigationRow(title: NSLocalizedString("setting_view_recorder_SampleRate", comment: ""),
                              subtitle: .rightAligned(SampleRate.SampleRateStr), icon: nil, action: weakify(self, type(of: self).showSampleRate))
                ], footer: ""),
            Section(title: NSLocalizedString("setting_view_feedback", comment: ""),
                    rows: [
                        TapActionRow(title: NSLocalizedString("setting_view_contact_us", comment: ""),
                                        action: weakify(self, type(of: self).onClickStartMailerBtn)),
                       TapActionRow(title: NSLocalizedString("setting_view_Review_msg", comment: ""),
                         action: weakify(self, type(of: self).requestReview)
                        )
                ]),
            Section(title: NSLocalizedString("setting_view_setting", comment: ""),
                    rows: [TapActionRow(title: NSLocalizedString("setting_view_addbox", comment: ""),
                             action: weakify(self, type(of: self).popBoxSetting))
                ]),
            Section(title: NSLocalizedString("setting_view_help", comment: ""),
                    rows: [TapActionRow(title: NSLocalizedString("setting_view_onboarding", comment: ""),
                             action: weakify(self, type(of: self).openOnboardingVC))
                ]),
        ]
    }
    
    
    
  // MARK: - UITableViewDataSource
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = super.tableView(tableView, cellForRowAt: indexPath)
    // Alter the cells created by QuickTableViewController
    return cell
  }

  // MARK: - Private Methods

  private func didToggleSelection(_ sender: Row) {
    guard let option = sender as? OptionRowCompatible else {
      return
    }
    let state = "\(option.title) is " + (option.isSelected ? "selected" : "deselected")
    print(state)
    showDebuggingText(state)
  }

  private func didToggleSwitch(_ sender: Row) {
    if let row = sender as? SwitchRowCompatible {
      let state = "\(row.title) = \(row.switchValue)"
      print(state)
      showDebuggingText(state)
    }
  }

  private func showAlert(_ sender: Row) {
    let alert = UIAlertController(title: "Action Triggered", message: nil, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel) { [unowned self] _ in
      self.dismiss(animated: true, completion: nil)
    })
    present(alert, animated: true, completion: nil)
  }
    
    private func showRecType(_ sender: Row) {
        self.navigationController?.pushViewController(RecorderSettingRecType(), animated: true)
    }
    private func showSampleRate(_ sender: Row) {
        self.navigationController?.pushViewController(RecorderSettingSampleRate(), animated: true)
    }
    private func showDebuggingText(_ text: String) {
        debugging.rows = [NavigationRow(title: text, subtitle: .none)]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.tableView.reloadData()
        }
    }
}


extension RecorderSettingViewController:MFMailComposeViewControllerDelegate{
    // MARK:对app进行评价
    func requestReview(){
        MyReviewManager.requestReview(vc: self)
    }
    // MARK:整理ボックス追加
    func popBoxSetting(_ sender: Row){
        // 遷移するViewを定義する.このas!はswift1.2では as?だったかと。
        let detailViewController: MoreBoxTableViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "moreboxtableview") as! MoreBoxTableViewController
        detailViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailViewController, animated:true)
    }
    
    // MARK:ご意見とお問い合わせ
    func onClickStartMailerBtn(_ sender: Row) {
        //メールを送信できるかチェック
        if MFMailComposeViewController.canSendMail()==false {
            print("Email Send Failed")
            return
        }
        let mailViewController = MFMailComposeViewController()
        let toRecipients = ["svalbard.k@gmail.com"]
        //var CcRecipients = ["cc@1gmail.com","Cc2@1gmail.com"]
        //var BccRecipients = ["Bcc@1gmail.com","Bcc2@1gmail.com"]
        mailViewController.mailComposeDelegate = self
        mailViewController.setSubject("About Recorder APP")
        mailViewController.setToRecipients(toRecipients) //宛先メールアドレスの表示
        //mailViewController.setCcRecipients(CcRecipients)
        //mailViewController.setBccRecipients(BccRecipients)
        mailViewController.setMessageBody("", isHTML: false)
        
        self.present(mailViewController, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Email Send Cancelled")
            break
        case MFMailComposeResult.saved.rawValue:
            print("Email Saved as a Draft")
            break
        case MFMailComposeResult.sent.rawValue:
            print("Email Sent Successfully")
            break
        case MFMailComposeResult.failed.rawValue:
            print("Email Send Failed")
            break
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:Onboarding ViewControllerr
    func openOnboardingVC(_ sender: Row){
        OnboardingManager.shared.presentOnboarding(basevc: self)
    }
}

