import UIKit
import QuickTableViewController
import Weakify

internal final class RecorderSettingSampleRate: QuickTableViewController {

  // MARK: - Properties
  private let debugging = Section(title: nil, rows: [NavigationRow(title: "", subtitle: .none)])

  // MARK: - UIViewController

  override func viewDidLoad() {
    super.viewDidLoad()
    title = NSLocalizedString("setting_view_recorder_SampleRate", comment: "")
    

    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
    UINavigationBar.appearance().barStyle = UIBarStyle.default
    self.navigationController?.navigationBar.barStyle = UIBarStyle.black
    self.navigationController?.navigationBar.tintColor = UIColor.white
    
    
    self.tableView.backgroundColor = GlobalBackgroundColor
  }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initData()
    }
    
    var list: [AudioRecordSampleRate] = []
    var currentitem:AudioRecordSampleRate?
    func initData(){
        list = AudioRecorderManager.shared.getAudioSampleRateList()
        currentitem =  AudioRecorderManager.shared.getCurrentAudioSampleRate()
        var options = [OptionRowCompatible]()
        for item in list{
            let b:Bool = (currentitem?.SampleRate == item.SampleRate)
            options.append(OptionRow(title: item.SampleRateStr,isSelected: b, action: weakify(self, type(of: self).didToggleSelection)))
        }
        
        let section = RadioSection(title: "SampleRate", options: options,
                                   footer: "")
        section.alwaysSelectsOneOption = true
        tableContents = [ section, debugging ]
    }
  // MARK: - UITableViewDataSource
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = super.tableView(tableView, cellForRowAt: indexPath)
    // Alter the cells created by QuickTableViewController
    return cell
  }

  // MARK: - Private Methods
  private func didToggleSelection(_ sender: Row) {
    guard let option = sender as? OptionRowCompatible else {
      return
    }
    let state = "\(option.title) is " + (option.isSelected ? "selected" : "deselected")
    print(state)
    showDebuggingText(state)
    if !option.isSelected{
        return
    }
    for item in list{
        if item.SampleRateStr == option.title{
            AudioRecorderManager.shared.setSampleRate(value: item.SampleRate)
        }
    }
  }
  private func showDebuggingText(_ text: String) {
    debugging.rows = [NavigationRow(title: text, subtitle: .none)]
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
      self?.tableView.reloadData()
    }
  }

}
