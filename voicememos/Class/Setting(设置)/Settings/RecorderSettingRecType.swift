//
//  DefaultViewController.swift
//  Example
//
//  Created by Ben on 01/09/2015.
//  Copyright (c) 2015 bcylin.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import UIKit
import QuickTableViewController
import Weakify

internal final class RecorderSettingRecType: QuickTableViewController {

  // MARK: - Properties
  private let debugging = Section(title: nil, rows: [NavigationRow(title: "", subtitle: .none)])

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("setting_view_recorder_type", comment: "")
        
//        let gear = #imageLiteral(resourceName: "iconmonstr-gear")
//        let globe = #imageLiteral(resourceName: "iconmonstr-globe")
//        let time = #imageLiteral(resourceName: "iconmonstr-time")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
        UINavigationBar.appearance().barStyle = UIBarStyle.default
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        self.tableView.backgroundColor = GlobalBackgroundColor
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initData()
    }
    var list: [AudioRecordType] = []
    var currentitem:AudioRecordType?
    func initData(){
        list = AudioRecorderManager.shared.getAudioRecordTypeList()
        currentitem =  AudioRecorderManager.shared.getCurrentAudioRecordType()
        var options = [OptionRowCompatible]()
        for item in list{
            let b:Bool = (currentitem?.RecType == item.RecType)
            options.append(OptionRow(title: item.RecTypeName,isSelected: b, action: weakify(self, type(of: self).didToggleSelection)))
        }
        let section =  RadioSection(title: "Rec Type", options: options,
                                    footer: "")
        section.alwaysSelectsOneOption = true
        tableContents = [section, debugging]
    }
    
  // MARK: - UITableViewDataSource
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = super.tableView(tableView, cellForRowAt: indexPath)
    // Alter the cells created by QuickTableViewController
    return cell
  }

  // MARK: - Private Methods
  private func didToggleSelection(_ sender: Row) {
    guard let option = sender as? OptionRowCompatible else {
      return
    }
    let state = "\(option.title) is " + (option.isSelected ? "selected" : "deselected")
    if !option.isSelected{
        return
    }
    
    print(state)
    showDebuggingText(state)
    
    for item in list{
        if item.RecTypeName == option.title{
            AudioRecorderManager.shared.setAudioRecType(value: item.RecType)
            AudioRecorderManager.shared.setSampleRate(value: AudioRecordSampleRate_Default)
        }
    }
  }
  private func showDebuggingText(_ text: String) {
    debugging.rows = [NavigationRow(title: text, subtitle: .none)]
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
        self?.tableView.reloadData()
    }
  }
}
