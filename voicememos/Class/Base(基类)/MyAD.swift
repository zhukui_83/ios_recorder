//
//  MyAD.swift
//  voicememos
//
//  Created by ksymac on 2019/08/24.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//
import Foundation
import GoogleMobileAds
import FirebaseRemoteConfig

let AdMobID_Banner_ID = "ca-app-pub-5780207203103146/6104370662"
let AdMobID_Interstitial_ID = "ca-app-pub-5780207203103146/7851029159"

class MyADManager:NSObject,GADInterstitialDelegate{
    static let shared = MyADManager()
    var interstitial: GADInterstitial?
    var ios_InterstitialAd_Percent = 10
    var remoteConfig: RemoteConfig?
    
    func initInterstitialAD(){
        getInterstitialDisplayPercent()
        self.resetAdmobADView()
    }
    
    func getInterstitialDisplayPercent(){
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
        remoteConfig?.configSettings = remoteConfigSettings
        let expirationDuration = (remoteConfig?.configSettings.isDeveloperModeEnabled)! ? 0 : 3600
        remoteConfig?.fetch(withExpirationDuration: TimeInterval(expirationDuration)) {
            [weak self] (status, error) -> Void in
            if (status == RemoteConfigFetchStatus.success) {
                // フェッチ成功
                self?.remoteConfig?.activateFetched()
                self?.ios_InterstitialAd_Percent =
                    Int(self?.remoteConfig?.configValue(forKey: "ios_InterstitialAd_Percent").numberValue ?? NSNumber(value: 20))
            } else {
                // フェッチ失敗
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
        }
    }
    func resetAdmobADView(){
        interstitial = createAndLoadInterstitial()
    }
    //初始化全屏广告
    func createAndLoadInterstitial() -> GADInterstitial? {
        let interstitial = GADInterstitial(adUnitID:AdMobID_Interstitial_ID )
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
        self.resetAdmobADView()
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
        self.resetAdmobADView()
    }
    
    func showInterstitial() {
        let iValue = Int.random(in: 0 ... 99)
        print(iValue)
        if(iValue < ios_InterstitialAd_Percent){
            if interstitial?.isReady ?? false {
                let topvc = MyRouter.shared.currentTopVc()
                interstitial!.present(fromRootViewController: topvc)
            } else {
                print("Ad wasn't ready")
            }
        }
    }
}
