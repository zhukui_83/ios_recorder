//
//  MyAnalytics.swift
//  voicememos
//
//  Created by ksymac on 2019/08/04.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//
import AWSPinpoint
import AWSMobileClient

enum EventName:String {
    case RecStart = "RecStart"
    case RecEnd = "RecEnd"
}

class MyAnalytics{
    /** start code copy **/
    var pinpoint: AWSPinpoint?
    /** end code copy **/
    static let shared = MyAnalytics()
    func initWhenLaunched(launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        /** start code copy **/
        // Create AWSMobileClient to connect with AWS
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let error = error {
                print("Error initializing AWSMobileClient: \(error.localizedDescription)")
            } else if let userState = userState {
                print("AWSMobileClient initialized. Current UserState: \(userState.rawValue)")
            }
        }
        
        // Initialize Pinpoint
        let pinpointConfiguration = AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: launchOptions)
        pinpoint = AWSPinpoint(configuration: pinpointConfiguration)
        /** end code copy **/
    }
    
}
extension MyAnalytics{
    func logEvent(eventName:String) {
        if let analyticsClient = pinpoint?.analyticsClient {
            let event = analyticsClient.createEvent(withEventType: eventName)
            event.addAttribute("type", forKey: "ios")
            //event.addAttribute("DemoAttributeValue2", forKey: "DemoAttribute2")
            //event.addMetric(NSNumber(value: arc4random() % 65535), forKey: "EventName")
            analyticsClient.record(event)
            analyticsClient.submitEvents()
        }
    }
    //
    func logStartEvent(recType:String){
        if let analyticsClient = pinpoint?.analyticsClient {
            let event = analyticsClient.createEvent(withEventType: EventName.RecStart.rawValue)
            event.addAttribute("type", forKey: "ios")
            event.addAttribute("RecType", forKey: recType)
            analyticsClient.record(event)
            analyticsClient.submitEvents()
        }
    }
    //
    func logEndEvent(recTime:Double){
        if let analyticsClient = pinpoint?.analyticsClient {
            let event = analyticsClient.createEvent(withEventType: EventName.RecEnd.rawValue)
            event.addAttribute("type", forKey: "ios")
            event.addMetric(NSNumber(value: recTime), forKey: "RecTime")
            analyticsClient.record(event)
            analyticsClient.submitEvents()
        }
    }
}
