//
//  BaseViewController.swift
//  packtrack
//
//  Created by ksymac on 2017/03/28.
//  Copyright © 2017 ZHUKUI. All rights reserved.
//

import Foundation
import UIKit

//public let GlobalBackgroundColor = UIColor.colorWithCustom(239, g: 239, b: 239)
public let GlobalBackgroundColor = UIColor.colorWithCustom(220, g: 220, b: 220)
//public let GlobalHeadColor = UIColor.colorWithCustom(80, g: 180, b: 220)
//public let GlobalHeadColor = UIColor.colorWithCustom(18, g: 148, b: 234)
//public let GlobalHeadColor = UIColor.colorWithCustom(80, g: 175, b: 239)
public let GlobalHeadColor = UIColor.colorWithCustom(113, g: 148, b: 194)
//public let GlobalHeadColor = UIColor.flatSkyBlue()


public let Global_BotoomView_BackGroundColor = UIColor.colorWithCustom(113, g: 148, b: 194)
//public let Global_BotoomView_BackGroundColor = UIColor.flatWhite()
public let Global_BotoomView_FontColor = UIColor.flatLime()


public let CollectionView_CenterCell_BackgroundColor = UIColor.flatSkyBlue()
public let CollectionView_LeftCell_BackgroundColor = UIColor.flatNavyBlue()
public let CollectionView_RightCell_BackgroundColor = UIColor.flatLime()






//rgb(79, 175, 239)
//public let GlobalHeadColor = UIColor.blue
class BaseViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = GlobalHeadColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UINavigationBar.appearance().barStyle = UIBarStyle.default
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
//        view.backgroundColor = GlobalBackgroundColor
    }
}
