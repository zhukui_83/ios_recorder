//
//  MyUINavigationController.swift
//  packtrack
//
//  Created by ksymac on 2017/12/28.
//  Copyright © 2017年 ZHUKUI. All rights reserved.
//

import UIKit
import ChameleonFramework

//设置弹出对话框
class MyUINavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let randomFlatColor = UIColor.randomFlat
//        let randomFlatColorContract = ContrastColorOf(randomFlatColor, returnFlat: true)
        // Nav bar
//        self.navigationBar.barTintColor = randomFlatColor()
        // Tab bar
//        self.tabBar.barTintColor = randomFlatColor
//        self.tabBar.tintColor = randomFlatColorContract
        
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = GlobalHeadColor
        self.navigationBar.tintColor = UIColor.white
        UINavigationBar.appearance().barStyle = UIBarStyle.default
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //
        view.backgroundColor = GlobalBackgroundColor
    }
}
