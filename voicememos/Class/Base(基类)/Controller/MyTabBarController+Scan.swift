//
//  MyTabBarController.swift
//  packtrack
//
//  Created by ksymac on 2017/10/30.
//  Copyright © 2017 ZHUKUI. All rights reserved.
//

import UIKit
//import LBXScan
import TOWebViewController

// MARK: - 设置统一弹出画面，设置全局扫描窗口和变量
// 扫描根窗口
//var ScanNabvc:UINavigationController?
// Web根窗口
var WebNabvc:UINavigationController?
// MARK: 统一弹出画面，弹出公司画面
//var companyNabvc:MyUINavigationController?
//var ScanVC:DIYScanViewController? = nil
//var barcodeScanVC:DIYScanViewController? = DIYScanViewController.init()
extension MyTabBarController{
    func dismissAllPop() {
        WebNabvc?.dismiss(animated: false) {}
        
    }


    // MARK: 扫描结果打开网页
    func popWebView(url:String?){
        guard let urlstr = url else {
                return
        }
        let webvc: TOWebViewController = TOWebViewController.init(urlString: urlstr)
        WebNabvc = MyUINavigationController.init(rootViewController: webvc)
        if let vc = WebNabvc{
            self.present(vc, animated: true) {
            }
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
