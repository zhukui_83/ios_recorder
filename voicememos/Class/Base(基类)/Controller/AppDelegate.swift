//
//  AppDelegate.swift
//  packtrack
//
//  Created by ZHUKUI on 2015/08/05.
//  Copyright (c) 2015年 ZHUKUI. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import GoogleMobileAds
import UserNotifications
import AWSPinpoint
import AWSMobileClient

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    // window 必须
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    //[AnyHashable("google.c.a.c_l"): test, AnyHashable("google.c.a.e"): 1, AnyHashable("google.c.a.ts"): 1524406280, AnyHashable("google.c.a.udt"): 0, AnyHashable("gcm.n.e"): 1, AnyHashable("aps"): {
    //alert = test;}, AnyHashable("google.c.a.c_id"): 8175499402270739430, AnyHashable("gcm.message_id"): 0:1524406281891803%8dadc93f8dadc93f]
    //var tracker: GAITracker?
    //var tabBarController:MyTabBarController?
    //var mainViewController:TrackMainViewController?
    //var toolBoxTableViewController:ToolBoxTableViewController?
    //https://github.com/googlesamples/google-services/blob/master/ios/analytics/AnalyticsExampleSwift/AppDelegate.swift#L27-L35
    //    http://qiita.com/kaneshin/items/71f1c19d094e87e30a07
    // MARK: - 设置状态栏颜色,设置监视网络状态
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        //firebase
        FirebaseApp.configure()
        //开始监视网络
        IJReachability.startMonitoring()
        //开始设置AD和
        initAD()
        //开始启动跟踪
        setupGoogleAnalytics()
        
        //ClassFile Create init folder
        MyClassFile.sharedInstance.createInitFolder()
        
        //ClassFile Create init folder
        MySampleFileManage.shared.createSampleFile()
        //requestMsgToken(application)
        
        //Analytics initial
        MyAnalytics.shared.initWhenLaunched(launchOptions: launchOptions)

        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = GlobalHeadColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barStyle = UIBarStyle.default
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        return true
    }
    func setupGoogleAnalytics() {
        //        GAI.sharedInstance().trackUncaughtExceptions = true;
        //        GAI.sharedInstance().dispatchInterval = 20
        //if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
        //            #if FREE
        //                appDelegate.tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-32908041-2") //voicememos ios
        //            #else
        //                appDelegate.tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-36383173-4")
        //            #endif
        //}
    }
    //请求 fcm token
    func requestMsgToken(_ application: UIApplication){
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }

    let admanager = MyADManager.shared
    func initAD(){
        //设置音乐
        //try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        GADMobileAds.configure(withApplicationID: "ca-app-pub-5780207203103146~8127898772")
    
        //设置静音
        GADMobileAds.sharedInstance().applicationVolume = 0.0
        GADMobileAds.sharedInstance().applicationMuted = true
        
        ComFunc.ver_free = true
        admanager.initInterstitialAD()
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        Analytics.setScreenName("ApplicationActive", screenClass: FIRAnalytics_Screen_Class1)
    }
    
    //comksy-ptrack://search?no=1&type=1
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print("url : \(url.absoluteString)")
        print("scheme : \(url.scheme!)")
        if let host = url.host{
            print("host : \(host)")
            if let port = url.port{
                print("port : \(port)")
            }
            if let query = url.query{
                print("query : \(query)")
            }
            OpenManager_Host = host
        }
        return true
    }
    
    // MARK: 进入前台时，进行处理
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let host = OpenManager_Host {
            print(host)

            OpenManager_Host = nil
            return
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    // MARK: 退出时，关闭些必须关闭的打开窗口
    func applicationWillResignActive(_ application: UIApplication) {
        //tabBarController?.dismissAllPop()
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
}

extension AppDelegate{
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
    }
}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        completionHandler()
    }
    
    //supportedInterfaceOrientationsFor
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if ( UIDevice.current.userInterfaceIdiom == .pad)
        {
            return UIInterfaceOrientationMask.allButUpsideDown
        }
        return UIInterfaceOrientationMask.portrait
    }
}
// [END ios_10_message_handling]
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//        PringManager.shared.createUser(name: "",device: fcmToken)
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
