//
//  MyRouter.swift
//  voicememos
//
//  Created by ksymac on 2019/08/24.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation

class MyRouter:NSObject{
    static let shared = MyRouter()
    
    ///获取当前控制器
    func currentTopVc() ->UIViewController{
        var vc = UIApplication.shared.keyWindow?.rootViewController
        
        if (vc?.isKind(of: UITabBarController.self))! {
            vc = (vc as! UITabBarController).selectedViewController
        }else if (vc?.isKind(of: UINavigationController.self))!{
            vc = (vc as! UINavigationController).visibleViewController
        }else if ((vc?.presentedViewController) != nil){
            vc =  vc?.presentedViewController
        }

        return vc!
    }
}
