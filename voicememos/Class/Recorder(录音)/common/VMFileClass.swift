//
//  VMFileClass.swift
//  voicememos
//
//  Created by ksymac on 2018/2/24.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit
import AVFoundation
import AudioKit


class VMFileClass: NSObject {
    class var sharedInstance : VMFileClass {
        struct Static {
            static let instance : VMFileClass = VMFileClass()
        }
        return Static.instance
    }
    
    func vmfilename() -> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
//        let interval = date.timeIntervalSince1970
        return dateString
    }
  
    func getFileDurationAndFileSize(filename: String) -> (Int, Int) {
        if let url = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record, filename: filename){
            let asset = AVURLAsset(url: URL.init(fileURLWithPath: url.path))
            do {
//                let player = try AVAudioPlayer.init(contentsOf: url)
                var duration = 0
                if let audioFile = EZAudioFile(url: url){
                    duration = (Int)(audioFile.duration)
                }
                return (duration,asset.fileSize!)
            } catch {
                print(error)
            }
        }
        return (0,0)
    }
    
    
    func getRecFileArrStr(rectype:Int,duration:Int,filesize:Int)->(String,String,String){
        let type = AudioRecorderManager.shared.getAudioRecordType(value: AudioRecType.init(rawValue: rectype)!)
        let minutes = duration / 60
        let seconds = duration % 60
        let mediaDuration = String(format:"%02i:%02i",minutes, seconds)

        var sizeMB = Double(filesize / 1024)
        sizeMB = Double(sizeMB / 1024)
        let sizestr = String(format: "%.2f", sizeMB) + " MB"
        
        return (type.FileExtension, mediaDuration, sizestr)
    }
    
    //MARK: delete File
    func deleteFile(filename : String){
        let url = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record, filename: filename)
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: url!)
        } catch {
            print("\(error)")
        }
    }
}

extension AVURLAsset {
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)
        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
}
//        kAudioFormatMPEG4AAC
//        kAudioFormatAppleLossless
//        kAudioFormatAppleIMA4
//        kAudioFormatiLBC
//        kAudioFormatULaw
//        kAudioFormatLinearPCM
