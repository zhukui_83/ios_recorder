//
//  AudioRecorderManager.swift
//  ela
//
//  Created by Bastien Falcou on 4/14/16.
//  Copyright © 2016 Fueled. All rights reserved.
//

import Foundation
import AVFoundation

//let audioFileNameType = ".mp3"
//let encoderBitRate: Int = 320000
//let numberOfChannels: Int = 2
//let sampleRate: Double = 44100.0

//let Key_AudioRecorderManager_initSetting = "Key_AudioRecorderManager_initSetting"
let Key_AudioRecorderManager_audioRecType = "Key_AudioRecorderManager_audioRecType"
//let Key_AudioRecorderManager_audioFileNameType = "Key_AudioRecorderManager_audioFileNameType"
//let Key_AudioRecorderManager_numberOfChannels = "Key_AudioRecorderManager_numberOfChannels"
//let Key_AudioRecorderManager_encoderBitRate = "Key_AudioRecorderManager_encoderBitRate"
let Key_AudioRecorderManager_sampleRate = "Key_AudioRecorderManager_sampleRate"


// 录音的格式
enum AudioRecType: Int {
    case aac = 1
    case alac = 2
    case mp3 = 3
    case wav = 4
}

struct AudioRecordType {
    var FormatID:AudioFormatID
    var RecType:AudioRecType
    var RecTypeName:String
    var FileExtension:String
}
struct AudioRecordSampleRate {
    var SampleRate:Double
    var SampleRateStr:String
}
let AudioRecordSampleRate_Default = 44100.0


extension AudioRecorderManager {

    func getSampleRate() -> Double{
        if let value: Double = UserDefaults.standard.object(forKey: Key_AudioRecorderManager_sampleRate) as? Double {
            return value
        } else {
            setSampleRate(value: AudioRecordSampleRate_Default)
            return AudioRecordSampleRate_Default
        }
    }
    func setSampleRate(value : Double){
        UserDefaults.standard.set(value, forKey: Key_AudioRecorderManager_sampleRate)
    }
    func getCurrentAudioSampleRate() -> AudioRecordSampleRate{
        let rate = getSampleRate()
        return getAudioSampleRate(rate: rate)
    }
    func getAudioSampleRate(rate : Double)-> AudioRecordSampleRate{
        return AudioRecordSampleRate(SampleRate: rate, SampleRateStr:  String.init(format: "%.1f Hz", rate))
    }
    func getAudioSampleRateList() -> [AudioRecordSampleRate]{
        var list =  [AudioRecordSampleRate]()
        let currentrettype = getAudioRecType()
        if currentrettype == .aac{
            list.append(getAudioSampleRate(rate : 48000.0))
            list.append(getAudioSampleRate(rate : AudioRecordSampleRate_Default))
        }else{
            list.append(getAudioSampleRate(rate : 48000.0))
            list.append(getAudioSampleRate(rate : AudioRecordSampleRate_Default))
            list.append(getAudioSampleRate(rate : 32000.0))
            list.append(getAudioSampleRate(rate : 22050.0))
        }
//        list.append(getAudioSampleRate(rate : 11025.0))
        return list
    }
    func getAudioRecType() -> AudioRecType{
        if let value: Int = UserDefaults.standard.object(forKey: Key_AudioRecorderManager_audioRecType) as? Int {
            return AudioRecType.init(rawValue: value)!
        } else {
            setAudioRecType(value: .mp3)
            return AudioRecType.mp3
        }
    }
    func setAudioRecType(value : AudioRecType){
        UserDefaults.standard.set(value.rawValue, forKey: Key_AudioRecorderManager_audioRecType)
    }
    func getCurrentAudioRecordType() -> AudioRecordType{
        let rectype = getAudioRecType()
        return getAudioRecordType(value:  rectype)
    }
    func getAudioRecordType(value: AudioRecType) -> AudioRecordType{
        switch value {
        case .aac: return AudioRecordType(FormatID: kAudioFormatMPEG4AAC, RecType: .aac, RecTypeName: "AAC", FileExtension: ".m4a")
        case .alac: return AudioRecordType(FormatID: kAudioFormatAppleLossless, RecType: .alac, RecTypeName: "ALAC", FileExtension: ".m4a")
        case .mp3: return AudioRecordType(FormatID: kAudioFormatLinearPCM, RecType: .mp3, RecTypeName: "MP3", FileExtension: ".mp3")
        case .wav: return AudioRecordType(FormatID: kAudioFormatLinearPCM, RecType: .wav, RecTypeName: "WAVE", FileExtension: ".wav")
        }
    }
    func getAudioRecordTypeList() -> [AudioRecordType]{
        var list =  [AudioRecordType]()
        list.append(getAudioRecordType(value: .aac))
        list.append(getAudioRecordType(value: .alac))
        list.append(getAudioRecordType(value: .mp3))
        list.append(getAudioRecordType(value: .wav))
        return list
    }
//    func getAudioRecordTypeList() -> Array<AudioRecordType>{
//        return [AudioRecordType(FormatID: kAudioFormatMPEG4AAC, RecType: .aac, RecTypeName: "AAC", FileExtension: ".m4a"),]
//    }
}
