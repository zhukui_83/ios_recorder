//
//  BootStrapVC+AD.swift
//  voicememos
//
//  Created by ksymac on 2019/08/03.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import Foundation
import Firebase
import GoogleMobileAds

extension BootStrapViewController: GADBannerViewDelegate {
    func initAD(){
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = AdMobID_Banner_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.frame = CGRect.init(x: 0, y: 0,
                                       width: UIScreen.main.bounds.width,
                                       height: kGADAdSizeBanner.size.height)
        bannerView.load(GADRequest())
        //ADView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height:0)
        ADViewHeight.constant = 0
        ADView.backgroundColor = UIColor.clear
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        ADView.addSubview(bannerView)
        ADViewHeight.constant = kGADAdSizeBanner.size.height
        bannerView.centerXAnchor.constraint(equalTo: ADView.centerXAnchor).isActive = true
        bannerView.centerYAnchor.constraint(equalTo: ADView.centerYAnchor).isActive = true
    }
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
    }
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
    }
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
    }
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
    }
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
    }
}
