//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
import FirebaseCore
import FirebaseAnalytics
import AVFoundation

//import SimplePDF
//import PandoraPlayer
extension BootStrapViewController :UITableViewDataSource, UITableViewDelegate{
    // MARK: 获得最新数据
    func refreshByNewDBData() {
        if(nowSelectedStatus < -1){
            listTrackMain = dbMainTb.getAll()
        }else{
            listTrackMain = dbMainTb.getAllByStatus(self.nowSelectedStatus)
        }
        self.tableView.reloadData()
        //        resetFooterView()
    }
    func setupTabViewUI() {
        self.tableView.separatorStyle = .none
        tableView.separatorColor = UIColor.clear
        tableView.estimatedRowHeight = 10000
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.keyboardDismissMode = .interactive
        tableView.tableFooterView = UIView.init(frame: CGRect.init(width: 0, height: 100))
        
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.refreshByNewDBData()
        // MARK:加入广告
        self.ADView.frame.size = CGSize(width: self.ADView.frame.width, height: 0)

    }
}
extension BootStrapViewController {
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTrackMain.count
    }
    // 为表视图单元格提供数据，该方法是必须实现的方法
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let cell = MainCardCell.CreateCellWithNib(table: self.tableView, maindata: listTrackMain[row], celldelegate:  self)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MainCardCell.getHeight(maindata: listTrackMain[indexPath.row])
    }
    // 支持单元格编辑功能
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // Override to support editing the table view.
    // for ios 8.0
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle")
    }
    //　MARK: 各行的Action
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        print("tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath)")
        let DelAction = UITableViewRowAction(style: .default, title: NSLocalizedString("main_view_cell_delete", comment: "")) {
            (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
            tableView.isEditing = false
            self.delTable(indexPath)
        }
        let MoveAction = UITableViewRowAction(style: .default, title: NSLocalizedString("main_view_cell_move", comment: "")) {
            (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
            tableView.isEditing = false
            self.openMoveDialog(indexPath)
        }
        MoveAction.backgroundColor = UIColor.orange
        
        let Action_share = UITableViewRowAction(style: .default, title: NSLocalizedString("main_view_cell_share", comment: "")) {
            (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
            tableView.isEditing = false
            self.MainCardCellBtnClick1(self.listTrackMain[indexPath.row])
        }
        Action_share.backgroundColor = UIColor.flatYellow()
        let Action_rename = UITableViewRowAction(style: .default, title: NSLocalizedString("main_view_cell_rename", comment: "")) {
            (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
            tableView.isEditing = false
            self.MainCardCellBtnClick2(self.listTrackMain[indexPath.row])
        }
        Action_rename.backgroundColor = UIColor.green
        let Action_other = UITableViewRowAction(style: .default, title: NSLocalizedString("main_view_cell_other", comment: "")) {
            (action: UITableViewRowAction, indexPath: IndexPath) -> Void in
            tableView.isEditing = false
            
            self.MainCardCellBtnClick3(self.listTrackMain[indexPath.row])
        }
        Action_other.backgroundColor = UIColor.darkGray
        return [ Action_other, DelAction, Action_share, ]//shareAction EditAction,
    }

}



extension BootStrapViewController:SlideMenuDelegate,BTNavigationDropdownMenu_Delegate{
    //MARK : 显示下拉菜单，被按钮调用
    func showDropdownMenu() {
        self.showMenu(nil)
    }
    //MARK : 隐藏下拉菜单，被按钮调用
    func hideDropdownMenu() {
        self.closeMenu()
    }
    //MARK: - 选中菜单条目时调用
    func slideMenuItemSelectedAtIndex(_ index: Int) {
        self.closeMenu()
        if index >= 0 {
            self.nowSelectedStatus = self.statuslist[index].status
            self.menuView?.setMenuTitle1(self.statusitems[index])
            self.refreshByNewDBData()
        }
    }
    // MARK:  不添加左边的菜单
    func addSlideMenuButton(){
    }
    
    //MARK: - 切换Menu是否显示
    @objc func switchMenuShow(_ sender : UIButton){
        if self.menuVC.isShown {
            self.closeMenu()
        }else{
            self.showMenu(sender)
        }
    }
    //MARK: - 显示菜单时的详细处理
    @objc func showMenu(_ sender : UIButton?){
        //        overlayView.frame = self.view.frame
        self.overlayView.isHidden = false
        self.setupNaviView_Default()
        self.menuVC.refresh()
        
        self.menuVC.isShown = true
        self.menuView?.isShown = true
        
        if menuVC.view.superview != self.overlayView{
            menuVC.delegate = self
            self.overlayView.addSubview(menuVC.view)
        }
        //        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            self.menuView?.menuArrow.transform = (self.menuView?.menuArrow.transform.rotated(by: 180 * 3 * CGFloat(M_PI/360)))!
            sender?.isEnabled = true
        }, completion:nil)
    }
    //MARK: - 关闭菜单时的详细处理
    func closeMenu(){
        self.menuVC.isShown = false
        self.menuView?.isShown = false
        let viewMenuBack : UIView = self.menuVC.view
        UIView.animate(withDuration: 0.3, animations: {
            () -> Void in
            var frameMenu : CGRect = viewMenuBack.frame
            frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
            viewMenuBack.frame = frameMenu
            viewMenuBack.layoutIfNeeded()
            viewMenuBack.backgroundColor = UIColor.clear
            
            self.menuView?.menuArrow.transform = CGAffineTransform.identity//(self.menuView?.menuArrow.transform.rotated(by: 180 * CGFloat(M_PI/360)))!
        }, completion: { (finished) -> Void in
            viewMenuBack.removeFromSuperview()
            self.overlayView.isHidden = true
        })
    }
    //MARK: - 点击控制菜单的按钮的处理 ，旧的，现在并没有左右
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10){
            sender.tag = 0;
            closeMenu()
            return
        }
        sender.isEnabled = false
        sender.tag = 10
        menuVC.btnMenu = sender
        self.showMenu(sender)
    }
}

// MARK:- Cell的各个action
extension BootStrapViewController:MainCardCellDelegate{
    func MainCardCellBtnClick1(_ bean : TrackMain?){
        //        MyQRBaseAction.sharedInstance.displayShareSheet(vc:self, bean: bean)
        share(vc:self, bean: bean!)
    }
    func MainCardCellBtnClick2(_ bean : TrackMain?){
        MyQRBaseAction.sharedInstance.displayRenameSheet(vc: self
            , bean: bean, completion: {
                self.refreshByNewDBData()
        })
    }
    func MainCardCellBtnClick3(_ bean : TrackMain?){
        //        MyQRBaseAction.sharedInstance.displayOtherSheet(vc:self, bean: bean)
        displayOtherSheet(vc:self, bean: bean)
    }
    func share(vc:UIViewController,bean : TrackMain) {
        let vc =  ShareWebViewController.init()
        let nav = UINavigationController.init(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        vc.bean = bean
        self.present(nav, animated: true, completion: nil)
    }
    func displayOtherSheet(vc:UIViewController,bean : TrackMain?) {
        guard let index = listTrackMain.index(where:{ (temp) -> Bool in
            temp.filename == bean?.filename
        }) else{
            return
        }
        let indexpath = IndexPath.init(row: index, section: 0)
        let actionMenu = UIAlertController(title: nil, message: NSLocalizedString("common_menu", comment: ""), preferredStyle: .actionSheet)
        let delAction = UIAlertAction(title: NSLocalizedString("main_view_cell_delete", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.delTable(indexpath)
        })
        let playAction = UIAlertAction(title: NSLocalizedString("main_view_cell_play", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.moveToPlayerVC(bean!)
        })
        let moveAction = UIAlertAction(title: NSLocalizedString("main_view_cell_move", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.openMoveDialog(indexpath)
        })
        let renameAction = UIAlertAction(title: NSLocalizedString("main_view_cell_rename", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.MainCardCellBtnClick2(self.listTrackMain[indexpath.row])
        })
        let shareAction = UIAlertAction(title: NSLocalizedString("main_view_cell_share", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.MainCardCellBtnClick1(self.listTrackMain[indexpath.row])
        })
        let pdfAction = UIAlertAction(title: NSLocalizedString("pdf", comment: ""), style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.tableView.isEditing = false
            self.createPDF(indexpath)
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("common_cancel", comment: ""), style: .cancel, handler: nil)
        actionMenu.addAction(shareAction)
        actionMenu.addAction(delAction)
        actionMenu.addAction(playAction)
        actionMenu.addAction(moveAction)
        actionMenu.addAction(renameAction)
        actionMenu.addAction(cancelAction)
        actionMenu.modalPresentationStyle = .popover
        if let presentation = actionMenu.popoverPresentationController {
            presentation.barButtonItem = navigationItem.rightBarButtonItems?[0]
        }
        vc.present(actionMenu, animated: true, completion: nil)
    }
    //MARK: 削除 Cell
    func delTable(_ indexPath: IndexPath ){
        let rowIndex = indexPath.row
        //var rowDict : NSDictionary = listTrackNo.objectAtIndex(rowIndex) as! NSDictionary
        let ID : Int = listTrackMain[rowIndex].rowID
        let filename : String = listTrackMain[rowIndex].filename
        let status : Int = listTrackMain[rowIndex].status
        if(status == -1){
            dbMainTb.delete(ID)
            dbDetailTb.deleteByfileName(filename)
            VMFileClass.sharedInstance.deleteFile(filename: listTrackMain[rowIndex].filenamewithextension)
        }else{
            let bean = self.listTrackMain[rowIndex]
            bean.status = -1
            dbMainTb.update(bean)
        }
        listTrackMain.remove(at: rowIndex)
        tableView.deleteRows(at: [indexPath], with: .fade)
        setupNaviView_Default()
    }
    func openMoveDialog(_ indexPath: IndexPath ){
        let rowIndex = indexPath.row
        let status : Int = listTrackMain[rowIndex].status
        let shareMenu = UIAlertController(title: nil,
                                          message: NSLocalizedString("main_view_cell_move_detail", comment: ""),
                                          preferredStyle: .actionSheet)
        for i in 0 ..< statuslist.count {
            let bean  = statuslist[i]
            if(statuslist[i].status != ComFunc.TrackList_all){
                if(bean.status != status){
                    let twitterAction = UIAlertAction(title: statusitems[i], style: .default, handler: {
                        (action:UIAlertAction) -> Void in
                        self.moveToStatus(indexPath,status: bean.status)
                    })
                    shareMenu.addAction(twitterAction)
                }
            }
        }
        let cancelAction = UIAlertAction(title:
            NSLocalizedString("common_cancel", comment: "cancel"),
                                         style: .cancel, handler: nil)
        shareMenu.addAction(cancelAction)
        shareMenu.popoverPresentationController?.sourceView = self.view;
        if let cell = tableView.cellForRow(at: indexPath){
            shareMenu.popoverPresentationController?.sourceRect = CGRect.init(x: 0,
                                                                              y: cell.frame.origin.y,
                                                                              width: cell.frame.width/2,
                                                                              height: cell.frame.height);
        }
        self.present(shareMenu, animated: true, completion: nil)
    }
    func moveToStatus(_ indexPath: IndexPath, status : Int){
        let rowIndex = indexPath.row
        let bean = self.listTrackMain[rowIndex]
        bean.status = status
        dbMainTb.update(bean)
        self.refreshByNewDBData()
        //listTrackMain.removeAtIndex(rowIndex)
        //tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        setupNaviView_Default()
    }
    // MARK: 点击并显示详细画面
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.moveToPlayerVC(self.listTrackMain[indexPath.row])
    }
    func moveToPlayerVC(_ datamain:TrackMain){
        let url = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Record,
                                                                    filename: datamain.filenamewithextension)
        print("open url : " + (url?.absoluteString)!)
        let item:AVPlayerItem = AVPlayerItem.init(url: url!)
        let playerVC = MyPandoraPlayer.configure(withAVItems: [item])
        playerVC.mainbean = datamain
        playerVC.title = datamain.filenamewithextension
        playerVC.modalPresentationStyle = .fullScreen
        playerVC.path = url
        self.present(playerVC, animated: true, completion: nil)
    }
    
    func createPDF(_ indexPath: IndexPath){
        //let bean = self.listTrackMain[indexPath.row]
        //let detaillist =  dbDetailTb.getAllByfileName(bean.filename)
        //        let A4paperSize = CGSize(width: 595, height: 842)
        //        let pdf = SimplePDF(pageSize: A4paperSize, pageMargin: 20.0)
        //
        //        pdf.addLineSpace(20.0)
        //        for detail in detaillist{
        //            if detail.type == 0{
        //                pdf.setContentAlignment(.left)
        //                pdf.addText(detail.detail)
        //                pdf.addLineSpace(20.0)
        //            }else{
        //                var dir_url = MyClassFile.sharedInstance.getDirectory(type: .Image)
        //                    dir_url.appendPathComponent(detail.imgname)
        //                if let image = UIImage(contentsOfFile: dir_url.path){
        //                    pdf.setContentAlignment(.center)
        //                    pdf.addImage( image )
        //                    pdf.beginNewPage()
        //                    pdf.addLineSpace(20.0)
        //                }
        //            }
        //        }
        //        let pdfData = pdf.generatePDFdata()
        //        // write to file
        //        var path = MyClassFile.sharedInstance.getDirectory(type: .RecordPdf)
        //        path.appendPathComponent(bean.filename+".pdf")
        //        do{
        //            try pdfData.write(to: path)
        //            print("\nThe generated pdf can be found at:")
        //            print("\n\t\(path)\n")
        //        }catch{
        //            print(error)
        //        }
    }
}
