//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
import AVFoundation
import MobileCoreServices
//import PandoraPlayer

extension BootStrapViewController {
    //Rename
    @IBAction func SettingsButtonTapped(_ sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "HistoryMain", bundle: nil)
        let vc: MyTabBarController = storyboard.instantiateViewController(withIdentifier: "MyTabBarController") as! MyTabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
