//
//  BootStrapViewController.swift
//  voicememos
//
//  Created by ksymac on 2019/07/27.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BootStrapViewController: BaseViewController {

    let dbMainTb = DBMainTable.shared//model
    let dbStatusTb = DBStatusTable.shared//model
    let dbDetailTb = DBDetailTable.shared//model
    
    @IBOutlet weak var TabView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ADView: UIView!
    @IBOutlet weak var ADViewHeight: NSLayoutConstraint!
    var bannerView: GADBannerView!
    
    @IBOutlet weak var ButtomView: UIView!
    @IBOutlet weak var CenterRecordButton: UIButton!
//    @IBOutlet weak var CenterRecordButtonTop: NSLayoutConstraint!
    @IBOutlet weak var BottomViewHeight: NSLayoutConstraint!
    var bottomLine:UIView?

    // MARK: TableView
    var listTrackMain : Array<TrackMain> = []
    var statuslist : Array<TrackStatus> = []
    var statusitems : Array<String> = [NSLocalizedString("main_view_box_no_cat", comment: ""),
                                       NSLocalizedString("main_view_box_all", comment: ""),
                                       NSLocalizedString("main_view_box_trash", comment: ""),]
    
    var nowSelectedStatus : Int = 0
    var menuVC : MenuViewController =  MenuViewController.instance()
    var menuView : BTNavigationDropdownMenu?
    var overlayView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 200))
    
    
    let viewModel = RecorderManager.shared.viewModel
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        OnboardingManager.shared.showOnboarding(basevc: self)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshByNewDBData()
        self.viewModel.askAudioRecordingPermission()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let img1 = UIImage.init(named: "Record-center")
        CenterRecordButton.setImage(img1?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        CenterRecordButton.tintColor = UIColor.white
        
        self.initUIColor()
        self.setupTabViewUI()
        
        //Overlay View
        overlayView.backgroundColor = UIColor.clear
        overlayView.isHidden = true
        self.navigationController?.view.addSubview(overlayView)
        
        self.setupNaviView_Default();
        
        self.initAD()
    }
    

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.overlayView.frame = self.view.frame
    }
    
    func initUIColor(){
        self.view.backgroundColor = UIColor.groupTableViewBackground
        self.view.backgroundColor = GlobalBackgroundColor
        self.TabView.backgroundColor = GlobalBackgroundColor
        self.tableView.backgroundColor = GlobalBackgroundColor
    }
    
    // MARK: 重新绘制菜单 也需要重新绘制无数据视图
    func setupNaviView_Default(){
        statuslist = dbStatusTb.getAllList()
        statusitems = dbStatusTb.getAllItems()
        var iselect = 0
        for i in 0 ..< statuslist.count {
            let bean  = statuslist[i]
            if(bean.status == self.nowSelectedStatus){
                iselect = i
            }
        }
        if(iselect == 0){
            self.nowSelectedStatus = ComFunc.TrackList_doing
        }
        menuVC.items = statusitems as [AnyObject]
        menuVC.iSelected = iselect
        
        if menuView == nil{
            menuView = BTNavigationDropdownMenu(frame: CGRect(x: 0.0, y: 0.0, width: 300, height: 44),
                                                title: statusitems[iselect],
                                                items: statusitems as [AnyObject],
                                                containerView: self.view)
            //menuView.title = statusitems[iselect]
            menuView!.cellHeight = 50
            menuView!.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
            menuView!.cellSelectionColor = UIColor(red: 0.0/255.0, green:180.0/255.0, blue:195.0/255.0, alpha: 1.0)
            menuView!.cellTextLabelColor = UIColor.white
            menuView!.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 18)
            menuView!.arrowPadding = 15
            menuView!.animationDuration = 0.5
            menuView!.maskBackgroundColor = UIColor.black
            menuView!.maskBackgroundOpacity = 0.3
            menuView!.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
                self.nowSelectedStatus = self.statuslist[indexPath].status
                self.refreshByNewDBData()
            }
            menuView?.delegate = self
        }
        //重置显示的标题
        menuView?.setMenuTitle1(statusitems[iselect])
        //resetFooterView()
        let settingItem : UIBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icons8-settings-50"),
            style: UIBarButtonItemStyle.plain,
            target: self, action: "SettingsButtonTapped:")
        let listItem : UIBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icons8-menu-filled-50"),
            style: UIBarButtonItemStyle.plain,target: self,
            action: #selector(self.switchMenuShow(_:))
        )
        self.navigationItem.titleView = menuView
        self.navigationItem.leftBarButtonItems = [listItem,]
        self.navigationItem.rightBarButtonItems = [settingItem,]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController?.navigationBar.barTintColor  = GlobalHeadColor
        
        self.CenterRecordButton.isHidden = false
        
        setupBtnUI()
    }
    func setupBtnUI(){
        if isiphonex {
            self.BottomViewHeight.constant = 92
        }else{
            self.BottomViewHeight.constant = 80
        }
        
        CenterRecordButton.layer.cornerRadius = 30
        CenterRecordButton.layer.borderWidth = 0
        CenterRecordButton.layer.borderColor = GlobalHeadColor.cgColor
        CenterRecordButton.layer.shadowRadius = 5
        CenterRecordButton.layer.shadowOpacity = 0.5
        CenterRecordButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        ButtomView.layer.shadowRadius = 5
        ButtomView.layer.shadowOpacity = 0.4
        ButtomView.layer.shadowOffset = CGSize(width: -10, height: -10)
        ButtomView.layer.cornerRadius = 0
    }
    // MARK: - MyActions
    @IBAction func MyRecordButtonStart(_ sender: AnyObject) {
        let recorderVC = RecorderVC.instance()
        let navc: MyUINavigationController = MyUINavigationController(rootViewController: recorderVC)
        navc.modalPresentationStyle = .fullScreen
        self.present(navc, animated: true) {
        }
    }
    var isiphonex: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
}
