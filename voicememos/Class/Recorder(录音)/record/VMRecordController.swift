//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
//import PandoraPlayer

class VMRecordViewController: BaseViewController {

    let dbMainTb = DBMainTable.shared//model
    let dbStatusTb = DBStatusTable.shared//model
    let dbDetailTb = DBDetailTable.shared//model
    
    var current_modify_bean:TrackMain?
    
    @IBOutlet weak var TabView: UIView!
    @IBOutlet weak var ColView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ADView: UIView!
    
    @IBOutlet weak var collectionView: ILDirectMessagesCollectionView!
    
    
    @IBOutlet weak var RecNameLabel: UILabel!
//    @IBOutlet weak var InputMenuView: UIView!
//    @IBOutlet weak var InputMenuViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ButtomView: UIView!
    
    @IBOutlet weak var ListButton: UIButton!
    @IBOutlet weak var StopButton: UIButton!
//    @IBOutlet weak var recordButton: UIButton?
    @IBOutlet weak var MyRecordButton: UIButton!
    @IBOutlet weak var CenterRecordButton: UIButton!
    @IBOutlet weak var CenterRecordButtonTop: NSLayoutConstraint!
    
//    @IBOutlet var SettingsButton: UIButton!
    @IBOutlet var audioVisualizationView: AudioVisualizationView!
    
    @IBOutlet var optionsView: UIView!
    @IBOutlet var optionsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var audioVisualizationTimeIntervalLabel: UILabel!
    @IBOutlet var meteringLevelBarWidthLabel: UILabel!
    @IBOutlet var meteringLevelSpaceInterBarLabel: UILabel!
    @IBOutlet var mytimeLabel: UILabel!
    @IBOutlet weak var RecFormatLabel: UILabel!
    
    
    //MARK: input view
    @IBOutlet weak var inputContainerView: UIView!
    var inputToolBarContainerView: MyTagKeyBoardView!
//    var inputToolBarContainerView1: MyTagKeyBoardView!
    @IBOutlet weak var inputContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var TitleEditView: UIView!
    @IBOutlet weak var TitleEditViewHeight: NSLayoutConstraint!
    @IBOutlet weak var BottomViewHeight: NSLayoutConstraint!
    let BottomViewHeight_Fix:CGFloat = 110
    
    
    let viewModel = ViewModel()
    var chronometer: Chronometer?
    //var recStartTime : Date?
    //var bottomView: ChatRoomInputView! //画面下部に表示するテキストフィールドと送信ボタン
    var chats: [ChatEntity] = []
    
    var messages: [ZukMessageModel] = []
    var sysmessages: [ZukMessageModel] = []
    
//    var messageNo = -99
    var messageEdit_CurrentIndex = -99
    
    // MARK: TableView
    var listTrackMain : Array<TrackMain> = []
    var statuslist : Array<TrackStatus> = []
    var statusitems : Array<String> = [NSLocalizedString("main_view_box_no_cat", comment: ""),
                                       NSLocalizedString("main_view_box_all", comment: ""),
                                       NSLocalizedString("main_view_box_trash", comment: ""),]
    
    var nowSelectedStatus : Int = 0
    var menuVC : MenuViewController =  MenuViewController.instance()
    var menuView : BTNavigationDropdownMenu?
    var overlayView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 200))
    
	enum AudioRecodingState {
		case ready
		case recording
		case recorded
        //case playing
        case paused
        var buttonImage: UIImage {
            switch self {
            //
            case .ready:
                return UIImage(named: "Record-Button")!
            case .recording:
                return UIImage(named: "Pause-Button")!
            case .paused:
                return UIImage(named: "Record-Button")!
            case .recorded:
                return UIImage(named: "Record-Button")!
            //case .playing:
            //      return UIImage(named: "Pause-Button")!
            }
        }
        
		var audioVisualizationMode: AudioVisualizationView.AudioVisualizationMode {
			switch self {
			case .ready, .recording:
				return .write
			case .paused,  .recorded://.playing,
				return .read
			}
		}
	}
	
    
    //根据状态设置图片等
	var currentState: AudioRecodingState = .ready {
		didSet {
            //self.recordButton?.setImage(self.currentState.buttonImage, for: UIControlState())
            self.MyRecordButton.setImage(self.currentState.buttonImage, for: UIControlState())
			self.audioVisualizationView.audioVisualizationMode = self.currentState.audioVisualizationMode
            //self.clearButton.isHidden = self.currentState == .ready || self.currentState == .recording //|| self.currentState == .playing
		}
	}
	
    class func instance() -> VMRecordViewController {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "VMRecordViewController") as! VMRecordViewController
    }
	
	override func viewDidLoad() {
		super.viewDidLoad()
        self.viewModel.askAudioRecordingPermission()
		self.viewModel.audioMeteringLevelUpdate = { [weak self] meteringLevel in
			guard let this = self, this.audioVisualizationView.audioVisualizationMode == .write else {
				return
			}
			this.audioVisualizationView.addMeteringLevel(meteringLevel)
		}
		
		self.viewModel.audioDidFinish = {
            [weak self] in
			self?.currentState = .recorded
			self?.audioVisualizationView.stop()
		}

        self.initUIColor()
        self.initMessages()
        //table View
        self.setupTabViewUI()
        self.setupCollectionViewUI()
        self.setupInputView()

        TitleEditView.isHidden = true
        //Overlay View
        overlayView.backgroundColor = UIColor.clear
        overlayView.isHidden = true
        self.navigationController?.view.addSubview(overlayView)
        
        self.setupNaviView_Default();
        self.setupUIWithState()
        self.resetViewTypeWithState()
	}
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: 重新绘制菜单 也需要重新绘制无数据视图
    func setupNaviView_Default(){
        statuslist = dbStatusTb.getAllList()
        statusitems = dbStatusTb.getAllItems()
        var iselect = 0
        for i in 0 ..< statuslist.count {
            let bean  = statuslist[i]
            if(bean.status == self.nowSelectedStatus){
                iselect = i
            }
        }
        if(iselect == 0){
            self.nowSelectedStatus = ComFunc.TrackList_doing
        }
        menuVC.items = statusitems as [AnyObject]
        menuVC.iSelected = iselect
        
        if menuView == nil{
            menuView = BTNavigationDropdownMenu(frame: CGRect(x: 0.0, y: 0.0, width: 300, height: 44),
                                                title: statusitems[iselect],
                                                items: statusitems as [AnyObject],
                                                containerView: self.view)
            //menuView.title = statusitems[iselect]
            menuView!.cellHeight = 50
            menuView!.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
            menuView!.cellSelectionColor = UIColor(red: 0.0/255.0, green:180.0/255.0, blue:195.0/255.0, alpha: 1.0)
            menuView!.cellTextLabelColor = UIColor.white
            menuView!.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 18)
            menuView!.arrowPadding = 15
            menuView!.animationDuration = 0.5
            menuView!.maskBackgroundColor = UIColor.black
            menuView!.maskBackgroundOpacity = 0.3
            menuView!.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
                self.nowSelectedStatus = self.statuslist[indexPath].status
                self.refreshByNewDBData()
            }
            menuView?.delegate = self
            //self.navigationItem.titleView = menuView
        }
        //重置显示的标题
        menuView?.setMenuTitle1(statusitems[iselect])
        //resetFooterView()
        let settingItem : UIBarButtonItem = UIBarButtonItem(
                image: UIImage(named: "icons8-settings-50"),
                style: UIBarButtonItemStyle.plain,
                target: self, action: "SettingsButtonTapped:")
        let listItem : UIBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icons8-menu-filled-50"),
            style: UIBarButtonItemStyle.plain,target: self,
            action: #selector(self.switchMenuShow(_:))
        )
        self.navigationItem.titleView = menuView
        self.navigationItem.leftBarButtonItems = [listItem,]
        self.navigationItem.rightBarButtonItems = [settingItem,]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController?.navigationBar.barTintColor  = GlobalHeadColor
        
//        self.ButtomView.backgroundColor = GlobalHeadColor
        self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor
        self.view.backgroundColor = UIColor.groupTableViewBackground
        setButtonColor()
        
        self.MyRecordButton.isHidden = true
        self.CenterRecordButton.isHidden = false
        self.StopButton.isHidden = true
        
        setupBtnUI()
    }
    func setupBtnUI(){
        CenterRecordButton.layer.cornerRadius = 32
        CenterRecordButton.layer.borderWidth = 0
        CenterRecordButton.layer.borderColor = GlobalHeadColor.cgColor
        if isiphonex {
            CenterRecordButtonTop.constant = 13
        }else{
            CenterRecordButtonTop.constant = 20
        }

        CenterRecordButton.layer.shadowRadius = 5
        CenterRecordButton.layer.shadowOpacity = 0.4
        CenterRecordButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        MyRecordButton.layer.shadowRadius = 5
        MyRecordButton.layer.shadowOpacity = 0.3
        MyRecordButton.layer.shadowOffset = CGSize(width: 2, height:2)
        
        StopButton.layer.shadowRadius = 5
        StopButton.layer.shadowOpacity = 0.3
        StopButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        
        ButtomView.layer.shadowRadius = 5
        ButtomView.layer.shadowOpacity = 0.3
        ButtomView.layer.shadowOffset = CGSize(width: -10, height: -10)
        ButtomView.layer.cornerRadius = 0
    }
    
    var isiphonex: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    func setupNaviView_RecodeStart(_ title: String?){
        self.navigationItem.titleView = nil
        self.navigationItem.title = title
        let editItem : UIBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icons8-edit"),
            style: UIBarButtonItemStyle.plain,
            target: self, action: #selector(self.ShowRenameDialog(_:)))
        
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.rightBarButtonItems = [editItem,]

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
//        self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor.withAlphaComponent(0.8)
        self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor.withAlphaComponent(1.0)
//        self.ButtomView.backgroundColor = UIColor.clear
        self.view.backgroundColor =  MyAppUI.shared.recorder_backgroud_color
        
        self.RecNameLabel.text = self.viewModel.currentAudioRecord?.recordNewFileName
        self.RecNameLabel.textColor = GlobalHeadColor
        self.RecNameLabel.isHidden = true
        self.RecFormatLabel.textColor = GlobalHeadColor
        self.mytimeLabel.textColor = GlobalHeadColor
        
        self.MyRecordButton.isHidden = false
        self.CenterRecordButton.isHidden = true
        self.StopButton.isHidden = false
    }
//    func setupNaviView_RecodeStop(){
//        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
//        self.navigationController?.navigationBar.barTintColor  = GlobalHeadColor
//    }
    
    func setButtonColor(){
//        let origImage = UIImage(named: "Record-Button")
//        let tintedImage = origImage?.withRenderingMode(.alwaysOriginal)
//        self.MyRecordButton?.setImage(tintedImage, for: .normal)
//        MyRecordButton?.tintColor = GlobalHeadColor
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.overlayView.frame = self.view.frame
    }
    
    
	// MARK: - Actions
	@IBAction func recordButtonDidTouchDown(_ sender: AnyObject) {
        RecorderStart()
	}
    
	@IBAction func recordButtonDidTouchUpInside(_ sender: AnyObject) {
        RecorderStop()
	}
    
    // MARK: - MyActions
    @IBAction func MyRecordButtonStart(_ sender: AnyObject) {
        switch self.currentState  {
        case .ready:
            weak var weak_self = self
            MyAppPermission.shared.askMicroPhonePermission { (b) in
                if b == true{
                    weak_self?.RecorderStart()
                    weak_self?.setupNaviView_RecodeStart(weak_self?.RecNameLabel.text!)
                }else{
                    weak_self?.showDialogForMicrophone()
                }
            }
        case .recording:
            RecorderPause()
        case .paused:
            RecorderRestart()
        default:
            return
        }
    }
    
    func showDialogForMicrophone(){
        let alert = UIAlertController(title: "Unable to access Microphone, please check system settings.", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
            MyAppPermission.shared.requetSettingForAuth()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { [unowned self] _ in
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func MyRecordButtonStop(_ sender: AnyObject) {
        RecorderStop()
        setupNaviView_Default()
    }

    func offsetscrollview(){
    }
    
	@IBAction func clearButtonTapped(_ sender: AnyObject) {
        ResetAfterRecord()
	}
	
	@IBAction func switchValueChanged(_ sender: AnyObject) {
		let theSwitch = sender as! UISwitch
		if theSwitch.isOn {
			self.view.backgroundColor = UIColor.mainBackgroundPurple
            self.audioVisualizationView.gradientStartColor = UIColor.audioVisualizationPurpleGradientStart
			self.audioVisualizationView.gradientEndColor = UIColor.audioVisualizationPurpleGradientEnd
		} else {
			self.view.backgroundColor = UIColor.mainBackgroundGray
			self.audioVisualizationView.gradientStartColor = UIColor.audioVisualizationGrayGradientStart
			self.audioVisualizationView.gradientEndColor = UIColor.audioVisualizationGrayGradientEnd
		}
	}
	
	@IBAction func audioVisualizationTimeIntervalSliderValueDidChange(_ sender: AnyObject) {
		let audioVisualizationTimeIntervalSlider = sender as! UISlider
		self.viewModel.audioVisualizationTimeInterval = TimeInterval(audioVisualizationTimeIntervalSlider.value)
		self.audioVisualizationTimeIntervalLabel.text = String(format: "%.2f", self.viewModel.audioVisualizationTimeInterval)
	}

	@IBAction func meteringLevelBarWidthSliderValueChanged(_ sender: AnyObject) {
		let meteringLevelBarWidthSlider = sender as! UISlider
		self.audioVisualizationView.meteringLevelBarWidth = CGFloat(meteringLevelBarWidthSlider.value)
		self.meteringLevelBarWidthLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
	}
	
	@IBAction func meteringLevelSpaceInterBarSliderValueChanged(_ sender: AnyObject) {
		let meteringLevelSpaceInterBarSlider = sender as! UISlider
		self.audioVisualizationView.meteringLevelBarInterItem = CGFloat(meteringLevelSpaceInterBarSlider.value)
		self.meteringLevelSpaceInterBarLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
	}
	
	@IBAction func optionsButtonTapped(_ sender: AnyObject) {
		let shouldExpand = self.optionsViewHeightConstraint.constant == 0
		self.optionsViewHeightConstraint.constant = shouldExpand ? 165.0 : 0.0
		UIView.animate(withDuration: 0.2) {
			self.optionsView.subviews.forEach { $0.alpha = shouldExpand ? 1.0 : 0.0 }
			self.view.layoutIfNeeded()
		}
	}
    
    @IBAction func openListVC(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "HistoryMain", bundle: nil)
        let vc: MyTabBarController = storyboard.instantiateViewController(withIdentifier: "MyTabBarController") as! MyTabBarController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func openFileListVC(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "WebServerMain", bundle: nil)
        let vc: UITabBarController = storyboard.instantiateViewController(withIdentifier: "RootController") as! UITabBarController
        self.present(vc, animated: true, completion: nil)
    }
}
//MARK: SetUp Data and UI With Record Start
extension VMRecordViewController{
    func setupStartRecordUI(){
        self.initMessages()
//        let start_str = String(format: "%@ \n %@  " , (self.viewModel.currentAudioRecord?.recordFileName)!, "Recorder starting...")
        let start_str = String(format: "%@ " , NSLocalizedString("rec_view_rec_starting", comment: ""))
        self.addSysTag(str: start_str)
//        self.addSysTag(str: "title: nothing")
//        self.addSysTag(str: "comment: nothing")
    }
    func initMessages(){
        self.messages = []
//        self.messageNo = 0
        self.messageEdit_CurrentIndex = -1
    }
}



