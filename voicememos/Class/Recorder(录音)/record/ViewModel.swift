//
//  ViewModel.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/6/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation

struct SoundRecord {
	var meteringLevels: [Float]?
    // 记录文件信息
    var recordFileName: String?//初始文件名称
    var recordFileURL: URL?//文件路径
    var recordNewFileName: String?//保存文件名称
    var recordType: AudioRecordType?//保存文件类型
    var recordSampleRate: AudioRecordSampleRate?
    
    var recordTitle: String = ""//文件Title
    var recordDesc: String = ""//文件详细
    
    //    var audioFilePathLocal: URL?
    //    var recordFileBak: String?//
    var recordStartTime : Date?
}

final class ViewModel {
	var audioVisualizationTimeInterval: TimeInterval = 0.1 // Time interval between each metering bar representation
    
	//文件记录
	var currentAudioRecord: SoundRecord?
	private var isPlaying = false
	
	var audioMeteringLevelUpdate: ((Float) -> ())?
	var audioDidFinish: (() -> ())?
    
	init() {
		// notifications update metering levels
		NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didReceiveMeteringLevelUpdate),
			name: .audioPlayerManagerMeteringLevelDidUpdateNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didReceiveMeteringLevelUpdate),
			name: .audioRecorderManagerMeteringLevelDidUpdateNotification, object: nil)
		
		// notifications audio finished
		NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didFinishRecordOrPlayAudio),
			name: .audioPlayerManagerMeteringLevelDidFinishNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didFinishRecordOrPlayAudio),
			name: .audioRecorderManagerMeteringLevelDidFinishNotification, object: nil)
	}
	
	// MARK: - Recording
	
	func askAudioRecordingPermission(completion: ((Bool) -> Void)? = nil) {
		return AudioRecorderManager.shared.askPermission(completion: completion)
	}
	
	func startRecording(completion: @escaping (SoundRecord?, Error?) -> Void) {
		AudioRecorderManager.shared.startRecording(with: self.audioVisualizationTimeInterval, completion: { [weak self] url, error in
            guard let url = url else {
				completion(nil, error!)
				return
			}

            self?.currentAudioRecord = SoundRecord()
//            self?.currentAudioRecord?.audioFilePathLocal = url
            self?.currentAudioRecord?.meteringLevels = []
            self?.currentAudioRecord?.recordFileName = url.lastPathComponent
            self?.currentAudioRecord?.recordNewFileName = url.lastPathComponent
            self?.currentAudioRecord?.recordFileURL = url
            self?.currentAudioRecord?.recordType = AudioRecorderManager.shared.getCurrentAudioRecordType()
            self?.currentAudioRecord?.recordSampleRate = AudioRecorderManager.shared.getCurrentAudioSampleRate()
            
			print("sound record created at url \(url.absoluteString))")
			completion(self?.currentAudioRecord, nil)
		})
	}
	
	func stopRecording() throws {
		try AudioRecorderManager.shared.stopRecording()
	}
	
	func resetRecording() throws {
		try AudioRecorderManager.shared.reset()
		self.isPlaying = false
		self.currentAudioRecord = nil
	}
    
    
    func pauseRecording() throws {
        try AudioRecorderManager.shared.pauseRecording()
    }
    func restartRecording() throws {
        try AudioRecorderManager.shared.restartRecording()
    }
    
	// MARK: - Playing
	func startPlaying() throws -> TimeInterval {
		guard let currentAudioRecord = self.currentAudioRecord else {
			throw AudioErrorType.audioFileWrongPath
		}
		
		if self.isPlaying {
			return try AudioPlayerManager.shared.resume()
		} else {
            //audioFilePathLocal
			guard let audioFilePath = currentAudioRecord.recordFileURL else {
				fatalError("tried to unwrap audio file path that is nil")
			}
            
			self.isPlaying = true
			return try AudioPlayerManager.shared.play(at: audioFilePath, with: self.audioVisualizationTimeInterval)
		}
	}
	
	func pausePlaying() throws {
		try AudioPlayerManager.shared.pause()
	}
	
	// MARK: - Notifications Handling
	@objc private func didReceiveMeteringLevelUpdate(_ notification: Notification) {
		let percentage = notification.userInfo![audioPercentageUserInfoKey] as! Float
		self.audioMeteringLevelUpdate?(percentage)
	}
	
	@objc private func didFinishRecordOrPlayAudio(_ notification: Notification) {
		self.audioDidFinish?()
	}
}
