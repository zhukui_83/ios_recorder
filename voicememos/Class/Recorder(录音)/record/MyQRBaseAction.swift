//
//  MyQRBaseAction.swift
//  qrbox
//
//  Created by ksymac on 2018/1/20.
//  Copyright © 2018年 ZHUKUI. All rights reserved.
//

import UIKit

let _myQRBaseAction = MyQRBaseAction()
class MyQRBaseAction: NSObject {
    class var sharedInstance : MyQRBaseAction {
        return _myQRBaseAction
    }
//    let appDelegate:AppDelegate = UIApplication.shared.delegate! as! AppDelegate
    
    // MARK:  シェア
    func displayShareSheet(vc:UIViewController, bean : TrackMain?) {
        self.shareBean(vc: vc, bean: bean)
    }
    
    //MARK:  share bean
    func shareBean(vc:UIViewController, bean : TrackMain?) {
        let shareMenu = UIAlertController(title: nil, message: "メニュー", preferredStyle: .actionSheet)
        let safariAction = UIAlertAction(title: "Safariで開く", style: .default, handler: {
            (action:UIAlertAction) -> Void in
            let str = "\((bean?.filename)!)"
            let urlString = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            vc.dismiss(animated: false, completion: nil)
            if let okurl = URL(string: urlString!){
                if UIApplication.shared.canOpenURL(okurl) {
                    //                    UIApplication.shared.open(okurl, options: [:], completionHandler: nil)
                    //If you want handle the completion block than
                    UIApplication.shared.open(okurl, options: [:], completionHandler: { (success) in
                        print("Open url : \(success)")
                    })
                }
            }else{
            }
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel, handler: nil)
        ////        shareMenu.addAction(safariAction)
        //        shareMenu.addAction(copyContentAction)
        //        shareMenu.addAction(shareContentAction)
        //        shareMenu.addAction(shareImgAction)
        shareMenu.addAction(cancelAction)
        shareMenu.modalPresentationStyle = .popover
        if let presentation = shareMenu.popoverPresentationController {
            presentation.barButtonItem = vc.navigationItem.rightBarButtonItems?[0]
        }
        vc.present(shareMenu, animated: true, completion: nil)
    }
    
    var bean:TrackMain?
    var rename_completion: (() -> ())?
    //MARK:- Rename
    func displayRenameSheet(vc:UIViewController, bean : TrackMain? , completion: @escaping () -> ()) {
        self.bean = bean
        self.rename_completion = completion
        popupRenameDialog(vc: vc, bean: bean)
    }
}
extension MyQRBaseAction:TitleEditViewControllerDelegate {
    func renameRecFileName(filename: String, title: String, desc: String) {
        if filename.removeWhitespace().utf8.count > 0{
            RenameFileAndUpdatDB(bean: self.bean,
                                 new_filename: filename,
                                 new_title: title,
                                 new_comment: desc)
            if let completion = rename_completion{
                completion()
            }
        }
    }
    func cancelClicked() {}
    func popupRenameDialog(vc:UIViewController, bean : TrackMain?){
        var filename = bean?.filename
        var title = bean?.title
        var desc = bean?.comment
        let alertView = TitleEditViewController(
            filename: filename!,
            title: title!,
            desc: desc!)
        alertView.deleage = self
        vc.present(alertView, animated: true, completion: nil)
    }
    
    // MARK: Rename RECFILE-----
    func RenameFileAndUpdatDB(bean : TrackMain?,
                              new_filename:String,
                              new_title: String,
                              new_comment:String){
        do {
            if var filename = bean?.filename,
                var fileurl =  bean?.fileurl,
                let fileExtension = bean?.recexten {
                //Rename file
                var resourceValues = URLResourceValues()
                resourceValues.name = String.init(format: "%@%@", new_filename, (bean?.recexten)!)
                try fileurl.setResourceValues(resourceValues)
                // File Rename
                if let bean = DBMainTable.shared.getAllByfilename(filename){
                    bean.filename = new_filename
                    bean.title = new_title
                    bean.comment = new_comment
                    DBMainTable.shared.updateByID(bean)
                }
                // detail DB
                DBDetailTable.shared.updateFileName(new_filename, filename: filename)
            }
        } catch {
            print("failed to set resource value")
        }
    }
}
