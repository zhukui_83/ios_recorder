//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave

extension VMRecordViewController {
    //MARK:- Recorder Start
    func RecorderStart() {
        if self.currentState == .ready {
            self.viewModel.startRecording { [weak self] soundRecord, error in
                if let error = error {
                    self?.showAlert(with: error)
                    return
                }
                self?.viewModel.currentAudioRecord?.recordStartTime = Date()
                self?.currentState = .recording
                self?.chronometer = Chronometer.init(withTimeInterval: 0.1)
                self?.chronometer?.start(shouldFire: true)
                
                if self?.viewModel.currentAudioRecord?.recordType?.RecType == .mp3{
                    let man = TFAudioFileManager.sharedInstance()
                    let path = self?.viewModel.currentAudioRecord?.recordFileURL?.path
                    let mp3path = String.init(format: "%@%@", path!, (self?.viewModel.currentAudioRecord?.recordType?.FileExtension)!)
                    let smapleRate = self?.viewModel.currentAudioRecord?.recordSampleRate?.SampleRate
                    man?.conventToMp3(withCafFilePath: path, mp3FilePath: mp3path, sampleRate: Int32(smapleRate!), callback: nil)
                }
                
                //Timer.scheduledTimer(timeInterval: audioVisualizationTimeInterval, target: self,
                //selector: #selector(AudioRecorderManager.timerDidUpdateMeter), userInfo: nil, repeats: true)
                self?.chronometer?.timerDidUpdate = {
                    [weak self] timerDuration in
                    //print(self?.chronometer)
                    print(timerDuration)
                    self?.mytimeLabel.text = MyDateUtils.getStrTimeInterval(timeInterval: timerDuration)
                    //String(format: "%0.1f", timerDuration)
                    //if self?.currentState == .playing{
                    //self?.offsetscrollview()
                    //}
                }
                self?.RecNameLabel.text = soundRecord?.recordFileName
                self?.WriteToDB_StartInfo()
                self?.setupStartRecordUI()
                self?.setupUIWithState()
                self?.resetViewTypeWithState()
                print(String.init(format: "record start ... %@", (self?.viewModel.currentAudioRecord?.recordFileURL?.absoluteString)!))
            }
        }
    }
    
    //MARK:- Recorder STOP
    func RecorderStop(){
        switch self.currentState {
        case .recording,.paused:
            self.chronometer?.stop()
            self.chronometer = nil
            do {
                try self.viewModel.stopRecording()
                self.currentState = .recorded
            } catch {
                self.currentState = .ready
                self.showAlert(with: error)
            }
            //            myscrollview.contentSize = CGSize.init(width: self.audioVisualizationView.bounds.size.width * 3, height: self.audioVisualizationView.bounds.size.height)
            //            let frame = self.audioVisualizationView.frame
            //            self.audioVisualizationView.frame = CGRect.init(x: 0, y: 0, width: frame.size.width * 3, height: frame.height)
            //            self.viewModel.currentAudioRecord!.meteringLevels = self.audioVisualizationView.scaleSoundDataToFitScreen()
            self.audioVisualizationView.audioVisualizationMode = .read
            //            ResetAfterRecord()
            WriteToDB_StopInfo()
//            self.openListVC(NSObject())
//            self.addSysTag(str: "Recorder End.")
            let dispatchTime: DispatchTime = DispatchTime.now()
                + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                self.ResetAfterRecord()
                MyReviewManager.showReviewWithAddEvent(vc: self) //是否显示评论
            })
            
            self.currentState = .ready
            self.resetViewTypeWithState()
            self.setupNaviView_Default()
            self.refreshByNewDBData()
        default:
            break
        }
    }
    func RecorderPause(){
        switch self.currentState {
        case .recording:
            self.chronometer?.pause()
            do {
                try self.viewModel.pauseRecording()
                self.currentState = .paused
            } catch {
                self.currentState = .ready
                self.showAlert(with: error)
            }
        default:
            break
        }
    }
    func RecorderRestart(){
        self.chronometer?.start()
        if self.currentState == .paused {
            do {
                try self.viewModel.restartRecording()
                self.currentState = .recording
            } catch {
                self.currentState = .ready
                self.showAlert(with: error)
            }
        }
    }
    func ResetAfterRecord(){
        do {
            try self.viewModel.resetRecording()
            self.audioVisualizationView.reset()
            self.currentState = .ready
//            self.initUI()
            self.setupUIWithState()
//            self.resetViewTypeWithState()
        } catch {
            self.showAlert(with: error)
        }
    }
}
