//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
//import PandoraPlayer
extension VMRecordViewController {
    
    func setupUIWithState(){
//        switch self.currentState {
//        case .recording,.paused:
//            self.ListButton.isHidden = true
//            self.StopButton.isHidden = false
////            MenuViewHeight.constant = 45
////            self.MenuView.isHidden = false
//        default:
//            self.ListButton.isHidden = false
//            self.StopButton.isHidden = true
////            MenuViewHeight.constant = 0
////            self.MenuView.isHidden = true
//        }
        self.audioVisualizationView.backgroundColor = UIColor.clear
        let type = AudioRecorderManager.shared.getCurrentAudioRecordType()
        let sample = AudioRecorderManager.shared.getCurrentAudioSampleRate()
        self.RecFormatLabel.text = String.init(format: "%@(%@),%@", type.RecTypeName,type.FileExtension,sample.SampleRateStr)
//        initUI()
        switch self.currentState {
        case .ready:
            self.messages.removeAll()
            self.collectionView.reloadData()
            self.audioVisualizationView.reset()
            
            self.mytimeLabel.text = ""
            self.RecFormatLabel.text = ""
            self.RecNameLabel.text = ""
            self.ListButton.isHidden = true
            self.StopButton.isEnabled = false
            break
        case .recording:
            self.StopButton.isEnabled = true
        case .paused:
            self.StopButton.isEnabled = false
        default:
            break
        }
    }
    func resetViewTypeWithState(){
        switch self.currentState {
        case .ready:
            setView(view:  self.ColView, hidden: true)
            setView(view:  self.TabView, hidden: false)
            break
        default:
            setView(view:  self.ColView, hidden: false)
            setView(view:  self.TabView, hidden: true)
            break
        }
    }
    @objc  func initUIColor(){
//        self.view.backgroundColor = UIColor(red: 113/255, green: 148/255, blue: 194/255, alpha: 1)
        self.view.backgroundColor = UIColor.groupTableViewBackground
        //输入框
//        self.InputMenuView.backgroundColor = UIColor.white
        //chat背景色
        //self.ColView.backgroundColor = GlobalBackgroundColor
        self.TabView.backgroundColor = GlobalBackgroundColor
        //self.tableView.backgroundColor = GlobalBackgroundColor
//        self.ColView.backgroundColor = MyAppUI.shared.recorder_backgroud_color
        self.ColView.backgroundColor = UIColor.clear
        
        self.tableView.backgroundColor = GlobalBackgroundColor
        self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor
        
        self.ButtomView.layer.shadowColor = UIColor.black.cgColor
        self.ButtomView.layer.shadowOpacity = 0.3 // 透明度
        self.ButtomView.layer.shadowOffset = CGSize(width: -1, height: -1) // 距離
        self.ButtomView.layer.shadowRadius = 1 // ぼかし量
        self.ButtomView.layer.cornerRadius = 5
//        self.ButtomView.layer.borderColor = GlobalHeadColor.cgColor
//        self.ButtomView.layer.borderWidth = 2
        self.ButtomView.clipsToBounds = true
        
        //chat背景色
        self.RecNameLabel.textColor = UIColor.flatWhite()
        self.RecFormatLabel.textColor = UIColor.flatWhite()
//        self.mytimeLabel.textColor = UIColor.flatWhiteColorDark()
        
        self.mytimeLabel.textColor = UIColor(red: 248, green: 249, blue: 252, alpha: 1)
    }
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
}
//MARK: - KeyBoard Handle 输入框的定制
extension VMRecordViewController :  UITextViewDelegate{
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        super.didRotate(from: fromInterfaceOrientation)
        self.collectionView.reloadData()
    }
    
    func addInputView(){
        self.inputContainerView.addSubview(self.inputToolBarContainerView)
        inputToolBarContainerView.translatesAutoresizingMaskIntoConstraints = false
        inputToolBarContainerView.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        inputToolBarContainerView.centerYAnchor.constraint(equalTo: inputContainerView.centerYAnchor).isActive = true
        
        inputToolBarContainerView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        inputToolBarContainerView.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor).isActive = true
    }
    func setupInputView(){
        self.inputToolBarContainerView = MyTagKeyBoardView(frame: self.inputContainerView.bounds)
        self.inputToolBarContainerView.delegate = self
        self.inputToolBarContainerView.editTextView.delegate = self
        self.inputToolBarContainerView.resignFirstResponder()

        //let localStr = NSLocalizedString("rec_view_send_btn", comment: "")
        //self.inputToolBarContainerView.sendButton.setTitle(localStr, for: .normal)
        //self.inputToolBarContainerView.sendButton.setTitleColor(UIColor.gray, for: .normal)
        
        self.inputToolBarContainerView.minimumContainerHeight = 50
        self.inputToolBarContainerView.maximumContainerHeight = 150
        
        //self.inputToolBarContainerView1 = MyTagKeyBoardView(frame: self.inputContainerView.bounds)
        //self.inputToolBarContainerView1.delegate = self
        //self.inputToolBarContainerView.textView.inputAccessoryView = inputToolBarContainerView1
        
        addInputView()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillChangeFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChangeFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)

        self.BottomViewHeight.constant = BottomViewHeight_Fix
        
        self.inputToolBarContainerView.backgroundColor = UIColor.clear
        self.inputToolBarContainerView.menuView.backgroundColor = UIColor.clear
        self.inputToolBarContainerView.editTextView.backgroundColor = UIColor.clear
        self.inputContainerView.backgroundColor = UIColor.white.withAlphaComponent(0.75)
//        self.BottomViewHeight.constant = 128
    }
//    override var inputAccessoryView: UIView? {
////        self.inputToolBarContainerView.removeFromSuperview()
//        return self.inputToolBarContainerView_2
//    }
//    override var canBecomeFirstResponder: Bool {
//        return true
//    }
    @objc func keyboardWillShow(notification: NSNotification) {
        self.setKeyboardOffset(notification: notification)
        print("keyboardWillShow-----------------")
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print("keyboardWillHide-----------------")
        self.BottomViewHeight.constant = self.BottomViewHeight_Fix
    }
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        self.setKeyboardOffset(notification: notification)
        print("keyboardWillChangeFrame-----------------")
    }
    @objc func keyboardDidChangeFrame(notification: NSNotification) {
//        self.setKeyboardOffset(notification: notification)
//        print("keyboardDidChangeFrame-----------------")
    }
    
    func setKeyboardOffset(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight: CGFloat
//        if #available(iOS 11.0, *) {
//            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
//        } else {
//            keyboardHeight = keyboardFrame.cgRectValue.height
//        }
        keyboardHeight = keyboardFrame.cgRectValue.height
        guard let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double,
        let options = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt else { return }
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.init(rawValue: options), animations: {
                self.BottomViewHeight.constant = keyboardHeight
            }, completion: { (success) in
        })

    }
    func textViewDidChange(_ textView: UITextView) {
        self.resetInputToolBarContainerViewUI()
    }
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11.0, *) {
            super.viewSafeAreaInsetsDidChange()
            print(view.safeAreaInsets.bottom)
            print("viewSafeAreaInsetsDidChange")
             // This value is the bottom safe area place value.
        }
    }
    
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true);
        return true;
    }
    //Reset InputToolBarContainerView UI
    func resetInputToolBarContainerViewUI(){
        self.inputToolBarContainerView.resetViewHeight()
    }
}
extension VMRecordViewController: MyTagKeyBoardViewDelegate {
    //send button
    func didTapSendButton(inputContainerView: MyTagKeyBoardView) {
        if inputContainerView.editTextView.text.isEmpty {
            self.EndEditTag()
            return
        }
        self.addTextTag(str: inputContainerView.editTextView.text)
    }
    func didTapEditButton(inputContainerView: MyTagKeyBoardView) {
        self.popupRenameDialog()
    }
    func didTapTagButton(inputContainerView: MyTagKeyBoardView) {
        addTextTag(str: nil)
    }
    func didChangeSize(inputContainerView: MyTagKeyBoardView, size: CGSize) {
        // TODO: remove inputContainerViewHeightConstraint
        // add an explicit height constraint in inputToolBarContainerView
        if self.inputToolBarContainerView.isHidden || self.inputContainerView.isHidden {
            self.inputContainerViewHeightConstraint.constant = 0
        }else{
            self.inputContainerViewHeightConstraint.constant = size.height
            self.inputToolBarContainerView.translatesAutoresizingMaskIntoConstraints = true
            self.inputToolBarContainerView.frame.size = CGSize(width: self.inputToolBarContainerView.frame.size.width, height: size.height)
        }
        if self.messageEdit_CurrentIndex > 0{
            
        }else{
//            self.scrollToLastItem(animated: false)
        }
    }

    func didTapPhotoButton(inputContainerView: MyTagKeyBoardView) {
        PickupImage()
    }
    func didTapCameraButton(inputContainerView: MyTagKeyBoardView) {
        callCamera(true)
    }
    func startEditTagBean(_ bean: ZukMessageModel?){
        self.inputToolBarContainerView.editTextView.text = bean?.body
        self.inputToolBarContainerView.editTextView.becomeFirstResponder()
        for (i,item) in self.messages.enumerated(){
            if item.No == bean?.No{
                messageEdit_CurrentIndex = i
            }
        }
    }
    func EndEditTag(){
        self.view.endEditing(true)
        self.inputToolBarContainerView.editTextView.endEditing(true)
        self.inputToolBarContainerView.editTextView.text = ""
        self.resetInputToolBarContainerViewUI()
        self.messageEdit_CurrentIndex = -1
    }
}






