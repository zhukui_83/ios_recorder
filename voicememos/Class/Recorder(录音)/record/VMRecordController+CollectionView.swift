//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
//import PandoraPlayer


extension VMRecordViewController :  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func setupCollectionViewUI() {
//        tableView.backgroundColor = UIColor(red: 113/255, green: 148/255, blue: 194/255, alpha: 1)
//        let nib = UINib(nibName: "ILIncomingMessageCollectionViewCell", bundle: nil)
//        self.collectionView.register(nib, forCellWithReuseIdentifier: "ILIncomingMessageCollectionViewCell")
//        let nib2 = UINib(nibName: "ILOutgoingMessageCollectionViewCell", bundle: nil)
//        self.collectionView.register(nib2, forCellWithReuseIdentifier: "ILOutgoingMessageCollectionViewCell")
//
        self.collectionView.register(ILIncomingMessageCollectionViewCell.nib, forCellWithReuseIdentifier: ILIncomingMessageCollectionViewCell.identifier)
        self.collectionView.register(ILOutgoingMessageCollectionViewCell.nib, forCellWithReuseIdentifier: ILOutgoingMessageCollectionViewCell.identifier)
        self.collectionView.register(LeftMessageCollectionViewCell.nib, forCellWithReuseIdentifier: LeftMessageCollectionViewCell.identifier)
        self.collectionView.register(CenterMessageCollectionViewCell.nib, forCellWithReuseIdentifier: CenterMessageCollectionViewCell.identifier)
        self.collectionView.register(RightMessageCollectionViewCell.nib, forCellWithReuseIdentifier: RightMessageCollectionViewCell.identifier)
//        self.collectionView.register(RightMessageCollectionViewCell.nib, forCellWithReuseIdentifier: RightMessageCollectionViewCell.identifier)
//        self.messages = self.prepareDemoMessages()
        self.collectionView.messages = self.messages
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.clear
//        self.inputToolBarContainerView = ILDirectMessagesInputContainerView(frame: self.inputContainerView.frame)
//        self.inputToolBarContainerView.delegate = self
//
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ILDirectMessagesViewController.keyboardWillShow(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ILDirectMessagesViewController.keyboardWillHide(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
            context.invalidateFlowLayoutCache = true
            (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
            self.collectionView.scrollToLastItem(animated: false)
        }
    }
    
    func prepareDemoMessages() -> [ILMessage] {
        return messages
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message = self.messages[indexPath.item]

        if message.isIncomingType == .center{
            var cell: CenterMessageCollectionViewCell!
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: CenterMessageCollectionViewCell.identifier, for: indexPath) as! CenterMessageCollectionViewCell
            cell.delegate = self
            cell.configure(message: message)
            return cell!
        }else{
            if message.isIncomingType == .right {
                var cell: RightMessageCollectionViewCell!
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: RightMessageCollectionViewCell.identifier, for: indexPath) as! RightMessageCollectionViewCell
                
                cell.delegate = self
                cell.configure(message: message)
                return cell!
            } else {
                var cell: LeftMessageCollectionViewCell!
                //            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ILIncomingMessageCollectionViewCell", for: indexPath) as! ILIncomingMessageCollectionViewCell
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: LeftMessageCollectionViewCell.identifier, for: indexPath) as! LeftMessageCollectionViewCell
                cell.delegate = self
                cell.configure(message: message, editmode: true)
                //            cell.setNeedsLayout()
                //            cell.layoutIfNeeded()
                return cell!
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.collectionView.collectionViewLayout as! ILDirectMessagesCollectionViewFlowLayout).sizeForItem(at: indexPath)
        print(indexPath)
        print(size)
        return size
    }
}

extension VMRecordViewController {
//    func scrollToLastItem(animated: Bool) {
//        if self.collectionView.numberOfSections == 0 {
//            return
//        }
//        
//        let lastIndexPath = IndexPath(item: self.collectionView.numberOfItems(inSection: 0) - 1, section: 0)
//
//        if self.collectionView.numberOfSections < lastIndexPath.section {
//            return
//        }
//
//        let contentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
//        if contentHeight < self.collectionView.bounds.height {
//            let visibleRect = CGRect(x: 0.0, y: contentHeight, width: 0.0, height: 0.0)
//            self.collectionView.scrollRectToVisible(visibleRect, animated: animated)
//            return
//        }
//
//        guard let collectionViewLayout = self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout else { return }
//        let cellSize = collectionViewLayout.sizeForItem(at: lastIndexPath)
//        let boundsHeight = self.collectionView.bounds.height
//            //- self.inputContainerView.frame.height
//        let position =  (cellSize.height > boundsHeight) ? UICollectionViewScrollPosition.bottom : UICollectionViewScrollPosition.top
//
//        self.collectionView.scrollToItem(at:lastIndexPath, at: position, animated: animated)
//    }
//
    func animateSending(animated: Bool, inputContainerView: ILDirectMessagesInputContainerView?) {
        // Reset the textview
        if let inputContainerView  = inputContainerView{
            guard let textView = inputContainerView.textView else { return }
            textView.text = nil
            inputContainerView.layoutTextView(with: textView.text)
        }
        // Update the layout
        self.collectionView.reloadData()
//        self.scrollToLastItem(animated: animated)
        self.collectionView.scrollToLastItem(animated: animated)
    }
}
extension VMRecordViewController:MessageCollectionViewCellDelegate{
    func MessageMenuBtnClick(_ bean: ZukMessageModel?) {
        let shareMenu = UIAlertController(title: nil, message: "Menu", preferredStyle: .actionSheet)
        let editAction = UIAlertAction(
            title: NSLocalizedString("rec_view_tag_editdialog_edit", comment: ""),
            style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.startEditTagBean(bean)
        })
        let delAction = UIAlertAction(
            title: NSLocalizedString("rec_view_tag_editdialog_delete", comment: ""),
            style: .default, handler: {
            (action:UIAlertAction) -> Void in
            self.deleteTag(bean)
        })
        let cancelAction = UIAlertAction(title:
            NSLocalizedString("rec_view_tag_editdialog_cancel", comment: ""), style: .cancel, handler: nil)
        shareMenu.addAction(editAction)
        shareMenu.addAction(delAction)
        shareMenu.addAction(cancelAction)
        shareMenu.modalPresentationStyle = .popover
        if let presentation = shareMenu.popoverPresentationController {
            presentation.barButtonItem = navigationItem.rightBarButtonItems?[0]
        }
        self.present(shareMenu, animated: true, completion: nil)
    }
    //MARK:更新数据
    func deleteTag(_ bean: ZukMessageModel?){
        for (i,item) in self.messages.enumerated(){
            if item.No == bean?.No{
                WriteToDB_DeleteTag(model: bean)
                self.messages.remove(at: i)
                self.collectionView.messages = self.messages
//                self.collectionView.reloadData()
                self.collectionView.deleteItems(at: [IndexPath.init(row: i, section: 0)])
                DispatchQueue.main.async {
                    let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
                    context.invalidateFlowLayoutCache = true
                    (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
//                    self.scrollToLastItem(animated: false)
                }
                return
            }
        }
    }
    
    
    
    
    
}

