import UIKit


// MARK: - DB
extension RecorderVC {
    // MARK: Detail DB -----
    func WriteToDB_AddTag(fileNo: String, model:ZukMessageModel){
        let detailbean = RecTagDetail.init()
        detailbean.type = model.MsgType.rawValue
        detailbean.fileName = (self.viewModel.currentAudioRecord?.recordFileName)!
        detailbean.detailNo = model.No
        if let body = model.body{
            detailbean.detail = body
        }
        if let imgname = model.ImageName{
            detailbean.imgname = imgname
        }
        
        detailbean.TagTime = model.TagTime //time
        if let url = model.url{
            detailbean.url = url
        }
        detailbean.senderName = model.senderName//senderName
        detailbean.isIncomingType = model.isIncomingType.rawValue//isIncoming
        dbDetailTb.add(detailbean)
    }
    // MARK: delete tag
    func WriteToDB_DeleteTag(model: ZukMessageModel?){
        if self.viewModel.currentAudioRecord != nil{
            let fileName = (self.viewModel.currentAudioRecord?.recordFileName)!
            let detailno = model?.No
            dbDetailTb.deleteByfileNameAndNo(fileName: fileName,detailNo: detailno!)
        }
    }
    // MARK: delete tag
    func WriteToDB_UpdateTag(fileNo: String, model:ZukMessageModel){
        self.WriteToDB_DeleteTag(model: model)
        self.WriteToDB_AddTag(fileNo: fileNo, model: model)
    }
    // MARK: 开始录音时的，保存的信息，开始时，没有扩展名，最后再添加扩展名
    func WriteToDB_StartInfo(){
        var filename =  (self.viewModel.currentAudioRecord?.recordFileName!)!
//        if self.viewModel.currentAudioRecord?.recordType?.RecType == .mp3{
//            filename = String.init(format: "%@%@", filename, (self.viewModel.currentAudioRecord?.recordType?.FileExtension)!)
//        }
//        let rectype = self.viewModel.currentAudioRecord?.recordType?.RecType.rawValue
        let bean = TrackMain.init(filename:filename,
                                  rectype: (self.viewModel.currentAudioRecord?.recordType)!)
        
        bean.recexten = ""//开始时，没有扩展名，最后再添加扩展名
        bean.title = ""
        bean.comment = ""
        dbMainTb.add(bean)
    }
    // MARK: DB End Recorde-----
    func WriteToDB_StopInfo(){
        // File Rename
        if self.viewModel.currentAudioRecord?.recordNewFileName != nil{
            RenameFileAndUpdatDB()
        }else{
            
        }
    }
    // MARK: Rename RECFILE-----
    func RenameFileAndUpdatDB(){
        do {
            if var fileurl = self.viewModel.currentAudioRecord?.recordFileURL,
                var filename = self.viewModel.currentAudioRecord?.recordFileName,
                var rectype = self.viewModel.currentAudioRecord?.recordType?.RecType,
                let fileExtension = self.viewModel.currentAudioRecord?.recordType?.FileExtension {
                
                var filenamewithextension = String.init(format: "%@%@", filename, fileExtension)
                if let newfilename = self.viewModel.currentAudioRecord?.recordNewFileName{
                    filenamewithextension = String.init(format: "%@%@", newfilename, fileExtension)
                }
                var duration = 0
                var filesize = 0
                
                if self.viewModel.currentAudioRecord?.recordType?.RecType == .mp3{
                    //mp3,做特殊处理。
                    //filename = String.init(format: "%@%@", filename, fileExtension)
                    print("---save mp3---")
                    //录音结束，删除一般的文件
                    guard let man = TFAudioFileManager.sharedInstance() else{
                        return
                    }
                    man.sendEndRecord() //结束录音
                    (duration,filesize) = VMFileClass.sharedInstance.getFileDurationAndFileSize(filename: filename)

                    try FileManager().removeItem(at: fileurl)
                    print(String.init(format: "record delete.%@", fileurl.absoluteString))
                    //重命名MP3文件
                    let mp3path = String.init(format: "%@%@", fileurl.absoluteString, fileExtension)
                    guard var mp3url = URL.init(string: mp3path) else{
                        return
                    }
                    var resourceValues = URLResourceValues()
                    resourceValues.name = filenamewithextension
                    try mp3url.setResourceValues(resourceValues)
                    print(String.init(format: "record end.%@", mp3url.absoluteString))
                }else{
                    var resourceValues = URLResourceValues()
                    resourceValues.name = filenamewithextension
                    try fileurl.setResourceValues(resourceValues)
                    
                    (duration,filesize) = VMFileClass.sharedInstance.getFileDurationAndFileSize(filename: filenamewithextension)
                }
                print(filenamewithextension)
                
                // File Rename
                if let bean = dbMainTb.getAllByfilename(filename){
                    bean.rectype = rectype.rawValue
                    if let newfilename = self.viewModel.currentAudioRecord?.recordNewFileName{
                        bean.filename = newfilename
                    }else{
                        bean.filename = filename
                    }
                    bean.recexten = fileExtension
                    bean.title = (self.viewModel.currentAudioRecord?.recordTitle)!
                    bean.comment = (self.viewModel.currentAudioRecord?.recordDesc)!
                    bean.datasize = filesize
                    bean.duration = duration
                    dbMainTb.updateByID(bean)
                }
                //file名称变化的时候需要更新详细的名称
                if let newfilename = self.viewModel.currentAudioRecord?.recordNewFileName{
                    dbDetailTb.updateFileName(newfilename, filename: filename)
                }
            }
        } catch { print("failed to set resource value") }
    }
    
    
    
    
    
    
}
