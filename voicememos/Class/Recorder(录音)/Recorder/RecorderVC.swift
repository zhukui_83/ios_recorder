//
//  RecorderVC.swift
//  voicememos
//
//  Created by ksymac on 2019/07/27.
//  Copyright © 2019 ZHUKUI. All rights reserved.
//

import UIKit
import SoundWave

class RecorderVC: BaseViewController {
    let dbMainTb = DBMainTable.shared//model
    let dbStatusTb = DBStatusTable.shared//model
    let dbDetailTb = DBDetailTable.shared//model
    
    var current_modify_bean:TrackMain?
    
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var ColView: UIView!
    @IBOutlet weak var collectionView: ILDirectMessagesCollectionView!
    
    @IBOutlet weak var RecNameLabel: UILabel!
    //@IBOutlet weak var InputMenuView: UIView!
    //@IBOutlet weak var InputMenuViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ButtomView: UIView!
    var bottomLine:UIView?
    //@IBOutlet weak var ListButton: UIButton!
    @IBOutlet weak var MyRecordButton: UIButton!
    @IBOutlet weak var StopButton: UIButton!
    //@IBOutlet weak var recordButton: UIButton?
    //@IBOutlet weak var CenterRecordButton: UIButton!
    //@IBOutlet weak var CenterRecordButtonTop: NSLayoutConstraint!
    //@IBOutlet var SettingsButton: UIButton!
    @IBOutlet var audioVisualizationView: AudioVisualizationView!
    
    @IBOutlet var optionsView: UIView!
    @IBOutlet var optionsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var audioVisualizationTimeIntervalLabel: UILabel!
    @IBOutlet var meteringLevelBarWidthLabel: UILabel!
    @IBOutlet var meteringLevelSpaceInterBarLabel: UILabel!
    @IBOutlet var mytimeLabel: UILabel!
    @IBOutlet weak var RecFormatLabel: UILabel!
    
    //MARK: input view
    @IBOutlet weak var inputContainerView: UIView!
    var inputToolBarContainerView: MyTagKeyBoardView!
    @IBOutlet weak var inputContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var TitleEditView: UIView!
    @IBOutlet weak var TitleEditViewHeight: NSLayoutConstraint!
    @IBOutlet weak var BottomViewHeight: NSLayoutConstraint!
    let BottomViewHeight_Fix:CGFloat = 110
    
    
    let viewModel = RecorderManager.shared.viewModel
    var chronometer: Chronometer?
    //var recStartTime : Date?
    //var bottomView: ChatRoomInputView! //画面下部に表示するテキストフィールドと送信ボタン
    var chats: [ChatEntity] = []
    
    var messages: [ZukMessageModel] = []
    var sysmessages: [ZukMessageModel] = []
    
    //    var messageNo = -99
    var messageEdit_CurrentIndex = -99

    // MARK: TableView
    var listTrackMain : Array<TrackMain> = []
    
    enum AudioRecodingState {
        case ready
        case recording
        case recorded
        //case playing
        case paused
        var buttonImage: UIImage {
            switch self {
            //
            case .ready:
                return UIImage(named: "Record-Button")!
            case .recording:
                return UIImage(named: "Pause-Button")!
            case .paused:
                return UIImage(named: "Record-Button")!
            case .recorded:
                return UIImage(named: "Record-Button")!
                //case .playing:
                //      return UIImage(named: "Pause-Button")!
            }
        }
        
        var audioVisualizationMode: AudioVisualizationView.AudioVisualizationMode {
            switch self {
            case .ready, .recording:
                return .write
            case .paused,  .recorded://.playing,
                return .read
            }
        }
    }
    
    
    //根据状态设置图片等
    var currentState: AudioRecodingState = .ready {
        didSet {
            self.MyRecordButton.setImage(self.currentState.buttonImage.maskWithColor(color: UIColor.darkGray), for: UIControlState())
            self.audioVisualizationView.audioVisualizationMode = self.currentState.audioVisualizationMode
        }
    }
    
    class func instance() -> RecorderVC {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RecorderVC") as! RecorderVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.askAudioRecordingPermission()
        self.viewModel.audioMeteringLevelUpdate = { [weak self] meteringLevel in
            guard let this = self, this.audioVisualizationView.audioVisualizationMode == .write else {
                return
            }
            this.audioVisualizationView.addMeteringLevel(meteringLevel)
        }
        
        self.viewModel.audioDidFinish = {
            [weak self] in
            self?.currentState = .recorded
            self?.audioVisualizationView.stop()
        }
        
        self.initUIColor()
        self.initMessages()
        self.setupCollectionViewUI()
        self.setupInputView()
        
        TitleEditView.isHidden = true
        
        self.setupViewUI();
        self.setupUIWithState()
        self.resetViewTypeWithState()
        
        //start record
        switch self.currentState  {
        case .ready:
            MyAppPermission.shared.askMicroPhonePermission {[weak self] (b) in
                if b == true{
                    self?.RecorderStart()
                    self?.setupNaviView_RecodeStart(self?.RecNameLabel.text!)
                }else{
                    self?.showDialogForMicrophone()
                }
            }
        case .recording:
            RecorderPause()
        case .paused:
            RecorderRestart()
        default:
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: 重新绘制无数据视图
    func setupViewUI(){
        if isiphonex {
            self.BottomViewHeight.constant = 102
        }else{
            self.BottomViewHeight.constant = 80
        }
        self.backgroundView.image = UIImage.init(named: "background_6")
        self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        StopButton.setImage(UIImage(named: "Stop-Button")?.maskWithColor(color: UIColor.darkGray), for: .normal)
        
        MyRecordButton.layer.shadowRadius = 2// ぼかし量
        MyRecordButton.layer.shadowOpacity = 0.3// 透明度
        MyRecordButton.layer.shadowOffset = CGSize(width: 5, height: 5) // 距離
        
        StopButton.layer.shadowRadius = 2
        StopButton.layer.shadowOpacity = 0.3
        StopButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        if bottomLine == nil{
            self.bottomLine = UIView.init(frame: CGRect.init(x: 10, y: 0, width: self.ButtomView.bounds.width-20, height: 1))
            self.bottomLine?.backgroundColor = UIColor.groupTableViewBackground
            //self.bottomLine?.layer.shadowColor = UIColor.black.cgColor
            //self.bottomLine.layer.cornerRadius = 5
            self.ButtomView.addSubview(self.bottomLine!)
        }
    }
    
    var isiphonex: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    func setupNaviView_RecodeStart(_ title: String?){
        self.navigationItem.titleView = nil
        self.navigationItem.title = title
        let editItem : UIBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icons8-edit"),
            style: UIBarButtonItemStyle.plain,
            target: self, action: #selector(self.ShowRenameDialog(_:)))
        
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.rightBarButtonItems = [editItem,]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        //self.ButtomView.backgroundColor = MyAppUI.shared.recorder_tabelview_bottomcolor.withAlphaComponent(1.0)
        self.view.backgroundColor =  MyAppUI.shared.recorder_backgroud_color
        
        self.RecNameLabel.text = self.viewModel.currentAudioRecord?.recordNewFileName
        self.RecNameLabel.textColor = GlobalHeadColor
        self.RecNameLabel.isHidden = true
        
        //self.RecFormatLabel.textColor = GlobalHeadColor
        //self.mytimeLabel.textColor = GlobalHeadColor
        self.RecFormatLabel.textColor = UIColor.gray
        self.mytimeLabel.textColor = UIColor.gray
        
        self.MyRecordButton.isHidden = false
        self.StopButton.isHidden = false
    }
    
    //    func setupNaviView_RecodeStop(){
    //        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
    //        self.navigationController?.navigationBar.barTintColor  = GlobalHeadColor
    //    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    deinit{
        print("------deinit------")
    }
    
    // MARK: - Actions
    @IBAction func recordButtonDidTouchDown(_ sender: AnyObject) {
        RecorderStart()
    }
    
    @IBAction func recordButtonDidTouchUpInside(_ sender: AnyObject) {
        RecorderStop()
    }
    
    // MARK: - MyActions
    @IBAction func MyRecordButtonStart(_ sender: AnyObject) {
        switch self.currentState  {
        case .ready:
            weak var weak_self = self
            MyAppPermission.shared.askMicroPhonePermission { (b) in
                if b == true{
                    weak_self?.RecorderStart()
                    weak_self?.setupNaviView_RecodeStart(weak_self?.RecNameLabel.text!)
                }else{
                    weak_self?.showDialogForMicrophone()
                }
            }
        case .recording:
            RecorderPause()
        case .paused:
            RecorderRestart()
        default:
            return
        }
    }
    
    func showDialogForMicrophone(){
        let alert = UIAlertController(title: "Unable to access Microphone, please check system settings.", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
            MyAppPermission.shared.requetSettingForAuth()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { [unowned self] _ in
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func MyRecordButtonStop(_ sender: AnyObject) {
        RecorderStop()
    }
    
    func offsetscrollview(){
    }
    
    @IBAction func clearButtonTapped(_ sender: AnyObject) {
        ResetAfterRecord()
    }
    
    @IBAction func switchValueChanged(_ sender: AnyObject) {
        let theSwitch = sender as! UISwitch
        if theSwitch.isOn {
            self.view.backgroundColor = UIColor.mainBackgroundPurple
            self.audioVisualizationView.gradientStartColor = UIColor.audioVisualizationPurpleGradientStart
            self.audioVisualizationView.gradientEndColor = UIColor.audioVisualizationPurpleGradientEnd
        } else {
            self.view.backgroundColor = UIColor.mainBackgroundGray
            self.audioVisualizationView.gradientStartColor = UIColor.audioVisualizationGrayGradientStart
            self.audioVisualizationView.gradientEndColor = UIColor.audioVisualizationGrayGradientEnd
        }
    }
    
    @IBAction func audioVisualizationTimeIntervalSliderValueDidChange(_ sender: AnyObject) {
        let audioVisualizationTimeIntervalSlider = sender as! UISlider
        self.viewModel.audioVisualizationTimeInterval = TimeInterval(audioVisualizationTimeIntervalSlider.value)
        self.audioVisualizationTimeIntervalLabel.text = String(format: "%.2f", self.viewModel.audioVisualizationTimeInterval)
    }
    
    @IBAction func meteringLevelBarWidthSliderValueChanged(_ sender: AnyObject) {
        let meteringLevelBarWidthSlider = sender as! UISlider
        self.audioVisualizationView.meteringLevelBarWidth = CGFloat(meteringLevelBarWidthSlider.value)
        self.meteringLevelBarWidthLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
    }
    
    @IBAction func meteringLevelSpaceInterBarSliderValueChanged(_ sender: AnyObject) {
        let meteringLevelSpaceInterBarSlider = sender as! UISlider
        self.audioVisualizationView.meteringLevelBarInterItem = CGFloat(meteringLevelSpaceInterBarSlider.value)
        self.meteringLevelSpaceInterBarLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
    }
    
    @IBAction func optionsButtonTapped(_ sender: AnyObject) {
        let shouldExpand = self.optionsViewHeightConstraint.constant == 0
        self.optionsViewHeightConstraint.constant = shouldExpand ? 165.0 : 0.0
        UIView.animate(withDuration: 0.2) {
            self.optionsView.subviews.forEach { $0.alpha = shouldExpand ? 1.0 : 0.0 }
            self.view.layoutIfNeeded()
        }
    }
    

//    @IBAction func openListVC(_ sender: Any) {
//        let storyboard: UIStoryboard = UIStoryboard(name: "HistoryMain", bundle: nil)
//        let vc: MyTabBarController = storyboard.instantiateViewController(withIdentifier: "MyTabBarController") as! MyTabBarController
//        self.present(vc, animated: true, completion: nil)
//    }
//    @IBAction func openFileListVC(_ sender: Any) {
//        let storyboard: UIStoryboard = UIStoryboard(name: "WebServerMain", bundle: nil)
//        let vc: UITabBarController = storyboard.instantiateViewController(withIdentifier: "RootController") as! UITabBarController
//        self.present(vc, animated: true, completion: nil)
//    }
}
//MARK: SetUp Data and UI With Record Start
extension RecorderVC{
    func setupStartRecordUI(){
        self.initMessages()
        //        let start_str = String(format: "%@ \n %@  " , (self.viewModel.currentAudioRecord?.recordFileName)!, "Recorder starting...")
        let start_str = String(format: "%@ " , NSLocalizedString("rec_view_rec_starting", comment: ""))
        self.addSysTag(str: start_str)
    }
    func initMessages(){
        self.messages = []
        //        self.messageNo = 0
        self.messageEdit_CurrentIndex = -1
    }
}



