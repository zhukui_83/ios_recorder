//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave
import AVFoundation
import MobileCoreServices
//import PandoraPlayer

extension RecorderVC {
    //Rename
    @IBAction func RenameButtonTapped(_ sender: AnyObject) {
        self.popupRenameDialog()
    }

    //MARK: Add Tag
    @IBAction func Btn_Tag_Add(_ sender: AnyObject) {
        addTextTag(str: nil)
    }
    func addTextTag(str: String?){
        var msgBody = ""
        if let str = str {
            msgBody = str
        }else{
            msgBody = String(format: "%@ %3d",
                             NSLocalizedString("rec_view_tag", comment: ""), self.getNextMessageNo())
        }
        var msgno = -1
        var upd_flg = 0
        if messageEdit_CurrentIndex >= 0{
            self.messages[messageEdit_CurrentIndex] = self.TxtMessage(with:msgBody,msg_index: messageEdit_CurrentIndex)
            if self.viewModel.currentAudioRecord != nil{
                WriteToDB_UpdateTag(fileNo: (self.viewModel.currentAudioRecord?.recordFileName)!, model:self.messages[messageEdit_CurrentIndex])
            }
            
            msgno = self.messages[messageEdit_CurrentIndex].No
            upd_flg = 1//update
        }else{
            let message = self.TxtMessage(with: msgBody)
            message.isIncomingType = .left
            self.messages.append(message)
            if self.viewModel.currentAudioRecord != nil{
                WriteToDB_AddTag(fileNo: (self.viewModel.currentAudioRecord?.recordFileName)!, model:message)
            }
        }
        self.collectionView.messages = self.messages
//        self.collectionView.reloadData()
        self.EndEditTag()
//        self.animateSending(animated: true, inputContainerView: nil)
        if upd_flg == 0{
            self.animateSending(animated: true, inputContainerView: nil)
        }else{
            resetCollectionViewCellWith(msgno:msgno,flg:upd_flg)
        }
    }
    func resetCollectionViewCellWith(msgno:Int,flg:Int){
        var indexrow = -1
        for (index, element) in self.messages.enumerated() {
            if element.No == msgno{
                indexrow = index
            }
        }
        if indexrow >= 0{
            let temp1 = IndexPath.init(row: indexrow, section: 0)
            if flg == 0{
                self.collectionView.insertItems(at: [temp1])
//                self.collectionView.invalidateIntrinsicContentSize()
            }else{
                self.collectionView.reloadItems(at: [temp1])
//                self.collectionView.invalidateIntrinsicContentSize()
            }
            DispatchQueue.main.async {
                let context = ILDirectMessagesCollectionViewFlowLayoutInvalidationContext.context
                context.invalidateFlowLayoutCache = true
                (self.collectionView.collectionViewLayout as? ILDirectMessagesCollectionViewFlowLayout)?.invalidateLayout(with: context)
                //                    self.scrollToLastItem(animated: false)
                //                    self.scrollToItem(index: i-1, animated: true)
//                if flg == 0 {
//                    self.scrollToItem(index: indexrow, animated: true)
//                }
            }
        }
    }
    
    
    
    
    func addSysTag(str : String){
        let message = self.TxtMessage(with: str)
//        message.isIncoming = false
        message.isIncomingType = .center
        self.messages.append(message)
        self.collectionView.messages = self.messages
        self.collectionView.reloadData()
//        self.animateSending(animated: true, inputContainerView: nil)
    }
    
    func addImgTag(img:UIImage,url:URL){
        if messageEdit_CurrentIndex >= 0{
            self.messages[messageEdit_CurrentIndex] = self.ImgMessage(with:img,msg_index: messageEdit_CurrentIndex)
            if self.viewModel.currentAudioRecord != nil{
                WriteToDB_UpdateTag(fileNo: (self.viewModel.currentAudioRecord?.recordFileName)!, model:self.messages[messageEdit_CurrentIndex])
            }
        }else{
            let mediaMessage1 = self.ImgMessage(with: img)
            self.messages.append(mediaMessage1)
            if self.viewModel.currentAudioRecord != nil{
                WriteToDB_AddTag(fileNo: (self.viewModel.currentAudioRecord?.recordFileName)!, model:mediaMessage1)
            }
        }
        self.collectionView.messages = self.messages
        self.collectionView.reloadData()
//        self.animateSending(animated: true, inputContainerView: nil)
        self.EndEditTag()
    }

}
let photo_picker = UIImagePickerController()
extension RecorderVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBAction func Btn_ImgTag_Add(_ sender: AnyObject) {
        PickupImage()
    }
    func PickupImage() {
        photo_picker.sourceType = .photoLibrary
        photo_picker.allowsEditing = false
        photo_picker.delegate = self
        photo_picker.mediaTypes = [kUTTypeImage as String]
        present(photo_picker, animated: true, completion: nil)
    }
    @IBAction func CameraButtonTapped(_ sender: AnyObject) {
        callCamera(true)
    }
    func callCamera(_ animated: Bool) {
        photo_picker.sourceType = .camera
        photo_picker.delegate = self
        present(photo_picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if picker.sourceType == .camera{
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        addImgTag(img: image,url: URL.init(string: "https://cdn.vox-cdn.com/uploads/chorus_image/image/55102943/692626382.0.jpg")!)
        
        photo_picker.dismiss(animated: true) {
            
        }
      }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        photo_picker.dismiss(animated: true) {
            
        }
    }
}

extension RecorderVC {
    func getNextMessageNo()->Int{
        let max_detailno = dbDetailTb.getMaxDetailNoWithfileName((viewModel.currentAudioRecord?.recordFileName)!)
        return max_detailno + 1
//        self.messageNo  = self.messageNo  + 1
//        return self.messageNo
    }
    func TxtMessage(with body: String, msg_index: Int? = nil) -> ZukMessageModel {
        if let index = msg_index{
            let message = self.messages[index]
            message.MsgType = ZukMessageModel.MessageType.text
            message.body = body
            return message
        }else{
            let message = ZukMessageModel(ZukMessageModel.MessageType.text, msgincoming:.left)
            message.No = getNextMessageNo()
            message.isIncomingType = .left
            message.senderName = sendname_left
            message.TagTime = getIntervalTime()
            message.date = Date(timeIntervalSinceNow: 0)
            message.body = body
            return message
        }
    }
    func ImgMessage(with img: UIImage, msg_index: Int? = nil) -> ZukMessageModel {
        if let index = msg_index{
            let message = self.messages[index]
            message.MsgType = ZukMessageModel.MessageType.image
            message.image = img
            message.body = ""
            let imgname = saveImage(image: img)
            message.ImageName = imgname
            return message
        }else{
            let message = ZukMessageModel(ZukMessageModel.MessageType.image, msgincoming: .left)
            message.MsgType = ZukMessageModel.MessageType.image
            message.date = Date(timeIntervalSinceNow: 0)
            message.isIncomingType = .left
            message.senderName = sendname_left
            message.image = img
            let imgname = saveImage(image: img)
            message.ImageName = imgname
            message.url = imgname
            message.No = getNextMessageNo()
            message.TagTime = getIntervalTime()
            return message
        }
    }
    func getIntervalTime()->Double{
        if let startTime = self.viewModel.currentAudioRecord?.recordStartTime{
            let now = NSDate()
            let span = now.timeIntervalSince(startTime)
            let seconds:Double = span
            return seconds
        }else{
            return 0.0
        }
    }
    
    func saveImage (image: UIImage) -> String{
        let imgname = "img"+MyClassFile.sharedInstance.getStrWithNow()+".jpg"
        let path = MyClassFile.sharedInstance.getFileUrlWithFileName(type: .Image, filename: imgname)
//        let pngImageData = UIImagePNGRepresentation(image)
        let jpgImageData = UIImageJPEGRepresentation(image, 0.8)
        do {
            try jpgImageData?.write(to: path!, options: .atomic)
            return imgname
        } catch {
            print(error)
            return ""
        }
    }
}

//MARK: 修改Title 和 Desc
extension RecorderVC:TitleEditViewControllerDelegate {
    func renameRecFileName(filename: String, title: String, desc: String) {
        if filename.removeWhitespace().utf8.count > 0{
            self.viewModel.currentAudioRecord?.recordNewFileName = filename.removeWhitespace()
            self.setupNaviView_RecodeStart(self.viewModel.currentAudioRecord?.recordNewFileName)
        }
        var sys_str :String?
        if title.removeWhitespace().utf8.count > 0{
            self.viewModel.currentAudioRecord?.recordTitle = title.removeWhitespace()
            sys_str = String(format: " %@ %@ " , NSLocalizedString("rec_cell_msg_title", comment: ""), (self.viewModel.currentAudioRecord?.recordTitle)!)
        }
        if desc.removeWhitespace().utf8.count > 0{
            self.viewModel.currentAudioRecord?.recordDesc = desc.removeWhitespace()
            if let str = sys_str{
                sys_str = String(format: "%@\n %@ %@ " ,
                                 str, NSLocalizedString("rec_cell_msg_desc", comment: ""), (self.viewModel.currentAudioRecord?.recordDesc)!)
            }else{
                sys_str = String(format: " %@ %@ " , NSLocalizedString("rec_cell_msg_desc", comment: ""), (self.viewModel.currentAudioRecord?.recordDesc)!)
            }
        }
        if sys_str != nil{
            self.addSysTag(str: sys_str!)
        }
        
        self.viewModel.currentAudioRecord?.recordTitle = title
        self.viewModel.currentAudioRecord?.recordDesc = desc
        self.EndEditTag()
    }
    func cancelClicked() {
        self.EndEditTag()
    }
    @objc func ShowRenameDialog(_ sender: AnyObject) {
        self.popupRenameDialog()
    }
    func popupRenameDialog(){
        guard  let _ = self.viewModel.currentAudioRecord else{
            return
        }
        
        var filename = viewModel.currentAudioRecord?.recordFileName
        if let newname = viewModel.currentAudioRecord?.recordNewFileName{
            filename = newname
        }
        var title = ""
        if let recordTitle = viewModel.currentAudioRecord?.recordTitle{
            title = recordTitle
        }
        var desc = ""
        if let recordDesc = viewModel.currentAudioRecord?.recordDesc{
            desc = recordDesc
        }
        
        let alertView = TitleEditViewController(filename:filename!,
                                                title: title,desc: desc)
        alertView.deleage = self
        present(alertView, animated: true, completion: nil)
    }
}

