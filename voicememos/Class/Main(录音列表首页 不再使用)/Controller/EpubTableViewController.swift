//
//  FileTableViewController.swift
//  MediaBox
//
//  Created by Zhu Kui on 2018/01/29.
//  Copyright © 2018年 Zhu Kui. All rights reserved.
//

import UIKit
import AVFoundation
//import PandoraPlayer

class EpubTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFileData()
    }
    
    var FileList :[MyBoxFileModel]?
    var DirectoryList: [MyBoxDirectoryModel]?
    func getFileData(){
        let videourl = MyClassFile.sharedInstance.getDirectory(type: .Record)
        (FileList, DirectoryList) =  MyClassFile.sharedInstance.getAllFileWithPath(documentsURL: videourl)
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (self.DirectoryList?.count)!
        }
        return (self.FileList?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        if indexPath.section == 0 {
            cell.textLabel?.text = self.DirectoryList![indexPath.row].name
        }else{
            cell.textLabel?.text = self.FileList![indexPath.row].name
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
        }else{
            let url = self.FileList![indexPath.row].fileURL
            let item:AVPlayerItem = AVPlayerItem.init(url: url!)
            let playerVC = MyPandoraPlayer.configure(withAVItems: [item])
            playerVC.modalPresentationStyle = .fullScreen
            self.present(playerVC, animated: true, completion: nil)
        }
    }
    @IBAction func CloseMe(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}




