//
//  MainCardCell.swift
//  packtrack
//
//  Created by ksymac on 2017/12/16.
//  Copyright © 2017 ZHUKUI. All rights reserved.
//

import UIKit

protocol MainCardCellDelegate {

    func MainCardCellBtnClick1(_ bean : TrackMain?);
    func MainCardCellBtnClick2(_ bean : TrackMain?);
    func MainCardCellBtnClick3(_ bean : TrackMain?);
}

class MainCardCell: UITableViewCell {
    
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    
    @IBOutlet weak var TitleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var CommentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var Btn1: UIButton!
    @IBOutlet weak var Btn2: UIButton!
    @IBOutlet weak var Btn3: UIButton!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelComment: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelUpdateDate: UILabel!
    @IBOutlet weak var labelInsertDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    var bean: TrackMain?
    //创建View
    class func CreateCellWithNib(table: UITableView, maindata : TrackMain, celldelegate: MainCardCellDelegate) -> MainCardCell{
        var cell = table.dequeueReusableCell(withIdentifier: "MainCardCell")
        if (cell == nil) {
            table.register(UINib(nibName: "MainCardCell", bundle: nil), forCellReuseIdentifier: "MainCardCell")
            cell = Bundle.main.loadNibNamed("MainCardCell", owner: self, options: nil)?.first as! MainCardCell
        }
        (cell as! MainCardCell).setView(maindata: maindata,celldelegate:  celldelegate)
        return cell as! MainCardCell
    }
    
    //返回高度
    class func getHeight(maindata : TrackMain)-> CGFloat{
        let offset = 12 + 5
        
        var title_height = 25
        let content_height = 25
        var comment_height = 25
        
        let time_height = 20
        let bottom_height = 45
        
//        var height:CGFloat = 5 + 50 + 50 + 40;
        if(maindata.title.isEmpty){
            title_height = 10
        }
        if(maindata.comment.isEmpty){
            comment_height = 5
        }
        let height:CGFloat = CGFloat(offset + title_height + content_height + comment_height + time_height + bottom_height);
        return height-50;
    }
    
    var delegate:MainCardCellDelegate?
    //设置View
    func setView( maindata : TrackMain, celldelegate : MainCardCellDelegate){
        
        self.Btn1.setTitle(NSLocalizedString("main_view_cell_share", comment: ""), for: .normal)
        self.Btn2.setTitle(NSLocalizedString("main_view_cell_rename", comment: ""), for: .normal)
        self.Btn3.setTitle(NSLocalizedString("main_view_cell_other", comment: ""), for: .normal)
        
        self.bean = maindata
        self.delegate = celldelegate
        self.backgroundColor = GlobalBackgroundColor
        
        self.ContainerView.layer.masksToBounds = true
        self.ContainerView.layer.cornerRadius = 7
        self.ContainerView.backgroundColor = UIColor.white
        
        self.view1.backgroundColor = UIColor.white
        self.view2.backgroundColor = UIColor.white
        self.view3.backgroundColor = UIColor.white
        self.view4.backgroundColor = UIColor.white
        self.view5.backgroundColor = GlobalHeadColor

        self.labelTitle.text = maindata.title
        self.labelContent.text = maindata.filenamewithextension
        self.labelComment.text = maindata.comment
        if(maindata.title.isEmpty){
            TitleViewHeight.constant  = 10
        }else{
            TitleViewHeight.constant  = 25
        }
        if(maindata.comment.isEmpty){
            CommentViewHeight.constant = 5
        }else{
            CommentViewHeight.constant = 25
        }
        
        let (type ,duration,filesize) = VMFileClass.sharedInstance.getRecFileArrStr(rectype: maindata.rectype, duration: maindata.duration, filesize: maindata.datasize)
        self.labelInsertDate.text = String.init(format: "%@   %@   %@", type ,  filesize, duration)
        self.labelUpdateDate.text = DateUtils.StrFormateWithYear(instr: maindata.insertDate)
    }
    @IBAction func clickBtn1(_ sender: Any) {
        self.delegate?.MainCardCellBtnClick1(self.bean)
    }
    @IBAction func clickBtn2(_ sender: Any) {
        self.delegate?.MainCardCellBtnClick2(self.bean)
    }
    @IBAction func clickBtn3(_ sender: Any) {
        self.delegate?.MainCardCellBtnClick3(self.bean)
    }
}



